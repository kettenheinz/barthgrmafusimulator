########################
# FU Simulator ReadMe  #
# Softwareprojekt 2018 #
########################

Das Wiki ist zu finden unter:
git clone git@bitbucket.org:kettenheinz/barthgrmafusimulator.git/wiki

Die Code-Dokumentation wird über Doxygen bereitgestellt. Sie liegt im Ordner /doxygen/html und ist über die dortige index.html aufrufbar.

Installation:
1. Nach dem Git clone den Branch "2018-SWP-Landgraf" auschecken.
$ git checkout 2018-SWP-Landgraf

2. Die beiden Installationsskripte installDependencies.sh und install.sh ausführen
3. Programm starten:
  ./bin/fusimulator reicht als Aufruf, es ist allerdings sinnvoll, die Anzahl der Durchläufe anzugeben
  ./bin/fusimulator --runCount 10000
  Weitere Optionen sind über --help nachlesbar.
  
4. Im Browser die Adresse http://localhost:9008/  aufrufen und die Simulation per Button starten


5. Ein bereits funktionierendes Beispiel kann im Master-Branch gefunden werden.
   ACHTUNG: Der Master-Branch kann kompiliert werden, installiert allerdings die dafür benötigten
   allerdings Bibliotheken OpenCV und Qt.
   
6. Fragen bitte an:
   gregor.barth@fu-berlin.de


