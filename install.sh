#!/bin/bash
# Install script for the FUSI Simulator

echo -e "==== Checking for gcc 7 or higher ===="
if [ $(dpkg-query -W -f='${Status}' gcc-7 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  echo "No gcc detected. Please install a current version of gcc and run this script again."
	exit
else
	currentver="$(gcc -dumpversion)"
	requiredver="7"
  if [ "$(printf '%s\n' "$requiredver" "$currentver" | sort -V | head -n1)" = "$requiredver" ]; then 
		echo "GCC-7 or higher was found."
  else
		echo "gcc was found but is outdated. Please install at least version 7 and run this script again."
		exit
  fi
fi

echo -e "==== Checking for CMake ===="
if [ $(command -v cmake 2>/dev/null | grep -c "cmake") -eq 0 ];
then
	"I require cmake but it's not installed.  Aborting."
	exit 1
else
	echo "Cmake found."
fi

echo -e "==== Setting up jsoncpp ===="
cd extRepositories/jsoncpp
python amalgamate.py
mkdir -p build
cd build

echo -e "==== Building project ===="
case "$OSTYPE" in
  linux*)   
    echo "building with cmake for Linux"
    cmake -DCMAKE_BUILD_TYPE=release -DBUILD_STATIC_LIBS=ON -DBUILD_SHARED_LIBS=OFF -DARCHIVE_INSTALL_DIR=. -G "Unix Makefiles" ..
    ;;
  msys*)
    echo "building with cmake for Windows"
    cmake -DCMAKE_BUILD_TYPE=release -DBUILD_STATIC_LIBS=ON -DBUILD_SHARED_LIBS=OFF -DARCHIVE_INSTALL_DIR=. ..
    ;;
  solaris*)
    echo "can't build for SOLARIS" ;;
  darwin*)
    echo "cant build for OSX" ;; 
  bsd*)
    echo "can't build for BSD" ;;
  *)
    echo "unknown: $OSTYPE, can't build project" ;;
esac

sleep 3s  #just so the user can read stuff

make
cd ../../../
cmake -H. -Bbuild
cmake --build build -- -j3 -B


echo -e "== Create documentation =="
doxygen doxygen.config


echo -e "==== Finished install ===="
