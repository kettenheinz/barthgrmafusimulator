#pragma once

#include <stdio.h>
#include <fstream>
#include <memory>
#include <vector>

#include "IBehaviour.h"

#define ROBOTRACKERPLUGIN_LIBRARY 1

#include "GlobalPlugin.h"
#include "Actions/Actions.h"

#include <QtWidgets>

class ROBOTRACKERPLUGINSHARED_EXPORT BehaviourIdle : public ISimAgent
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "de.fu-berlin.mi.biorobotics.IRobotrackerBehaviour.Plugin" FILE "RobotrackerPlugin.json")
	Q_INTERFACES(ISimAgent)


public:
	std::array<double, 4> nextPos(std::array<double, 4> agentData, std::vector<std::array<double, 4>> otherAgents, double wWidth, double wHeight);

	void init();
protected:

private:

};
