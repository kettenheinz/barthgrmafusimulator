#include "BehaviourIdle.h"

//placeholder
void BehaviourIdle::init() {}

std::array<double, 4> BehaviourIdle::nextPos(std::array<double, 4> agentData, std::vector<std::array<double, 4>> otherAgents, double wWidth, double wHeight) {

	std::array<double, 4> targetPos;
	targetPos[0] = agentData.at(0) + 0.01;
	targetPos[1] = agentData.at(1) - 0.001;
	targetPos[2] = 0;
	targetPos[3] = 0;
	return targetPos;
}
