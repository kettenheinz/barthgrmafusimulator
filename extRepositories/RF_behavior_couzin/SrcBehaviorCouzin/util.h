#pragma once
#include <random>
#include <cmath>

#include "eigen.h"

//namespace robofish::ai::util
//{
    extern thread_local std::mt19937_64 rng;

    inline double angle_towards(Eigen::Vector2d a, Eigen::Vector2d b)
    {
        auto dot = a.dot(b);
        Eigen::Matrix2d tmp;
        tmp << a, b;
        auto det = tmp.determinant();
        return std::atan2(det, dot);
    }

    template<typename Value, typename II>
    std::size_t digitize(Value&& value, II bins_begin, II bins_end)
    {
        std::size_t i = 0;
        for (std::size_t i = 0; bins_begin != bins_end; ++bins_begin, ++i)
        {
            if (value < *bins_begin)
            {
                return i;
            }
        }
        return std::numeric_limits<std::size_t>::max();
    }
//}
