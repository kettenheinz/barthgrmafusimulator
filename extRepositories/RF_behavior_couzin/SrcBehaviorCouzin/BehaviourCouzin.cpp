#include "BehaviourCouzin.h"

#include <random>
#define _USE_MATH_DEFINES
#include <math.h>

#if defined(__GNUC__)
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wint-in-bool-context"
#endif
#include <Eigen/Dense>
#if defined(__GNUC__)
    #pragma GCC diagnostic pop
#endif

#include "couzin.h"


void BehaviourCouzin::init() {

	double zor  = 25.0;							//zone of repulsion (2.0)
	double zoo  = 4.0;							//zone of orientation (4.0)
	double zoa  = 128.0;						//zone of attraction (128.0)
	double fop  = 4.71239;					//field of perception in rad (270°)
	double tr   = 4.71239;					//turn rate in rad (45°)
	double s    = 1.0;							//speed (1.0)
	double sd   = 0.02;							//standard distribution (0.02)
	double ts   = 0.1;							//time step (0.1)
	double zobr = 20.0;							//zone of border repulsion (20.0)

	behavior = new CouzinBehaviour(zor, zoo, zoa, fop, tr, s, sd, ts, zobr);
}


/*---------------------------------------*/

std::array<double, 4> BehaviourCouzin::nextPos(std::array<double, 4> agentData, std::vector<std::array<double, 4>> otherAgents, double wWidth, double wHeight) {

	//convert into a pose
	Eigen::Vector4d pose{agentData.at(0), agentData.at(1), agentData.at(2), agentData.at(3)};

	//fill with data from other agents
	std::vector<Eigen::Vector4d> other_poses;
	for (auto oA : otherAgents) {
		other_poses.emplace_back(oA.at(0), oA.at(1), oA.at(2), oA.at(3));
	}
	//convert world data
	Eigen::Vector2d world{wWidth, wHeight};

	Eigen::Vector4d newPosV = behavior->apply(pose, other_poses, world);

	//go from eigenvector to std::array
	return {newPosV[0], newPosV[1], newPosV[2], newPosV[3]};
}
