#pragma once
#include <random>
#include <iostream>

#if defined(__GNUC__)
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wint-in-bool-context"
#endif
#include <Eigen/Dense>
#if defined(__GNUC__)
    #pragma GCC diagnostic pop
#endif

#include "util.h"

//namespace robofish::ai
//{
    bool zobr_target(Eigen::Ref<Eigen::Vector2d> target, double zobr, Eigen::Ref<const Eigen::Vector4d> const& pose, Eigen::Ref<const Eigen::Vector2d> const& world);

    template <typename InputRange>
    bool zor_target(Eigen::Ref<Eigen::Vector2d> target, double zobr, double zor, Eigen::Ref<const Eigen::Vector4d> const& pose, InputRange&& other_poses, Eigen::Ref<const Eigen::Vector2d> const& world)
    {
        if (zobr_target(target, zobr, pose, world))
        {
            return false;
        }

        target = Eigen::Vector2d{0,0};
        bool repulsed = false;

        for (Eigen::Ref<const Eigen::Vector4d> const& other_pose : other_poses)
        {
            if (other_pose.data() == pose.data())
            {
                continue;
            }

            Eigen::Vector2d towards = other_pose.segment<2>(0) - pose.segment<2>(0);
            auto distance = towards.norm();
            towards.normalize();
            if (distance < zor)
            {
                repulsed = true;
                target -= towards;
            }
        }

        return repulsed;
    }


    double correction_angle(Eigen::Vector2d& target, Eigen::Vector2d& before, Eigen::Vector2d& after);

    class CouzinBehaviour
    {
    private:
        double zor, zoo, zoa, zobr;
        double rr, ro, ra;
        double fop, tr, s, sd;
        double ts;

        std::normal_distribution<double> direction_error;
    public:
        inline CouzinBehaviour(double zor, double zoo, double zoa, double fop, double tr, double s, double sd, double ts, double zobr)
            : zor(zor), zoo(zoo), zoa(zoa), rr(zor), ro(zor + zoo), ra(zor + zoo + zoa), fop(fop), tr(tr), s(s), sd(sd), ts(ts), direction_error(0, sd), zobr(zobr)
        {
        }

        template <typename InputRange>
        inline Eigen::Vector4d apply(Eigen::Ref<const Eigen::Vector4d> const& pose, InputRange&& other_poses, Eigen::Ref<const Eigen::Vector2d> const& world)
        {
            bool repulsed = false;
            Eigen::Vector2d d_r{0,0}, d_o{0,0}, d_a{0,0};

            repulsed = zobr_target(d_r, zobr, pose, world);

            if (!repulsed) for (Eigen::Ref<const Eigen::Vector4d> const& other_pose : other_poses)
            {
                if (other_pose.data() == pose.data())
                {
                    continue;
                }

                Eigen::Vector2d towards = other_pose.segment<2>(0) - pose.segment<2>(0);
                auto distance = towards.norm();
                towards.normalize();
                if (distance < this->rr)
                {
                    repulsed = true;
                    d_r -= towards;
                }
                else
                {
                    if (repulsed)
                    {
                        continue;
                    }

                    if (pose.segment<2>(2).dot(towards) > this->fop / 2.)
                    {
                        continue;
                    }

                    if (distance < this->ro)
                    {
                        d_o += other_pose.segment<2>(2).normalized();
                    }
                    else if (distance < this->ra)
                    {
                        d_a += towards;
                    }
                }
            }

						Eigen::Vector2d d{0,0};

            if (repulsed)
            {
                d = d_r;
            }
            else
            {
                d = d_o + d_a;
            }

            if (d.isZero(1e-5))
            {
                d = pose.segment<2>(2);
            }

            d = (Eigen::Rotation2Dd(this->direction_error(rng)) * d).normalized();

            auto max_turn_angle = this->ts * this->tr;
            auto required_turn_angle = angle_towards(pose.segment<2>(2), d);
            auto turn_angle = std::min(std::abs(required_turn_angle), max_turn_angle);

            if (required_turn_angle < 0)
            {
                turn_angle *= -1;
            }

            Eigen::Vector2d v = (Eigen::Rotation2Dd(turn_angle) * pose.segment<2>(2)).normalized();
            Eigen::Vector2d c = pose.segment<2>(0) + (v * this->s);

            if (c[0] < 0 || c[0] > world[0])
            {
                v[0] *= -1;
            }
            if (c[1] < 0 || c[1] > world[1])
            {
                v[1] *= -1;
            }
            c = pose.segment<2>(0) + (v * s);
						/*
						std::cout << "repulsed "<< repulsed  <<"\n";
						std::cout << "vector d " << d <<"\n";
						std::cout << "turn_angle "<< turn_angle <<"\n";
						std::cout << "vector v " <<v << "\n";
						std::cout << "vector c " << c <<"\n";
						std::cout << "pose " << pose <<"\n";
						std::cout << "pose segment " << pose.segment<2>(2) <<"\n";
						*/
            return {c[0], c[1], v[0], v[1]};
        }
    };
//}
