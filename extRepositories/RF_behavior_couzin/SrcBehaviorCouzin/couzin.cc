#include "couzin.h"
#include "util.h"

bool zobr_target(Eigen::Ref<Eigen::Vector2d> target, double zobr, Eigen::Ref<const Eigen::Vector4d> const& pose, Eigen::Ref<const Eigen::Vector2d> const& world)
{
    bool repulsed = false;
    target = Eigen::Vector2d{0,0};

    if (world[0] - pose[0] < zobr)
    {
        repulsed = true;
        target += Eigen::Vector2d{-1, 0};
    }

    if (world[1] - pose[1] < zobr)
    {
        repulsed = true;
        target += Eigen::Vector2d{0, -1};
    }

    if (pose[0] < zobr)
    {
        repulsed = true;
        target += Eigen::Vector2d{1, 0};
    }

    if (pose[1] < zobr)
    {
        repulsed = true;
        target += Eigen::Vector2d{0, 1};
    }

    return repulsed;
}

double correction_angle(Eigen::Vector2d& target, Eigen::Vector2d& before, Eigen::Vector2d& after)
{
    return std::abs(angle_towards(before, target)) - std::abs(angle_towards(after, target));
}
