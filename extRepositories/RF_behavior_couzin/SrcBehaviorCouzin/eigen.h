#pragma once

// Resolve conflict between X11 and Eigen
#ifdef Success
    #undef Success
#endif

// Ignore warnings for Eigen
#if defined(__GNUC__)
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wint-in-bool-context"
#endif
#include <Eigen/Dense>
#if defined(__GNUC__)
    #pragma GCC diagnostic pop
#endif
