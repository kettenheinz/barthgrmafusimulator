#!/bin/bash
# Dependency install script for the FUSI Simulator


echo -e "==== Installing boost ===="
sudo apt-get install libboost-all-dev

echo -e "==== Installing Qt5 ===="
sudo apt-get install qt5-default
sudo apt-get install libqt5charts5-dev

echo -e "==== installing jsoncpp ===="
sudo apt-get install libjsoncpp-dev

echo -e "==== Installing OpenCV ===="
sudo apt-get install libopencv-dev

echo -e "==== Installing Eigen lib for couzin behaviour ===="
sudo apt-get install libeigen3-dev

echo -e "==== Installing SSL & Doxy ===="
sudo apt-get install libssl-dev doxygen

echo -e "==== Finished installing dependencies ===="

