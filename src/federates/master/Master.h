#pragma once

#include <string>
#include <vector>

#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"

/**
 * \class Master
 * This federate manages the flow of a federation by starting and stopping its execution, as well as loading the scenario(s).
 * \author Gregor Barth
 * \date 08.12.2017
 */

class Master: public Federate {
public:
	Master(std::string const& name, std::string const configPath, std::string somPath);
	~Master();

	void init();
	void update();

	void loadConfig();
	void loadScenario();

	void setRunCount(int runCount) { mRunCount = runCount;}

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);

private:
	int  mFrameRate;
	bool mIsRunning;
	bool mFinished;
	int	 mRunCount;
	int  mFrameCount;
};
