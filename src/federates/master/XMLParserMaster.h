#pragma once

#include <string>
#include <map>
#include <vector>


/*
 * \class XMLParser
 * A simple XML-Parser which can parse all config XML-Files of this master federate.
 * \author Gregor Barth
 */

class XMLParserMaster {
public:
	XMLParserMaster();

	/**
	 * Parse all configuration values of a federate.
	 */
	std::vector<std::map<std::string,std::string>> parseFederateConfig(std::string path);

	/**
	 * Parse the scenario data for the simulation
	 */
	std::string parseScenario(std::string path);

private:
};
