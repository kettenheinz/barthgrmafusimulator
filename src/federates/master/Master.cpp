#include "Master.h"

#include <chrono>
#include <iostream>
#include <map>
#include <thread>

#include "XMLParserMaster.h"


Master::Master(std::string const& name,
							 std::string const configPath,
							 std::string const somPath)
	: Federate(name, configPath, somPath)
	, mFrameRate(10000)
	, mIsRunning(false)
	, mFinished(false)
	, mRunCount(0)
	, mFrameCount(0)
{

}

/*-----------------------------------------*/

Master::~Master(){}

/*-----------------------------------------*/
void Master::init() {
	declareInteractions();
	loadConfig();
}

/*-----------------------------------------*/
void Master::update() {
	auto nextFrame = std::chrono::steady_clock::now();
	while (! mFinished) {
		if (mIsRunning) {
			//Main simulation loop
			//The simulation runs at a fixed fps rate and is currently synched
			if (mFrameCount < mRunCount) {
				nextFrame += std::chrono::milliseconds(1000/mFrameRate);
				//main code execution
				//announce fps to the federation
				////TODO nur onChange senden
				InteractionClassHandle interH = mRtia->getInteractionClassHandle("SetFrameRate");
				std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
				std::vector<ParameterHandle> parameters = mRtia->getInteractionClassParameters(interH);
				for (auto paramH : parameters) {
					std::string paramName = mRtia->getParameterName(paramH);
					std::string value;
					if (paramName.find("fps") != std::string::npos) {
						value = mEncoder->serialize<int>(mFrameRate);
					}
					ParameterHandleValuePair pair(paramH, value);
					pair.setName(paramName);
					parameterHandleValuePairSet.push_back(pair);
				}
				mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);

				//advance time
				mRtia->timeAdvanceRequest(mHandle);
				mFrameCount++;
				
				//wait till end of frame
				std::this_thread::sleep_until(nextFrame);
			} else {
				mIsRunning = false;
				mFinished = true;
				std::cout << "Simulation finished after " << mFrameCount << " iterations. \n";
			}
		}
	}
}

/*-----------------------------------------*/
void Master::loadConfig() {
	XMLParserMaster parser;
	std::vector<std::map<std::string, std::string>> config = parser.parseFederateConfig(CONFIGPATH);
	mFrameRate = stoi(config.front().at("fps"));
}

/*-----------------------------------------*/
void Master::loadScenario() {
	XMLParserMaster parser;
	std::string scenario  = parser.parseScenario(CONFIGPATH);

	std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
	InteractionClassHandle interH = mRtia->getInteractionClassHandle("LoadScenario");
	//get all parameters belonging to this interaction
	std::vector<ParameterHandle> parameters = mRtia->getInteractionClassParameters(interH);

	for (auto paramH : parameters) {
		std::string paramName = mRtia->getParameterName(paramH);
		std::string value;
		if (paramName.find("scenarioPath") != std::string::npos) {
			value = mEncoder->serialize<std::string>(scenario);
		}
		ParameterHandleValuePair pair(paramH, value);
		pair.setName(paramName);
		parameterHandleValuePairSet.push_back(pair);
	}
	//send interaction via rti
	mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);

}

/*-----------------------------------------*/

void Master::receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters) {
	//check which interaction
	if (mRtia->getInteractionClassName(interH) == "ButtonStartSimulation") {
		mIsRunning = true;	
	} else if (mRtia->getInteractionClassName(interH) == "ButtonStopSimulation") {
		mIsRunning = false;
	}
}

