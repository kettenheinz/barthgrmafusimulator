#include "XMLParserMaster.h"

#include <iostream>
//#include <tinyxml2.h>
#include "util/XMLParser/tinyxml2.h"


using namespace std;
using namespace tinyxml2;

XMLParserMaster::XMLParserMaster(){};


/*-------------------------------------------------------*/		
vector<map<string,string>> XMLParserMaster::parseFederateConfig(string path) {
	vector<map<string,string>> config;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
//			(eResult == 0) ? cout<<"Federate config loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLNode * nMaster = nRoot->FirstChild();
	map<string,string> configData;
	XMLElement * eKey = nMaster->FirstChildElement("scenario");
	eKey = eKey->NextSiblingElement("fps");
	string objValue = eKey->GetText();
	configData.insert(pair<string, string>("fps", objValue));
	config.push_back(configData);
	return config;
}

/*-------------------------------------------------------*/		
string XMLParserMaster::parseScenario(string path) {
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
//			(eResult == 0) ? cout<<"Federate config loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLNode * nMaster = nRoot->FirstChild();
	map<string,string> configData;
	XMLElement * eKey = nMaster->FirstChildElement("scenario");
	string scenarioPath = eKey->GetText();
	return scenarioPath;

}

