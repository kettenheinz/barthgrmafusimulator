#include "federates/statisticsGenerator/StatisticsGenerator.h"

#include <iostream>

#include "util/Position/Position.h"

StatisticsGenerator::StatisticsGenerator(std::string const& name, std::string const configPath, std::string const somPath)
	:	Federate(name, configPath, somPath)
{
	mAgentFactory = new StatisticsGeneratorAgentFactory();
}

/*---------------------------------------*/

void StatisticsGenerator::init() {
	declareObjects();
	declareInteractions();
	createAgents();
}

/*---------------------------------------*/
/*
 * Main update loop.
 */
void StatisticsGenerator::update() {
	double cohesion = calculateCohesion();
	std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
	InteractionClassHandle interH = mRtia->getInteractionClassHandle("CohesionUpdate");
	//get all parameters belonging to this interaction
	std::vector<ParameterHandle> parameters = mRtia->getInteractionClassParameters(interH);

	for (auto paramH : parameters) {
		std::string paramName = mRtia->getParameterName(paramH);
		std::string value;

		if (paramName.find("cohesion") != std::string::npos) {
			value = mEncoder->serialize<double>(cohesion);
		}
		ParameterHandleValuePair pair(paramH, value);
		pair.setName(paramName);
		parameterHandleValuePairSet.push_back(pair);
	}
	//send interaction via rti
	mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);
}

/*---------------------------------------*/
void StatisticsGenerator::reflectAttributeValues(ObjectInstanceHandle objH, std::vector<AttributeHandleValuePair> attributes) {
	//get correlating agent
	Agent* agent = getAgent(objH);
	//decode values from incomming data
	for (auto pair : attributes) {
		std::string attrName = pair.getName();
		if (attrName.find("pos") != std::string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			agent->setPos(p);
		}
	}
}

/*---------------------------------------*/
void StatisticsGenerator::receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters) {

}

/*---------------------------------------*/
void StatisticsGenerator::createAgents() {
	for (auto inst : mDiscoveredObjects) {
		Agent* agent = mAgentFactory->provideStatisticsAgent("StatisticsAgent");
		mAgents.push_back(agent);
		agent->setHandle(inst);
	}
}

/*---------------------------------------*/
double StatisticsGenerator::calculateCohesion() {
	double cohesion = 0;
	int s = mAgents.size();
	double diffX, diffY;
	for (int i=0; i<= s-1; i++) {
		for (int j=i+1; j<= s-1; j++) {
		  diffX = std::abs(mAgents.at(i)->getPos().at(0) - mAgents.at(j)->getPos().at(0));	
			diffY = std::abs(mAgents.at(i)->getPos().at(1) - mAgents.at(j)->getPos().at(1));	
		}
	}
	diffX = diffX/s;
	diffY = diffY/s;
	cohesion += (diffX + diffY)/2;

	return cohesion;
}

