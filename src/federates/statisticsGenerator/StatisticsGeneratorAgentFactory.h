#pragma once

#include <string>

#include "federates/statisticsGenerator/StatisticsAgent.h"

/**
 * \class StatisticsGeneratorAgentFactory
 * Factory for agent class instances used by the statistics generator federate.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class StatisticsGeneratorAgentFactory {
	public:
		StatisticsGeneratorAgentFactory(){}
		~StatisticsGeneratorAgentFactory(){}

	StatisticsAgent* provideStatisticsAgent(std::string agentType) {
		StatisticsAgent* agent = new StatisticsAgent(agentType);
		return agent;
	}


};
