#pragma once

#include <iostream>
#include "librti/Agent.h"
#include "util/Position/Position.h"


class StatisticsAgent: public Agent {
	public:
		StatisticsAgent(std::string _type):Agent(_type),pos(Position(0.0,0.0)), targetPos(Position(0.0,0.0)), orientation(0.0, 0.0){}

		void init(){}

		/*
		 * This agent only stores data, no main loop code needed right now
		 */
		void update(){	}

		Position getPos() {return pos;}
		Position getTargetPos() {return targetPos;}
		Position getOrientation() {return orientation;}

		void setPos(Position p) {pos = p;};
		void setTargetPos(Position p) {targetPos = p;}
		void setOrientation(Position o) {orientation = o;}
		void setPluginPath(std::string path) {}
		void setConfigPath(std::string path) {}
		void setScenario(Scenario* _scenario) {scenario = _scenario;}


	private:
		Position pos;
		Position targetPos;
		Position orientation;
};
