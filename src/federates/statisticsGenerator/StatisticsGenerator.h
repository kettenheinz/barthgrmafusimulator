#pragma once

#include <vector>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/ParameterHandleValuePair.h"

#include "federates/statisticsGenerator/StatisticsGeneratorAgentFactory.h"

/**
 * \class StatisticsGenerator
 * Federate for simulating a swarm of several fish.
 * \author Gregor Barth
 *
 */


class StatisticsGenerator: public Federate {
public:

	StatisticsGenerator(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	/**
	 * 6.11 HLA service
	 */
	void reflectAttributeValues(ObjectInstanceHandle obj, std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);

private:
	StatisticsGeneratorAgentFactory* mAgentFactory;

	/**
	 * Create a statistics agent for each discovered object
	 */
	void createAgents();

	/**
	 * Calculate the cohesion of the animal swarm by averaging the distance from each agent to all other agents.
	 */
	double calculateCohesion();

};

