#pragma once

#include <vector>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"
#include "util/Scenario/Scenario.h"
#include "util/Position/Position.h"

#include "CSVParserLogPlayer.h"

/**
 * \class LogPlayer
 * Federate for replaying data from a log file.
 * \author Gregor Barth
 *
 */


class LogPlayer: public Federate {
public:

	LogPlayer(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	/**
	 * 6.11 HLA service
	 * Not really needed here in the log player.
	 */
	void reflectAttributeValues(ObjectInstanceHandle obj, std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);

private:
	/**
	 * Create all agent objects
	 */
	void createAgents();

	Position strToPos(std::string);

	int frameCounter;
	CSVParserLogPlayer* csvParser;
};

