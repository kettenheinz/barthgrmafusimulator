#pragma once

#include <string>
#include <map>
#include <vector>


/**
 * \class XMLParserLogPlayer
 * A simple XML-Parser which can parse all config XML-Files of this project.
 * \author Gregor Barth
 * \date 25.05.2018
 */

class XMLParserLogPlayer {
public:
	XMLParserLogPlayer();

	/**
	 * Parse all agents to be created of this federate.
	 */
	std::vector<std::map<std::string,std::string>> parseFederateAgents(std::string path);

private:
};
