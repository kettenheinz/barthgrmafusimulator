#include "federates/logPlayer/LogPlayer.h"

#include <boost/algorithm/string.hpp>

#include "LogPlayerAgent.h"
#include "XMLParserLogPlayer.h"


using namespace std;

LogPlayer::LogPlayer(string const& name, string const configPath, string const somPath)
	:	Federate(name, configPath, somPath),
		frameCounter(1)
{
	csvParser = new CSVParserLogPlayer("src/federates/logPlayer/SimulationLog1.csv");	
}

/*---------------------------------------*/

void LogPlayer::init() {

	declareObjects();
	declareInteractions();
	createAgents();
}

/*---------------------------------------*/
/*
 * Main update loop.
 */
void LogPlayer::update() {
	//get next line as map of column name and value from parser
	map<string, string> attributeValueMap = csvParser->parseLineAt(frameCounter+1);

	for(auto objH : mRegisteredObjects) {
		vector<AttributeHandleValuePair> attributeHandleValuePairSet;
		//get all instanceAttributeHandles belonging to this objectInstance
		vector<InstanceAttributeHandle> objAttributes = mRtia->getObjectInstanceAttributes(objH);

		//match local variables to mHandles and set encoded local variable as value
		for (auto attrH : objAttributes) {
			string attrName = mRtia->getInstanceAttributeName(attrH);
			string value;

			//check if we deal with a position and convert it
			if (attrName.find("pos") != string::npos ||
					attrName.find("targetPos") != string::npos ||
					attrName.find("orientation") != string::npos) {
				Position pos = strToPos(attributeValueMap.find(attrName)->second);
				value = mEncoder->serialize<Position>(pos);
			} else {
				//just use the string value
				value = mEncoder->serialize<string>(attributeValueMap.find(attrName)->second);
			}

			AttributeHandleValuePair pair(attrH, value);
			pair.setName(attrName);
			attributeHandleValuePairSet.push_back(pair);	
		}
		//update one object instance with all its instance attributes
		mRtia->updateAttributeValues(mHandle, objH, attributeHandleValuePairSet);
	}

	frameCounter++;
}

/*---------------------------------------*/
void LogPlayer::reflectAttributeValues(ObjectInstanceHandle objH, vector<AttributeHandleValuePair> attributes) {
}

/*---------------------------------------*/
void LogPlayer::receiveInteraction(InteractionClassHandle interH, vector<ParameterHandleValuePair> parameters) {

}

/*---------------------------------------*/
/*
 * Create all mAgents the federate requires and let the federate register them if possible.
 */
void LogPlayer::createAgents() {
	XMLParserLogPlayer parser;
	vector<map<string,string>> config = parser.parseFederateAgents(CONFIGPATH);

	for (map<string,string> m : config) {
		string agentType = m.at("objectClass");
		int agentAmount = stoi(m.at("amount"));

		for(int i=0; i< agentAmount; i++) { //amount to create
			Agent* agent;
			agent = new LogPlayerAgent(agentType);
			mAgents.push_back(agent);
			registerObject(agent, agentType);
		}
	}
}

/*---------------------------------------*/
Position LogPlayer::strToPos(string data) {
	vector<string> posData;
	boost::split(posData, data, boost::is_any_of("|"));
	return Position(stod(posData.at(0)), stod(posData.at(1)) );
}

