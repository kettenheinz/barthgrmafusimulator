#include "XMLParserLogPlayer.h"

#include <iostream>
//#include <tinyxml2.h>
#include "util/XMLParser/tinyxml2.h"


using namespace std;
using namespace tinyxml2;

XMLParserLogPlayer::XMLParserLogPlayer(){};


/*-------------------------------------------------------*/		
vector<map<string,string>> XMLParserLogPlayer::parseFederateAgents(string path) {
	vector<map<string,string>> config;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
//			(eResult == 0) ? cout<<"Federate config loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLNode * nSwarm = nRoot->FirstChild();
	XMLNode * nAgent = nSwarm->FirstChild();
	while (nAgent != nullptr) {
		//parse individual agent
		map<string,string> agentData;
		XMLElement * eKey = nAgent->FirstChildElement("objectClass");
		string objValue = eKey->GetText();
		agentData.insert(pair<string, string>("objectClass", objValue));
		eKey = eKey->NextSiblingElement("amount");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("amount", objValue));
		config.push_back(agentData);

		nAgent = nAgent->NextSibling();
	}
	return config;
}
