#pragma once

#include "librti/Agent.h"

#include "util/Position/Position.h"


/**
 * \class LogPlayerAgent
 * This class defines an agent object for the log player.
 * It implements the agent interface for consistency purposed but only
 * serves as a placeholder agent for the data the log player replays from
 * a log file.
 * \author Gregor Barth
 * \date 25.05.2018
 */

class LogPlayerAgent: public Agent {
	public:
		LogPlayerAgent(std::string _type):Agent(_type) {}

		void init() {}
		void update() {}


		Position getPos() {return Position(0.0, 0.0);}
		Position getTargetPos() {return Position(0.0, 0.0);}
		Position getOrientation() {return Position(0.0, 0.0);}

		void setPos(Position p) {}
		void setTargetPos(Position p) {}
		void setOrientation(Position o) {}
		void setPluginPath(std::string path) {}
		void setConfigPath(std::string path) {}
		void setScenario(Scenario* _scenario) {}

};
