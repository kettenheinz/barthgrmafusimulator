#pragma once

#include <fstream>
#include <map>
#include <string>

/**
 * \class CSVParserLogPlayer
 * This parser parses simulation log files in the csv file format.
 * \author Gregor Barth
 * \date 25.05.2018
 */

class CSVParserLogPlayer {
public:
	CSVParserLogPlayer(std::string _path);

	/**
	 * Parse a line at the indicated index.
	 */
	std::map<std::string, std::string> parseLineAt(int lineNum);

private:
	std::string path;
	
	std::fstream& goToLine(std::fstream& file, unsigned int line);

};
