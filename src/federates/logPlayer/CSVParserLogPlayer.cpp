#include "CSVParserLogPlayer.h"

#include <iostream>
#include <limits>
#include <boost/tokenizer.hpp>


using namespace std;
using namespace boost;


CSVParserLogPlayer::CSVParserLogPlayer(std::string _path):path(_path) {}

/*---------------------------------------*/

map<string, string> CSVParserLogPlayer::parseLineAt(int lineNum) {

	typedef tokenizer< escaped_list_separator<char> > Tokenizer;
	vector<string> header;
	vector<string> data;
	map<string, string> nameValueMap;
	string lineH; //header line
	string line;  //all other lines

	//open file
	fstream file(path, ios_base::in);
	if (!file) {
		cout << "Unable to open log file at " << path << "\n";
	} else {

		//Get first line with column names into vector
    getline(file,lineH);
    Tokenizer tokH(lineH);
		for (Tokenizer::iterator it(tokH.begin()), end(tokH.end()); it != end; ++it){
	    header.push_back((*it));
		}

		//get desired line with data into vector
		goToLine(file, lineNum);
		if (!file) {
			cout << "Can't reach line " << lineNum << "\n";
		} else {
			getline(file,line);
			Tokenizer tok(line);
			for (Tokenizer::iterator it(tok.begin()), end(tok.end()); it != end; ++it){
				data.push_back((*it));
			}
		}
	}

	//insert name/data pairs into map
	//order is not important here as data is later found via keys
	for (auto k = 0; k < header.size(); k++) {
		nameValueMap.insert(pair<string, string>(header.at(k), data.at(k)));
	}

	return nameValueMap;
}

/*---------------------------------------*/
std::fstream& CSVParserLogPlayer::goToLine(std::fstream& file, unsigned int line) {
  file.seekg(std::ios::beg);
  for(unsigned int i=0; i < line - 1; ++i) {
		file.ignore(std::numeric_limits<std::streamsize>::max(),'\n');
	}
	return file;	
}

