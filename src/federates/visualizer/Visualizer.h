#pragma once
#include <string>
#include <vector>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"

#include "util/XMLParser/XMLParser.h"
#include "federates/visualizer/VisualizerAgentFactory.h"


#include "VisualizationServer.h"
#include "Resource.h"

#include "drawables/Label.h"
#include "drawables/Button.h"
#include "drawables/Canvas.h"
#include "drawables/CanvasContent.h"


/**
 * \class Visualizer
 * Federate to prepare the visualization of agents in the FUSimulator.
 * Subscribed data is reformatted and then send to a webpage via a websocket server.
 * \author Gregor Barth
 * \date 08.12.2017
 */


class Visualizer: public Federate {
public:
	Visualizer(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	/**
	 * Take the locally stored AttributeHandleValuePairs and transform them into a human readable
	 * Stringstream which we can then send off to the visualization server.
	 */
	std::string createDisplayData();

	/**
	 * 6.11 HLA service
	 */
	void reflectAttributeValues(ObjectInstanceHandle objH, std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);


private:
	/**
	 * Create a statistics agent for each discovered object
	 */
	void createAgents();

	VisualizerAgentFactory* mAgentFactory;
	
	VisualizationServer vServer;
	double cohesion; //cohesion statistic
	int mFrameRate;

	drawable::Button* startButton;
	bool buttonState;

	//Debugging objects
	std::unique_ptr<Resource> resource;
	std::unique_ptr<Resource> resourceCanv;
	std::unique_ptr<Resource> resourceCoh;

	drawable::Label*  label;
	drawable::Label*  labelCohesion;

	drawable::Canvas* canvas;
	drawable::CanvasContent canvasContent;

};

