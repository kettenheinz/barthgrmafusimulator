#pragma once

#include "Drawable.h"

/**
 * \class Label
 * \author Gregor Barth
 */

namespace drawable {

class Label : public Drawable {
public:
	Label(std::string const& text);
	~Label();

	void onConnectElement() override;

	/* Change the text and send out an updateElement event  */
	void setText(std::string const& text);

private:
	std::string mText;
	bool        mEditable;
};

}
