#include "Container.h"

#include <algorithm>
#include <iostream>

#include "federates/visualizer/Resource.h"

namespace drawable {

Container::Container() : Drawable("div") {}

/*---------------------------------------*/
Container::~Container() {}

/*---------------------------------------*/
void Container::onConnectElement() {
	auto msg = getMsgTemplate();
	mResource->createElement(msg);

	//create all elements this container was created with
	for (auto& d : mDrawables) {
		d->onConnectElement();
	}
}

/*---------------------------------------*/
void Container::onDisconnectElement() {
	auto msg = getMsgTemplate();
	mResource->destroyElement(msg);

	for (auto& d : mDrawables) {
		d->onDisconnectElement();
	}
}

/*---------------------------------------*/
bool Container::onEvent(std::string const& address, std::string const& event, Json::Value const& data) {
	//we first check if the event has to be handled by the container itself, afterwards we check its children
	bool processed = Drawable::onEvent(address, event, data);
	if (processed) return true;

	for (auto& d : mDrawables) {
		processed = d->onEvent(address, event, data);
		if (processed) return true;
	}
	return false;
}

/*---------------------------------------*/
void Container::addDrawable(UDrawable _drawable) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	auto drawable = _drawable.get();
	drawable->setContainer(this);
	drawable->setResource(mResource);
	mDrawables.push_back(std::move(_drawable));
	drawable->onConnectElement();
}


}
