function createCanvas(msg, sendEvent) {
		var element = $("<canvas>");

		var drawGeometry = function(geometry) {
			var ctx = element.get(0).getContext("2d");
			for (var idx in geometry.geometry) {
				var g = geometry.geometry[idx];

				//draw all the different shapes here
				if (g.type == "circle") {
					ctx.strokeStyle = g.color;
					ctx.lineWidth = 1;
					ctx.beginPath();
					ctx.arc(g.x,g.y,g.r,g.sa,g.ea);
					ctx.stroke();
				} else if (g.type == "circleFilled") {
					ctx.strokeStyle = g.color;
					ctx.fillStyle = g.color;
					ctx.lineWidth   = 1;
					ctx.beginPath();
					ctx.arc(g.x, g.y, g.r, g.sa, g.ea);
					ctx.fill();
				} else if (g.type == "line") {
					ctx.strokeStyle = g.color;
					ctx.lineWidth   = 1;
					ctx.beginPath();
					ctx.moveTo(g.x1, g.y1);
					ctx.lineTo(g.x2, g.y2);
					ctx.stroke();
				} else if (g.type == "rectangle") {
					ctx.strokeStyle = g.color;
					ctx.lineWidth   = 1;
					ctx.beginPath();
					ctx.rect(g.x1, g.y1, g.x2 - g.x1 + 1, g.y2 - g.y1 + 1);
					ctx.stroke();
				} else if (g.type == "rectangleFilled") {
					ctx.strokeStyle = g.color;
					ctx.fillStyle   = g.color;
					ctx.lineWidth   = 1;
					ctx.beginPath();
					ctx.rect(g.x1, g.y1, g.x2 - g.x1 + 1, g.y2 - g.y1 + 1);
					ctx.fill();
				} else if (g.type == "text") {
					ctx.beginPath();
					ctx.fillStyle = g.color;
					ctx.font = "15px Georgia";
					ctx.fillText(g.text, g.x, g.y);
					ctx.stroke();
				} else if (g.type == "arrow") {
					function canvas_arrow(context, fromx, fromy, tox, toy){
						var headlen = 10;   // length of head in pixels
						var angle = Math.atan2(toy-fromy,tox-fromx);
						context.moveTo(fromx, fromy);
						context.lineTo(tox, toy);
						context.lineTo(tox-headlen*Math.cos(angle-Math.PI/6),toy-headlen*Math.sin(angle-Math.PI/6));
						context.moveTo(tox, toy);
						context.lineTo(tox-headlen*Math.cos(angle+Math.PI/6),toy-headlen*Math.sin(angle+Math.PI/6));
					}
					ctx.strokeStyle = g.color;
					ctx.lineWidth   = 1;
					ctx.beginPath();
					canvas_arrow(ctx, g.x1, g.y1, g.x2, g.y2);
					ctx.stroke();
				}

			}
		}

		var drawBackground = function(msg) {
			var ctx = element.get(0).getContext("2d");
			if (msg.background != "") {
				ctx.beginPath();
				ctx.rect(0, 0, msg.width, msg.height);
				ctx.fillStyle = msg.background;
				ctx.fill();
			} else {
				ctx.clearRect(0, 0, msg.width, msg.height)
			}
		}

		var update = function(msg) {
			var width  = msg.content.width;
			var height = msg.content.height;
			element.attr("width", width);
			element.attr("height", height);

			drawBackground(msg.content);
			drawGeometry(msg.content);
		};

		//make update function accessible
		element.update = update;

		applyStdCreateElement(element, msg);
		return element;
}
	
function updateCanvas(element, msg) {
		if (applyStdUpdateElement(element, msg)) {
		} else if (msg.event == "redraw") {
			element.update(msg);
		} else {
			console.log("unknown canvas update: " + msg);
		}
}
