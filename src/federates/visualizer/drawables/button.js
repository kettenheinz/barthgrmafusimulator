function createButton(msg, sendEvent) {
		var element = $("<button>");
		element.html(msg.caption);

		var onClick = function(e) {
			sendEvent("click", {});
		}
		element.on("click", onClick);

		applyStdCreateElement(element, msg);
		return element;
}
	
function updateButton(element, msg) {
  if (applyStdUpdateElement(element, msg)) {
	} else if (msg.event == "setCaption") {
		element.html(msg.caption);
	}
}
