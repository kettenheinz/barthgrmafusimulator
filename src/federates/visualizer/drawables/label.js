function createLabel(msg, sendEvent) {
	var element = $("<label>");
	applyStdCreateElement(element, msg);
	element.html(msg.text);
	return element;
}
	
function updateLabel(element, msg) {
	if (applyStdUpdateElement(element, msg)) {
	} else if (msg.event == "setText") {
		element.html(msg.text);
	}
}
