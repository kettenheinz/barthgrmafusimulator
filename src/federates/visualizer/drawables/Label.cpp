#include "Label.h"

#include "federates/visualizer/Resource.h"


namespace drawable {

Label::Label(std::string const& text)
	: Drawable("label")
	, mText(text)
{
	
}

/*---------------------------------------*/
Label::~Label() {}

/*---------------------------------------*/
void Label::onConnectElement() {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	auto msg = getMsgTemplate();
	msg["text"]         = mText;
	msg["editable"]     = mEditable;
	mResource->createElement(msg);
}

/*---------------------------------------*/
void Label::setText(std::string const& text) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	mText = text;

	auto msg = getMsgTemplate();
	msg["event"] = "setText";
	msg["text"] = mText;
	mResource->updateElement(msg);
}

}
