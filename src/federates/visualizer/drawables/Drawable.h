#pragma once

#include <functional>
#include <string>
#include <mutex>

#include "jsoncpp/include/json/json.h"
//#include <json/json.h>

/**
 * \class Drawable
 * \author Gregor Barth
 */

class Resource;

namespace drawable {
class Container;

template<typename T>
std::unique_lock<T> make_unique_lock(T& mutex) {
	return std::unique_lock<T>(mutex);
}

class Drawable {
public:
	using Callback = std::function<void(Json::Value const&)>;

	Drawable(std::string type);
	virtual ~Drawable();

	virtual void onConnectElement();
	virtual void onDisconnectElement();
	virtual bool onEvent(std::string const& address, std::string const& event, Json::Value const& data);


	/*--------------------------- Getters and Setters ------------------------------*/
	Json::Value getMsgTemplate() const;
	std::string getUID() const;
	std::string getHTMLID() const   { return mHTMLID; }
	Resource* getResource() const   { return mResource; }
	Container* getContainer() const { return mContainer; }

	void setResource(Resource* res)   { mResource = res; }
	void setContainer(Container* con) { mContainer = con; }
	void setHTMLID(std::string id)    { mHTMLID = id; }

private:

	std::string mType;
	std::string mHTMLID;
	//Style mStyle;

protected:
	mutable std::recursive_mutex mMutex;

	Resource*  mResource {nullptr};
	Container* mContainer {nullptr};
	std::map<std::string, Callback> mCallbacks;
};

}
