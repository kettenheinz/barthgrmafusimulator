#include "Canvas.h"

#include "federates/visualizer/Resource.h"

namespace drawable {

Canvas::Canvas()
	: Drawable("canvas")
{
	mCanvasContent.setSize(640, 480);
	mCanvasContent.setBackground("#B0B0B0");

	using namespace std::placeholders;
	mCallbacks["click"]      = std::bind(&Canvas::onClick, this, _1);
	mCallbacks["rightClick"] = std::bind(&Canvas::onRightClick, this, _1);
	mCallbacks["mouseWheel"] = std::bind(&Canvas::onMouseWheel, this, _1);
}

/*---------------------------------------*/
Canvas::~Canvas() {}

/*---------------------------------------*/
void Canvas::onConnectElement() {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	auto msg = getMsgTemplate();
	mResource->createElement(msg);
}

/*---------------------------------------*/
void Canvas::setContent(CanvasContent content) {
	mCanvasContent = content;

	auto msg			 = getMsgTemplate();
	msg["event"]	 = "redraw";
	msg["content"] = mCanvasContent.getContent();
	mResource->updateElement(msg);
}

/*---------------------------------------*/
void Canvas::setOnClick(mouseCallback callback) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	mOnClicked = callback;
}

/*---------------------------------------*/
void Canvas::setOnRightClick(mouseCallback callback) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	mOnRightClicked = callback;
}
/*---------------------------------------*/
void Canvas::setOnMouseWheel(mouseWheelCallback callback) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	mOnMouseWheel = callback;
}
/*---------------------------------------*/
void Canvas::onClick(Json::Value const& event) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	if (mOnClicked) {
		mOnClicked(event["pX"].asInt(), event["pY"].asInt());
	}
}
/*---------------------------------------*/
void Canvas::onRightClick(Json::Value const& event) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	if (mOnRightClicked) {
		mOnRightClicked(event["pX"].asInt(), event["pY"].asInt());
	}
}
/*---------------------------------------*/
void Canvas::onMouseWheel(Json::Value const& event) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	if (mOnMouseWheel) {
		mOnMouseWheel(event["wheel"].asInt());
	}
}



}
