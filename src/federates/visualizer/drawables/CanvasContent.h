#pragma once

#define _USE_MATH_DEFINES 1
#include <math.h>

#include "jsoncpp/include/json/json.h"

/**
 * \class CanvasContent
 * \author Gregor Barth
 */

namespace drawable {

class CanvasContent {
public:
	CanvasContent();
	~CanvasContent();

	void clear();

	void setSize(int width, int height);
	void setBackground(std::string background);
	Json::Value getContent() const;

	void addCircle(std::string rgbColor, int x, int y, int r, double sa = 0., double ea = M_PI*2);
	void addCircleFilled(std::string rgbColor, int x, int y, int r, double sa = 0., double ea = M_PI *2);
	void addLine(std::string rgbColor, int x1, int y1, int x2, int y2, int width);
	void addRectangle(std::string rgbColor, int x1, int y1, int x2, int y2);
	void addRectangleFilled(std::string rgbColor, int x1, int y1, int x2, int y2);
	void addText(std::string rgbColor, int x, int y, std::string const& text);
	void addArrow(std::string rgbColor, int x1, int y1, int x2, int y2);


private:
	Json::Value mContent;
};

}
