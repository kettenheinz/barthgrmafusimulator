#include "Drawable.h"

#include <iostream>
#include <sstream>
#include <websocketpp/server.hpp>

#include "federates/visualizer/Resource.h"
#include "Container.h"


namespace drawable {

Drawable::Drawable(std::string type)
	: mResource {nullptr}
	, mContainer {nullptr}
	, mType(std::move(type))
{
}

/*---------------------------------------*/
Drawable::~Drawable() {}

/*---------------------------------------*/


/*---------------------------------------*/
void Drawable::onConnectElement() {
	auto msg = getMsgTemplate();
	mResource->createElement(msg);
}

/*---------------------------------------*/
void Drawable::onDisconnectElement() {
	auto msg = getMsgTemplate();
	mResource->destroyElement(msg);
}

/*---------------------------------------*/
bool Drawable::onEvent(std::string const& address, std::string const& event, Json::Value const& data) {
	if (address != getUID()) return false;

	auto it = mCallbacks.find(event);
	if (it != mCallbacks.end()) {
		it->second(data);
	}
	return true;
}

/*---------------------------------------*/
Json::Value Drawable::getMsgTemplate() const {
	auto msgTemplate = Json::Value(Json::objectValue);
	msgTemplate["type"]     = mType;
	msgTemplate["address"]  = getUID();
	msgTemplate["htmlid"]   = mHTMLID;
//	msgTemplate["styleClasses"] = mType;  //TODO add later

	if (mContainer) {
		msgTemplate["parent"] = mContainer->getUID();
	}
	return msgTemplate;
}

/*---------------------------------------*/
std::string Drawable::getUID() const {
	std::stringstream ss;
	ss << std::hex << "0x" << this;
	return ss.str();
}



}
