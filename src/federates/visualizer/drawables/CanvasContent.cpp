#include "CanvasContent.h"

namespace drawable {

/*---------------------------------------*/
CanvasContent::CanvasContent() {
	setSize(100, 100);
	setBackground("#A0A0A0");
	mContent["geometry"] = Json::Value(Json::arrayValue);
}

/*---------------------------------------*/
CanvasContent::~CanvasContent() {
}

/*---------------------------------------*/
void CanvasContent::clear() {
	mContent["geometry"] = Json::Value(Json::arrayValue);
}

/*---------------------------------------*/
void CanvasContent::setSize(int width, int height) {
	mContent["width"]  = width;
	mContent["height"] = height;
}

/*---------------------------------------*/
void CanvasContent::setBackground(std::string background) {
	mContent["background"] = background;
}

/*---------------------------------------*/

Json::Value CanvasContent::getContent() const {
	return mContent;
}

/*---------------------------------------*/
void CanvasContent::addCircle(std::string rgbColor, int x, int y, int r, double sa, double ea) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "circle";
	value["color"] = rgbColor;
	value["x"]     = x;
	value["y"]     = y;
	value["r"]     = r;
	value["sa"]    = sa;
	value["ea"]    = ea;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addCircleFilled(std::string rgbColor, int x, int y, int r, double sa, double ea) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "circleFilled";
	value["color"] = rgbColor;
	value["x"]     = x;
	value["y"]     = y;
	value["r"]     = r;
	value["sa"]    = sa;
	value["ea"]    = ea;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addLine(std::string rgbColor, int x1, int y1, int x2, int y2, int width) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "line";
	value["color"] = rgbColor;
	value["x1"]    = x1;
	value["y1"]    = y1;
	value["x2"]    = x2;
	value["y2"]    = y2;
	value["width"] = width;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addRectangle(std::string rgbColor, int x1, int y1, int x2, int y2) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "rectangle";
	value["color"] = rgbColor;
	value["x1"]    = x1;
	value["y1"]    = y1;
	value["x2"]    = x2;
	value["y2"]    = y2;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addRectangleFilled(std::string rgbColor, int x1, int y1, int x2, int y2) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "rectangleFilled";
	value["color"] = rgbColor;
	value["x1"]    = x1;
	value["y1"]    = y1;
	value["x2"]    = x2;
	value["y2"]    = y2;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addText(std::string rgbColor, int x, int y, std::string const& text) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "text";
	value["color"] = rgbColor;
	value["x"]     = x;
	value["y"]     = y;
	value["text"]  = text;
	mContent["geometry"].append(value);
}

/*---------------------------------------*/
void CanvasContent::addArrow(std::string rgbColor, int x1, int y1, int x2, int y2) {
	auto value = Json::Value(Json::objectValue);
	value["type"]  = "arrow";
	value["color"] = rgbColor;
	value["x1"]    = x1;
	value["y1"]    = y1;
	value["x2"]    = x2;
	value["y2"]    = y2;
	mContent["geometry"].append(value);
}


}
