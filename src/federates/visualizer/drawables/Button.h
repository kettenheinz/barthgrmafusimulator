#pragma once

#include <functional>

#include "Drawable.h"

/**
 * \class Button
 * \author Gregor Barth
 */

namespace drawable {

class Button : public Drawable {
public:
	using Callback = std::function<void()>;

	Button(std::string caption);
	~Button();

	void onConnectElement() override;
	void onClick(Json::Value const& event);

	void setCallback(Callback callback);
	void setCaption(std::string const& caption);
	std::string getCaption() const;

private:
	std::string mCaption;
	Callback    mOnClicked; //store a function executed on an onClick event
};

}
