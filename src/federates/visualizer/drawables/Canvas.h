#pragma once

#include <functional>

#include "Drawable.h"
#include "CanvasContent.h"

/**
 * \class Canvas
 * \author Gregor Barth
 */

namespace drawable {

class Canvas : public Drawable {
public:
	using mouseCallback = std::function<void(int pX, int pY)>; //get a mouse position
	using mouseWheelCallback = std::function<void(int wheel)>; //get a mouse wheel rotation

	Canvas();
	~Canvas();

	void onConnectElement() override;

	/* Add all objects which are to be drawn to the canvas and
	 * send a message to the web client.
	 * Call this every frame.
	 * */
	void setContent(CanvasContent content);

	void setOnClick(mouseCallback callback);
	void setOnRightClick(mouseCallback callback);
	void setOnMouseWheel(mouseWheelCallback callback);

	void onClick(Json::Value const& event);
	void onRightClick(Json::Value const& event);
	void onMouseWheel(Json::Value const& event);

private:
	CanvasContent mCanvasContent;	

	mouseCallback			 mOnClicked;
	mouseCallback			 mOnRightClicked;
	mouseWheelCallback mOnMouseWheel;

};

}
