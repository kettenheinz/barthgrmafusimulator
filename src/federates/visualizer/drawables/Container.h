#pragma once

#include <memory>
#include <set>
#include <string>
#include <vector>


#include "Drawable.h"
#include "Label.h"
#include "Button.h"
#include "Canvas.h"


/**
 * \class Container
 * A container groups several html-objects together. It can hold any number or type of object derived
 * from the drawable class.
 * \author Gregor Barth
 */

namespace drawable {

class Container : public Drawable {
public:
	using UDrawable = std::unique_ptr<Drawable>;
	using Drawables = std::vector<UDrawable>;

	Container();
	~Container() override;

	void onConnectElement() override;
	void onDisconnectElement() override;

	/**
	 * Handle all incomming events for ourselves and all contained objects.
	 **/
	bool onEvent(std::string const& address, std::string const& event, Json::Value const& data) override;

	/*--------------------------- Getters and Setters ------------------------------*/
	/**
	 * Add an object derived of the drawable class to this container
	 */
	template<typename T, typename ...Args>
	auto add(Args&&... args) -> T* {
		auto drawable = std::unique_ptr<T>(new T(std::forward<Args>(args)...));
		auto ptr = drawable.get();
		addDrawable(std::move(drawable));
		return ptr;
	}

	void addDrawable(UDrawable _drawable);
	Drawables const& getDrawables() const {return mDrawables;}


protected:
	Drawables mDrawables;
};

}
