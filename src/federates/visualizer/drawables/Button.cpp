#include "Button.h"

#include "federates/visualizer/Resource.h"

namespace drawable {
Button::Button(std::string caption)
	: Drawable("button")
	, mCaption(caption)
{
	using namespace std::placeholders;
	//add the click event to the drawable
	mCallbacks["click"] = std::bind(&Button::onClick, this, _1);
}

/*---------------------------------------*/
Button::~Button() {}

/*---------------------------------------*/
void Button::onConnectElement() {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	auto msg = getMsgTemplate();
	msg["caption"] = mCaption;
	mResource->createElement(msg);
}

/*---------------------------------------*/
void Button::onClick(Json::Value const& event) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	if (mOnClicked) {
		mOnClicked();
	}
}

/*---------------------------------------*/
void Button::setCallback(Callback callback) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;
	mOnClicked = callback;
}

/*---------------------------------------*/
void Button::setCaption(std::string const& caption) {
	auto lock { make_unique_lock(mMutex) };
	(void)lock;

	mCaption = caption;
	auto msg = getMsgTemplate();
	msg["event"]   = "setCaption";
	msg["caption"] = mCaption;
	mResource->updateElement(msg);
}

/*---------------------------------------*/
std::string Button::getCaption() const {
	return mCaption;
}


}
