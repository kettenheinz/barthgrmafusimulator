#include "VisualizationServer.h"

#include "Resource.h"

#include <fstream>
#include <map>


VisualizationServer::VisualizationServer() : m_count(0) {
	mServer.set_reuse_addr(true);
	// set up access channels to only log interesting things
	mServer.clear_access_channels(websocketpp::log::alevel::all);
	mServer.set_access_channels(websocketpp::log::alevel::access_core);
	mServer.set_access_channels(websocketpp::log::alevel::app);


	// Initialize the Asio transport policy
	mServer.init_asio();

	mServer.set_listen_backlog(64); //SUUUUUUUPER IMPORTANT! sonst nix handle_access called

	// Bind the open handler
	mServer.set_open_handler([=](websocketpp::connection_hdl hdl) {
		std::lock_guard<std::recursive_mutex> lock(mMutex);
		auto locked = hdl.lock();
		//connect an existing resource to the new con
		auto con = mServer.get_con_from_hdl(hdl);
		auto resource = con->get_request().get_uri();

		auto it = mWSResources.find(resource);
		if(it == mWSResources.end()) {
			std::cerr << "Unknown WS resource on con opening: " << resource << std::endl;
			con->close(websocketpp::close::status::normal, "Resource not found!");
			return;
		}
		mConnectionToResource[con] = it->second;
		it->second->onConnect(hdl);
	});

	// Bind the close handler
	mServer.set_close_handler([=](websocketpp::connection_hdl hdl) {
		std::lock_guard<std::recursive_mutex> lock(mMutex);
		auto locked = hdl.lock();

		auto con = mServer.get_con_from_hdl(hdl);
		auto it = mConnectionToResource.find(con);
		if (it == mConnectionToResource.end()) {
			return;	//nothing to close, just continue
		}
		it->second->onDisconnect(hdl);
		mConnectionToResource.erase(it);
	});

	// Bind the message handler
	mServer.set_message_handler([=](websocketpp::connection_hdl hdl, WSServer::message_ptr msg) {
		std::lock_guard<std::recursive_mutex> lock(mMutex);
		auto locked = hdl.lock();

		auto con = mServer.get_con_from_hdl(hdl);
		auto it = mConnectionToResource.find(con);
		if (it == mConnectionToResource.end()) {
			return; //unknown connection
		}
		//pass message to resource
		it->second->onMessage(msg->get_payload());
	});

	//Bind the http handler
	mServer.set_http_handler([=] (websocketpp::connection_hdl hdl) {
		std::lock_guard<std::recursive_mutex> lock(mMutex);
		auto con = mServer.get_con_from_hdl(hdl);
		auto& request = con->get_request();

		try {
			if (request.get_method() == "GET") {
			getHTTP(con);
			}
		} catch (std::exception const& e) {
			std::cerr << "ERROR on GET request: " << e.what() << "\n";
		} catch (...) {
			std::cerr << "Unknown ERROR on GET request.\n";
		}
	});

	//add our javascript folder to the document root
	mRootFolders.emplace_back("./src/federates/visualizer");
	mRootFolders.emplace_back("./src/federates/visualizer/lib");
	mRootFolders.emplace_back("./src/federates/visualizer/drawables");


	//we need to hand out all the basic javascripts here
	registerDynamicResource("/VisualizationServer.js", [=] () {
		return readFile("./src/federates/visualizer/VisualizationServer.js");
	});

}

/*---------------------------------------*/
VisualizationServer::~VisualizationServer() {
	mServer.stop();
	mThread.join();
}

/*---------------------------------------*/
void VisualizationServer::run(std::string docroot, uint16_t port) {

		std::stringstream ss;
		ss << "Running visualization server on port "<< port <<" using docroot=" << docroot;
		mServer.get_alog().write(websocketpp::log::alevel::app,ss.str());

		// listen on specified port
		mServer.listen(port);

		m_docroot = docroot;

		mThread = std::thread([=]() {
			// Start the server accept loop
			mServer.start_accept();
			// Start the ASIO io_service run loop
			try {
					mServer.run();
			} catch (websocketpp::exception const & e) {
					std::cout << e.what() << std::endl;
			}
		});
		mIsRunning = true;
}


/*---------------------------------------*/
void VisualizationServer::getHTTP(WSServer::connection_ptr& con) {
	auto& req = con->get_request();
	std::string uri = req.get_uri();
	if (uri == "/") {
		uri = "/index.html";
	}

	//check for dynamic resource
	auto dynamicResource = mDynamicResources.find(uri);
	if (dynamicResource != mDynamicResources.end()) {
		std::string content = dynamicResource->second(); //call for the page creation function
		con->set_body(content);
		con->set_status(websocketpp::http::status_code::ok);
		return;
	}

	// check if a static file from one of the folders is required and deliver it
	std::string fileName = "";
	for (auto const& p : mRootFolders) {
		auto name = p + uri;
		if (fileExists(name)) {
			fileName = std::move(name);
			break;
		}
	}
	if (fileName == "") {
		std::cout << "file couldn't be found: " << uri << "\n";
		con->set_status(websocketpp::http::status_code::not_found);
		return;
	}

	try {
		auto content = readFile(fileName);
		con->set_body(content);
		con->set_status(websocketpp::http::status_code::ok);
	} catch (...) {
		std::cout << "file not found: " << fileName << " for uri: " << uri << std::endl;
		con->set_status(websocketpp::http::status_code::not_found);
	}
}

/*---------------------------------------*/
std::unique_ptr<Resource> VisualizationServer::createResource(std::string key) {
	std::unique_ptr<Resource> resource {new Resource(this, key)};
	return resource;
}


/*---------------------------------------*/
bool VisualizationServer::hasWSResource(std::string const& key) const {
	return mWSResources.find(key) != mWSResources.end();
}

/*---------------------------------------*/
std::string VisualizationServer::getNextWSResourceName(std::string const& key) const {
	std::string name = key;
	//TODO better version?
	if (hasWSResource(key)) {
		int i = 0;
		do {
			name = name + std::to_string(++i);
		} while(hasWSResource(name));
	}
	return name;
}

/*---------------------------------------*/
void VisualizationServer::registerWSResource(std::string const& _key, Resource* resource) {
	auto key = _key;
	mWSResources.emplace(key, resource);
}

/*---------------------------------------*/
void VisualizationServer::registerDynamicResource(std::string const& key, DynamicCallback dynCB) {
	mDynamicResources[key] = dynCB;
}
/*---------------------------------------*/
void VisualizationServer::registerDynamicResource(std::string const& key, DynamicCallbackP dynCB) {
	mDynamicResources[key] = [=] () {
		WebPage page; //create empty page
		dynCB(page);  //set attributes and children of page via passed in lambda function
		return page.generateHTML(); //get the final page
	};
}

/*---------------------------------------*/
std::string VisualizationServer::readFile(std::string const& fileName) {
	std::ifstream in(fileName, std::ios::in | std::ios::binary);
	if (! in) {
		throw std::runtime_error(std::string("file couldn't be loaded: ") + strerror(errno) + " (" + std::to_string(errno) +")");
	}

	std::string contents;
	in.seekg(0, std::ios::end);
	contents.resize(in.tellg());
	in.seekg(0, std::ios::beg);
	in.read(&contents[0], contents.size());
	in.close();
	return contents;
}

/*---------------------------------------*/
bool VisualizationServer::fileExists(std::string const& file) {
	struct stat buffer;
	return (stat (file.c_str(), &buffer) == 0);
}
