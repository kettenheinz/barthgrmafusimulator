#pragma once

#include <memory>
#include <string>
#include <vector>

#include "jsoncpp/include/json/json.h"
//#include <json/json.h>

#include <websocketpp/common/connection_hdl.hpp>
#include <websocketpp/connection.hpp>

#include "VisualizationServer.h"
#include "drawables/Container.h"

/**
 * \class Resource
 * The resource class represents a route to a websocket endpoint.
 * Any kind of webobject can be represented by it. The server maps connections
 * to resources to allow communication between individual objects and the outside world
 * via their own connections.
 * Behind each resource is a container object which groups several html objects together.
 * \author Gregor Barth
 */

class Resource final : public drawable::Container {

public:
	Resource(VisualizationServer* server, std::string const& _key);
	~Resource();

	//communication handling
	void onConnect(websocketpp::connection_hdl hdl);
	void onDisconnect(websocketpp::connection_hdl hdl);
	void onMessage(std::string const& msg);

	void createElement(Json::Value const& value);
	void destroyElement(Json::Value const& value);
	void updateElement(Json::Value const& value);


private:
	VisualizationServer* mVServer;
	std::vector<websocketpp::connection_hdl> mConnections;

	/**
	 * Create a message containing an event for the javascript on the client side
	 * to process.
	 **/
	void sendMessage(std::string const& event, Json::Value const& value);

};
