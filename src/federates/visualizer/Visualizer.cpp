#include "federates/visualizer/Visualizer.h"

#include <chrono>
#include <ctime>
#include <map>
#include <math.h>
#include <iostream>
#include <sstream>

#include "drawables/Container.h"

using namespace std;

Visualizer::Visualizer(string const& name, string const configPath, string const somPath)
	:	Federate(name, configPath, somPath)
	, buttonState(false)
	, mFrameRate(0)
{
}

/*---------------------------------------*/

void Visualizer::init() {
	declareObjects();
	declareInteractions();
	createAgents();

	//start/stop button
	resource = vServer.createResource("/DataLabel");	
	startButton = resource->add<drawable::Button>("Start");
	startButton->setCallback( [=] () {
			//toggle the simulation
			if (buttonState == true) {
				InteractionClassHandle interH = mRtia->getInteractionClassHandle("ButtonStopSimulation");
				std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
				mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);
				buttonState = false;
				startButton->setCaption("Start");
			} else {
				InteractionClassHandle interH = mRtia->getInteractionClassHandle("ButtonStartSimulation");
				std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
				mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);
				buttonState = true;
				startButton->setCaption("Stop");
			}
	});
	label  = resource->add<drawable::Label>("Test Text for a label");

	//2d view of tank
	resourceCanv = vServer.createResource("/Canvas");	
	canvas = resourceCanv->add<drawable::Canvas>();
	canvasContent.setBackground("#000000");
	canvasContent.setSize(1000, 1000);

	//display cohesion value
	resourceCoh = vServer.createResource("/Cohesion");
	labelCohesion = resourceCoh->add<drawable::Label>("Cohesion data");

	//create communication endpoints
	vServer.registerDynamicResource("/index.html", [] (WebPage& page) {
		page.setTitle("FUSimulator");
		page.addBootstrapContainer("/DataLabel");
		page.addBootstrapContainer("/Canvas");
		page.addBootstrapContainer("/Cohesion");
	});


	vServer.run("../src/visualizer/",9008);

}

/*---------------------------------------*/
void Visualizer::update() {
	label->setText(createDisplayData());

	//redraw the canvas each frame pass
	canvasContent.clear();
	//display fps
	canvasContent.addText("#FFFFFF", 20, 20, "FPS: " + std::to_string(mFrameRate));
	
	for (auto &a : mAgents) {
			Position p = a->getPos();
			Position ot = a->getOrientation();
			canvasContent.addCircleFilled("#FF0000", p.at(0), p.at(1), 3);
//			canvasContent.addArrow("#00FF00", p.at(0), p.at(1), p.at(0) + (ot.at(0) *20), p.at(1) + (ot.at(1)*20));
			canvasContent.addLine("#00FF00", p.at(0), p.at(1), p.at(0) + (ot.at(0) *20), p.at(1) + (ot.at(1)*20), 1);
			canvasContent.addText("#FFFFFF", p.at(0), p.at(1)-5, p.toString());
		}
	canvas->setContent(canvasContent);

	labelCohesion->setText("Cohesion: " + std::to_string(cohesion));
}

/*---------------------------------------*/
void Visualizer::createAgents() {
	for (auto inst : mDiscoveredObjects) {
		Agent* agent = mAgentFactory->provideVisualizerAgent("VisualizerAgent");
		mAgents.push_back(agent);
		agent->setHandle(inst);
	}
}

/*---------------------------------------*/
std::string Visualizer::createDisplayData() {
	std::stringstream data;

	//print time
	auto cur_time = std::chrono::system_clock::now();
	std::time_t timestamp = std::chrono::system_clock::to_time_t(cur_time);
	const char * tc = ctime(&timestamp);
  string str = string {tc};
  str.pop_back(); //ctime always has \n at the end, get rid of it
	data << str << ",   ";

	//print data
/*	for (auto &a : mAgents) {
			data << a->getPos().toString() << " , ";
			data << a->getTargetPos().toString() << " , ";
			data << a->getOrientation().toString() << " , ";
	}
*/
	data << "\n";
	return data.str();
}

/*---------------------------------------*/
void Visualizer::reflectAttributeValues(ObjectInstanceHandle objH, std::vector<AttributeHandleValuePair> attributes) {
	//get correlating agent
	Agent* agent = getAgent(objH);
	//decode values from incomming data
	for (auto pair : attributes) {
		std::string attrName = pair.getName();
		if (attrName.find("pos") != std::string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			agent->setPos(p);
		} else if (attrName.find("targetPos") != string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			agent->setTargetPos(p);
		} else if (attrName.find("orientation") != string::npos) {
			Position o = mEncoder->deserialize<Position>(pair.getValue());
			agent->setOrientation(o);
		}
	}
}

/*---------------------------------------*/
void Visualizer::receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters) {
	if (mRtia->getInteractionClassName(interH) == "CohesionUpdate") {
		for (auto pair : parameters) {
			string paramName = pair.getName();
			if (paramName.find("cohesion") != string::npos) {
				double c = mEncoder->deserialize<double>(pair.getValue());	
				cohesion = c;
			}
		}
	}
	if (mRtia->getInteractionClassName(interH) == "SetFrameRate") {
		for (auto pair : parameters) {
			string paramName = pair.getName();
			if (paramName.find("fps") != string::npos) {
				mFrameRate = mEncoder->deserialize<int>(pair.getValue());	
			}
		}
	}
}

