#include "WebPage.h"

#include <sstream>

void HTMLElement::printHTML(std::stringstream& ss, int indent) const {
	std::string spaces(indent, '\t'); //we have to pad each indented line with a tab
	//opening tag
	ss << spaces << "<" + mTag;
	//print attributes
	for (auto const& a : mAttributes) {
		ss << " " << a.first << "=\"" << a.second << "\"";
	}
	ss << ">" << "\n"; //close the opening tag
	//print children
	for (auto const& c : mChildren) {
		c.printHTML(ss, indent+1);
	}
	//closing tag
	ss << spaces << "</" + mTag + ">" + "\n";
}


/*---------------------------------------*/
WebPage::WebPage() {}

/*---------------------------------------*/
WebPage::~WebPage() {}

/*---------------------------------------*/
void WebPage::addBootstrapContainer(std::string target) {
	HTMLElement element;
	element.mTag = "div";
	element.mAttributes["class"]  = "bootstrapContainer";
	element.mAttributes["target"] = target;
	mHTMLElements.emplace_back(std::move(element));
}

/*---------------------------------------*/
std::string WebPage::generateHTML() {
	std::stringstream ss;
	//we use raw string literals here to make formatting the html code easier in here
	//thus we also need to remove the leading spaces we normally use for indentation in c++ code
	ss << R"raw(
<!doctype html>
<html>
	<head>
)raw";
	ss << "<title>" << mTitle << "</title>\n";
	ss << R"raw(
    <script src='lib/jquery.min.js'></script>
    <script src='lib/jquery-ui.min.js'></script>

    <script src='drawables/label.js'></script>
    <script src='drawables/button.js'></script>
    <script src='drawables/canvas.js'></script>

    <script src='VisualizationServer.js'></script>

    <link rel="stylesheet" href="style.css" type="text/css">
  </head>

	<body>
	)raw";


	//add all html elements we created dynamically
	for (auto e : mHTMLElements) {
		e.printHTML(ss, 1);
	}

	ss << R"raw(
	</body>
</html>
)raw";
	return ss.str();
}

