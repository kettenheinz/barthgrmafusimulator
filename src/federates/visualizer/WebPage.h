#pragma once

#include <map>
#include <string>
#include <vector>

/**
 * A basic html element.
 */
struct HTMLElement {
	std::string mTag;
	std::map<std::string, std::string> mAttributes;
	std::vector<HTMLElement> mChildren;

	/* Convert a HTMLElement into a string and add it to the stringstream representing the webpage*/
	void printHTML(std::stringstream& ss, int indent) const;
};


/**
 * \class WebPage
 * This class represents the webpage which will be rendered by the visualizer.
 * It is only a scaffold which contains the required javascripts and local files.
 * All html-elements are added dynamically.
 * \author Gregor Barth
 **/

class WebPage {
public:
	WebPage();
	~WebPage();

	void addBootstrapContainer(std::string target);

	/* Return the html code as string for the server to send to the network. */
	std::string generateHTML();

	void setTitle(std::string title) {mTitle = title;}

private:
	std::string							 mTitle {"FUSimulator Visualizer"};
	std::vector<HTMLElement> mHTMLElements;

};
