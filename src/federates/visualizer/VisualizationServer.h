#pragma once

#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>

#include <fstream>
#include <functional>
#include <memory>
#include <iostream>
#include <set>
#include <streambuf>
#include <string>
#include <thread>

#include "WebPage.h"


/**
 * \class VisualizationServer
 * This is a websocket server which interacts with a local website.
 * \author Gregor Barth
 */

using WSServer       = websocketpp::server<websocketpp::config::asio>;
using connection_hdl = websocketpp::connection_hdl;

class Resource;

class VisualizationServer {
public:
		using Callback         = std::function<std::string()>;
		using DynamicCallback  = std::function<std::string()>;
		using DynamicCallbackP = std::function<void(WebPage& page)>;

		VisualizationServer();
		~VisualizationServer();

		/* Main server loop */
		void run(std::string docroot, uint16_t port);

		void getHTTP(WSServer::connection_ptr& con);

		std::unique_ptr<Resource> createResource(std::string key);
		bool hasWSResource(std::string const& key) const;
		std::string getNextWSResourceName(std::string const& key) const;
		void registerWSResource(std::string const& _key, Resource* resource);
		void registerDynamicResource(std::string const& key, DynamicCallback dynCB);
		void registerDynamicResource(std::string const& key, DynamicCallbackP dynCB);

		std::string readFile(std::string const& fileName);
		bool fileExists(std::string const& file);

		// getter
		WSServer* getWSServer() {return &mServer;}

private:
    WSServer mServer;
		std::map<WSServer::connection_ptr, Resource*> mConnectionToResource;

		std::map<std::string, Resource*> mWSResources;
		std::map<std::string, DynamicCallback>  mDynamicResources;

		mutable std::recursive_mutex mMutex;
		std::thread mThread;
		bool mIsRunning {false};

    std::string m_docroot;
		std::vector<std::string> mRootFolders; //root for all scripts and other files


    //TEMP  Telemetry data
    WSServer::timer_ptr m_timer;
		std::stringstream visualizerData;
    uint64_t m_count;
};
