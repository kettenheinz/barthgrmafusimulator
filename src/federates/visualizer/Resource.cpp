#include <websocketpp/server.hpp>
#include "Resource.h"

#include "drawables/Drawable.h"

Resource::Resource(VisualizationServer* server, std::string const& _key)
	: mVServer(server)
	, drawable::Container()
{

	auto key = mVServer->getNextWSResourceName(_key);
	mVServer->registerWSResource(key, this);
	setResource(this);
}

/*---------------------------------------*/
Resource::~Resource() {}

/*---------------------------------------*/
void Resource::onConnect(websocketpp::connection_hdl hdl) {
	mConnections.push_back(hdl);
	onConnectElement(); //call to container
}

/*---------------------------------------*/
void Resource::onDisconnect(websocketpp::connection_hdl hdl) {
	//nothing to do here atm
}

/*---------------------------------------*/
void Resource::onMessage(std::string const& msg) {
//	Json::Reader reader;
	Json::CharReaderBuilder builder;
	Json::CharReader* reader = builder.newCharReader();
	std::string errors;

	Json::Value _event;
//	reader.parse(msg, _event);
	bool parsingSuccessful = reader->parse(msg.c_str(), msg.c_str() + msg.size(), &_event, &errors);
	delete reader;

	auto address = _event["address"].asString();
	auto event   = _event["event"].asString();
	auto value   = _event["msg"];
	onEvent(address, event, value); //call to container
}

/*---------------------------------------*/
void Resource::createElement(Json::Value const& value) {
	sendMessage("createElement", value);
}
/*---------------------------------------*/
void Resource::destroyElement(Json::Value const& value) {
	sendMessage("destroyElement", value);
}
/*---------------------------------------*/
void Resource::updateElement(Json::Value const& value) {
	sendMessage("updateElement", value);
}
/*---------------------------------------*/
void Resource::sendMessage(std::string const& event, Json::Value const& value) {
	for (auto c : mConnections) {
		//on the initial creation of drawables we normally have no connections yet,
		//so check for that here
		auto root = Json::Value(Json::objectValue);
		root["event"] = event;
		root["data"]  = value;

//		Json::FastWriter jsonWriter;
//		std::string data = jsonWriter.write(root);
		Json::StreamWriterBuilder builder;
		builder.settings_["indentation"] = "";
		std::string data = Json::writeString(builder, root);

		mVServer->getWSServer()->send(c, data, websocketpp::frame::opcode::TEXT);
	}
}

