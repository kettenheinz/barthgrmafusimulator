$( document ).ready(function() {
	$(".bootstrapContainer").each( function(index, element) {
		wsSocket($(element).attr("target"), $(element));
	} );
});


var webSocketCt = 0;

//store all created web elements and their addresses in here
var elementStorage = { directPath : {} };

//main function
var wsSocket = function(path, wrapper) {
	webSocketCt += 1;
	var url = 'ws://' + document.location.host + path;
	var ws = new WebSocket(url);
	var count = webSocketCt;
	ws.onmessage = function(msg) {
		var options = JSON.parse(msg.data);

		if (options.event == "createElement") {
			var data = options.data;
			
			//send stuff back to the WSServer
			var sendEvent = function(event, message) {
				var address = data.address;
				ws.send(JSON.stringify({address: address, event: event, msg : message}));
			}
			
			var element; //blanko element
			//TODO make this a factory or something nice
      if (data.type == "label") {
        element = createLabel(data, sendEvent);
      } else if (data.type == "button") {
        element = createButton(data, sendEvent);
      } else if (data.type == "canvas") {
        element = createCanvas(data, sendEvent);
			}
			//acctually add the object to the page
      elementStorage.directPath[count + data.address] = element;
      wrapper.append(element);

    } else if (options.event == "updateElement") {
			var data = options.data;	
			var element = elementStorage.directPath[count + data.address];	
      if (data.type == "label") {
   			updateLabel(element, data);
      } else if (data.type == "button") {
   			updateButton(element, data);
      } else if (data.type == "canvas") {
   			updateCanvas(element, data);
      }

		} else {
			console.log(msg.data);
			console.log("unknown event: " + options.event);
		}
	};
	return ws;
};

//TODO rename
var applyStdCreateElement = function(element, msg) {
	if (msg.htmlid != "") {
		element.attr("id", msg.htmlid);
	}
}

//TODO rename
var applyStdUpdateElement = function(element, msg) {
	if (msg.event == "addClass") {
		element.addClass(msg.class);
		return true;
	} else if (msg.event == "removeClass") {
		element.removeClass(msg.class);
		return true;
	} else if (msg.event == "setHtmlId") {
		if (typeof msg.htmlid != "") {
			element.attr("id", msg.htmlid);
		} else {
			element.removeAttr("id");
		}
	}
	return false;
}

