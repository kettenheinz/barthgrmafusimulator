#pragma once

#include <string>

#include "federates/visualizer/VisualizerAgent.h"

/**
 * \class VisualizerAgentFactory
 * Factory for agent class instances used by the Visualizer federate.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class VisualizerAgentFactory {
	public:
		VisualizerAgentFactory(){}
		~VisualizerAgentFactory(){}

	VisualizerAgent* provideVisualizerAgent(std::string agentType) {
		VisualizerAgent* agent = new VisualizerAgent(agentType);
		return agent;
	}


};
