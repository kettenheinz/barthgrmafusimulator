#pragma once
#include <string>
#include <vector>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"

#include "util/XMLParser/XMLParser.h"

/**
 * \class Logger
 * Federate to write data from the simulation to an external file.
 * Which data is logged is specified by the subscriptions in the SOM.xml.
 * \author Gregor Barth
 * \date 08.12.2017
 */


class Logger: public Federate {
public:

	Logger(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	void clearOutputCSV();

	/**
	 * Create an ordered local storage for data we subscribed to, so we can print
	 * it in the correct order.
	 */
	void createUpdateDataPairs();

	/**
	 * Write all attribute names and extra data fields as column headers into our output .csv file.
	 * The attribute names are read from the list of subscribed attributes.
	 */
	void writeHeadersToCSV();

	/**
	 * Write data directly to a csv file.
	 * NOTE: This file is encoded in UTF-8. If office only shows chinese symbols it most likely
	 * interprets the csv file as UTF-16. Change the encoding and you are fine.
	 */
	void writeDataToCSV();

	/**
	 * 6.11 HLA service
	 */
	void reflectAttributeValues(ObjectInstanceHandle obj, std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);


private:
	std::vector<AttributeHandleValuePair> mUpdateData; //stores current value pairs
	int mFrameCounter;

	AttributeHandleValuePair* getUpdateDataByName(std::string name);
};

