#include "federates/logger/Logger.h"

#include <map>
#include <iostream>
#include <fstream>

#include <chrono>
#include <ctime>

using namespace std;

Logger::Logger(string const& name, string const configPath, string const somPath)
	:	Federate(name, configPath, somPath)
{}

/*---------------------------------------*/

void Logger::init() {
	declareObjects();
	clearOutputCSV();
	createUpdateDataPairs();
	writeHeadersToCSV();	

	mFrameCounter = 0;
}

/*---------------------------------------*/
void Logger::update() {
	mFrameCounter += 1;
	writeDataToCSV();
}


/*---------------------------------------*/
void Logger::clearOutputCSV() {
	std::ofstream ofs;
	ofs.open("SimulationLog.csv", std::ofstream::out | std::ofstream::trunc);
	ofs.close();
}

/*---------------------------------------*/
void Logger::createUpdateDataPairs() {
	for (auto h : mDiscoveredAttributes) {
		//encode the default values because new values we receive via the RTI will also be encoded
		//and in @writeDataToCSV() we decode by default
		std::string value = mEncoder->serialize<std::string>("0");
		AttributeHandleValuePair pair(h, value);
		pair.setName(mRtia->getInstanceAttributeName(h));
		mUpdateData.push_back(pair);
	}
}

/*---------------------------------------*/
void Logger::writeHeadersToCSV() {
	std::fstream file;
	file.open("SimulationLog.csv", std::ios_base::app);
	file << "Frame" << ",";
	file << "Timestamp" << ",";
	for (auto p : mUpdateData) {
		file << p.getName() << ",";
	}
	file << "\n";
	file.close();
}
/*---------------------------------------*/
void Logger::writeDataToCSV() {
	std::fstream file;
	file.open("SimulationLog.csv", std::ios_base::app);

	//print frame
	file << mFrameCounter << ",";

	//print time
	auto cur_time = std::chrono::system_clock::now();
	std::time_t timestamp = std::chrono::system_clock::to_time_t(cur_time);
	const char * tc = ctime(&timestamp);
  string str = string {tc};
  str.pop_back(); //ctime always has \n at the end, get rid of it
	file << str << ",";

	//print data
	for (AttributeHandleValuePair pair : mUpdateData ) {
		std::string attrName = pair.getName();
		if (attrName.find("pos") != std::string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			file << p.toString() << ",";
		} else if (attrName.find("targetPos") != std::string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			file << p.toString() << ",";
		} else if (attrName.find("orientation") != std::string::npos) {
			Position o = mEncoder->deserialize<Position>(pair.getValue());
			file << o.toString() << ",";
		}
	}
	file << "\n";
	file.close();
}

/*---------------------------------------*/
void Logger::reflectAttributeValues(ObjectInstanceHandle obj, std::vector<AttributeHandleValuePair> attributes) {
		for (auto attr : attributes) {
			string attrName = attr.getName();
			AttributeHandleValuePair* pair = getUpdateDataByName(attrName);
			pair->setValue(attr.getValue());
		}
}

/*---------------------------------------*/
void Logger::receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters) {

	//check interaction type
	//then store parameter values from received map
}

/*---------------------------------------*/
AttributeHandleValuePair* Logger::getUpdateDataByName(std::string name) {
	for (auto & ud : mUpdateData) {
		if (ud.getName() == name) {
			return &ud;
		}
	}
	throw UnknownObjectError();
}
