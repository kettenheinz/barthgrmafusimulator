#include "federates/terminalController/TerminalController.h"

#include <iostream>
#include <sstream>


TerminalController::TerminalController(std::string const& name, std::string const configPath, std::string const somPath)
	:	Federate(name, configPath, somPath)
	, inputRunning(false)
{
}

/*---------------------------------------*/


void TerminalController::init() {
//	declareObjects();
	declareInteractions();

	// Execute lambda asyncronously.
	fut = std::async(std::launch::async, [] {
			std::string s = "";
			std::cin >> s;
			return s;
	});

	std::chrono::seconds span (2);
	if (!inputRunning) {
		while(fut.wait_for(span) != std::future_status::ready) {
				std::cout << "Type 'start' to begin the simulation..." << std::endl;
		}
		std::string start = "start";
		if (fut.get() == start) {
			std::cout << "Input was: " << "start" << std::endl;
			InteractionClassHandle interH = mRtia->getInteractionClassHandle("ButtonStartSimulation");
			std::vector<ParameterHandleValuePair> parameterHandleValuePairSet;
			mRtia->sendInteraction(mHandle, interH, parameterHandleValuePairSet);
		} else {
			std::cout << "Type 'start' to start the simulation. \n";
		}
	}
}


/*---------------------------------------*/
/*
 * Main update loop.
 */
void TerminalController::update() {


	//get user input here
/*		std::string input = "";
		std::cout << "Please enter a valid sentence (with spaces):\n>";
		std::getline(std::cin, input);
		std::cout << "You entered: " << input << std::endl << std::endl;
*/
}

/*---------------------------------------*/
void TerminalController::receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters) {

}


