#pragma once

#include <string>
#include <vector>
#include <future>
#include <thread>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"


/**
 * \class TerminalController
 * Federate for handling user input from the terminal to control starting
 * and stopping the simulation.
 * \author Gregor Barth
 *
 */


class TerminalController: public Federate {
public:

	TerminalController(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);

private:
	bool inputRunning;
	std::future<std::string> fut;
};

