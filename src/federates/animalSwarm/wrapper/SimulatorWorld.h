#pragma once

#include "IBehaviour.h"
#include "util/Scenario/Scenario.h"

/**
 * \class SimulatorWorld
 * This is a wrapper class for the RF_interface, encapsulating a scenario object of the FUSimulator.
 * \author Gregor Barth
 * \date 14.08.2018
 */

class SimulatorWorld: public IModelWorldDescriptor{
public:
	SimulatorWorld(QObject *parent);
	SimulatorWorld(Scenario* _scenario);

	cv::Point2f pxToCm(cv::Point p);
	cv::Point2f cmToPx(cv::Point2f p);
	
	//Gets the arena width in cm
	double getWidth();

	//Gets the arena height in cm
	double getHeight();

	// Geometric functions / obstacle avoidance
	std::vector<cv::Point> getArena() const;
	std::vector<std::vector<cv::Point2f>> getObstacles();

private:
	Scenario* scenario;
};
