#pragma once

#include "IBehaviour.h"
#include "librti/Agent.h"

/**
 * \class SimulatorAgent
 * This is a wrapper class for the RF_interface, encapsulating an agent object of the FUSimulator.
 * \author Gregor Barth
 * \date 14.08.2018
 */


class SimulatorAgent: public IBody{
public:
	SimulatorAgent();

 /**
	* Uniquely identifies this body within the environment.
	*/
	int uid() const override;

	/**
	 * Centroid (in centimeters) with regards to the environment's origin point.
   */
	cv::Point2f position() const override;

	/**
	 * Forward directed unit vector ([-1,1],[-1,1]).
	 */
	cv::Vec2f orientation() const override;



	void setAgent(Agent* _agent) {agent = _agent;}
private:
	Agent* agent;
};
