#include "SimulatorWorld.h"

SimulatorWorld::SimulatorWorld(QObject *parent) {}

SimulatorWorld::SimulatorWorld(Scenario* _scenario):scenario(_scenario) {}

/*---------------------------------------*/
cv::Point2f SimulatorWorld::pxToCm(cv::Point p) {
	return static_cast<cv::Point2f>(p.x, p.y);
}
/*---------------------------------------*/
cv::Point2f SimulatorWorld::cmToPx(cv::Point2f p) {
	return p;
}


// Geometric functions / obstacle avoidance
/*---------------------------------------*/
std::vector<cv::Point> SimulatorWorld::getArena() const {
	cv::Point point;
	std::vector<cv::Point> vec;
	vec.push_back(point);
	return vec;

}
/*---------------------------------------*/
std::vector<std::vector<cv::Point2f>> SimulatorWorld::getObstacles() {
	std::vector<std::vector<cv::Point2f>> vec;
	return vec;
}


/*---------------------------------------*/
//Gets the arena width in cm
double SimulatorWorld::getWidth() { return scenario->getWidth(); }

/*---------------------------------------*/
//Gets the arena height in cm
double SimulatorWorld::getHeight() {return scenario->getHeight(); }

