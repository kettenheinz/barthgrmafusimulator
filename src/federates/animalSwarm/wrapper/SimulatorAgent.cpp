#include "SimulatorAgent.h"

#include <iostream>


SimulatorAgent::SimulatorAgent() {}

/*---------------------------------------*/
int SimulatorAgent::uid() const {
	return agent->getUid();
}

/*---------------------------------------*/
cv::Point2f SimulatorAgent::position() const {
	return cv::Point2f(agent->getPos().at(0), agent->getPos().at(1));
}

/*---------------------------------------*/
cv::Vec2f SimulatorAgent::orientation() const {
	return cv::Vec2f(agent->getOrientation().at(0), agent->getOrientation().at(1));
}

