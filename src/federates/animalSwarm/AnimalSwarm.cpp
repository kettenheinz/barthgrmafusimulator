#include "federates/animalSwarm/AnimalSwarm.h"

#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "util/Position/Position.h"
#include "util/Scenario/ScenarioObject.h"
#include "XMLParserAnimalSwarm.h"


using namespace std;

AnimalSwarm::AnimalSwarm(string const& name, string const configPath, string const somPath)
	:	Federate(name, configPath, somPath)
{
	mAgentFactory = new AnimalSwarmAgentFactory();
}

/*---------------------------------------*/

void AnimalSwarm::init() {
	//create basic random seed for agent creation
	XMLParserAnimalSwarm parser;
	//get seed, if 0 is received, use time NULL as seed
	auto seed = parser.parseAgentSeed(CONFIGPATH);
	if (seed == 0) { seed = time(NULL); }

	//seed our random number generator from config
	//use the same seed for all agents in one swarm
	swarmRng();
	swarmRng.seed(seed);
	std::cout << "Using seed: " << seed << " for agent generation \n";

	declareObjects();
	declareInteractions();
	createAgents();
}

/*---------------------------------------*/
/*
 * Main update loop.
 */
void AnimalSwarm::update() {

	for (auto &a: mAgents) {
		a->update();

		vector<AttributeHandleValuePair> attributeHandleValuePairSet;
		ObjectInstanceHandle objH = a->getHandle();
		//get all instanceAttributeHandles belonging to this objectInstance
		vector<InstanceAttributeHandle> objAttributes = mRtia->getObjectInstanceAttributes(objH);
		
		//match local variables to mHandles and set encoded local variable as value
		for (auto attrH : objAttributes) {
			string attrName = mRtia->getInstanceAttributeName(attrH);
			string value;

			if (attrName.find("pos") != string::npos) {
				value = mEncoder->serialize<Position>(a->getPos());
			} else if (attrName.find("targetPos") != string::npos) {
				value = mEncoder->serialize<Position>(a->getTargetPos());
			} else if (attrName.find("orientation") != string::npos) {
				value = mEncoder->serialize<Position>(a->getOrientation());
			}

			AttributeHandleValuePair pair(attrH, value);
			pair.setName(attrName);
			attributeHandleValuePairSet.push_back(pair);	
		}
		//update one object instance with all its instance attributes
		mRtia->updateAttributeValues(mHandle, objH, attributeHandleValuePairSet);
	}
}

/*---------------------------------------*/
void AnimalSwarm::loadScenario(std::string path) {

	//create new mScenario object
	mScenario = new Scenario();
	XMLParserAnimalSwarm parser;

	std::vector<std::map<std::string, std::string>> mScenarioObjects = parser.parseScenarioObjects(path);

	for (map<string,string> obj : mScenarioObjects) {
		double posX   = stod(obj.at("posX"));
		double posY   = stod(obj.at("posY"));
		double width  = stod(obj.at("width"));
		double height = stod(obj.at("height"));
		ScenarioObject* sObj = new ScenarioObject(Position(posX,posY), width, height);
		mScenario->addScenarioObject(sObj);
	}

	//set all scenario related values for agents
	initializeAgents();
}

/*---------------------------------------*/
void AnimalSwarm::reflectAttributeValues(ObjectInstanceHandle objH, vector<AttributeHandleValuePair> attributes) {
	//get correlating agent
	Agent* agent = getAgent(objH);
	//decode values from incomming data

	for (auto pair : attributes) {
		string attrName = pair.getName();
		if (attrName.find("pos") != string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			agent->setPos(p);
		} else if (attrName.find("targetPos") != string::npos) {
			Position p = mEncoder->deserialize<Position>(pair.getValue());
			agent->setTargetPos(p);
		} else if (attrName.find("orientation") != string::npos) {
			Position o = mEncoder->deserialize<Position>(pair.getValue());
			agent->setOrientation(o);
		}
	}
}

/*---------------------------------------*/
void AnimalSwarm::receiveInteraction(InteractionClassHandle interH, vector<ParameterHandleValuePair> parameters) {
	if (mRtia->getInteractionClassName(interH) == "LoadScenario") {
		for (auto pair : parameters) {
			string paramName = pair.getName();
			if (paramName.find("scenarioPath") != string::npos) {
				string mScenario = mEncoder->deserialize<string>(pair.getValue());	
				loadScenario(mScenario);
			}
		}
	}
}

/*---------------------------------------*/
std::vector<Agent*> AnimalSwarm::getOtherAgents(ObjectInstanceHandle agentH) {
	std::vector<Agent*> otherAgents;
	for (auto a : mAgents) {
		if (a->getHandle() != agentH) {
			otherAgents.push_back(a);
		}
	}
	return otherAgents;
}


/*---------------------------------------*/
/*
 * Create all mAgents the federate requires and let the federate register them if possible.
 */
void AnimalSwarm::createAgents() {
	XMLParserAnimalSwarm parser;
	vector<map<string,string>> config = parser.parseFederateAgents(CONFIGPATH);

	for (map<string,string> m : config) {
		string agentType = m.at("objectClass");
		int agentAmount = stoi(m.at("amount"));
		string agentPlugin = m.at("plugin");
		string agentConfig = m.at("config");

		for(int i=0; i< agentAmount; i++) { //amount to create
			Agent* agent;
			if (agentType == "FishAgent") {
				agent = mAgentFactory->provideFishAgent(agentType);
			} else if (agentType == "RoboFishAgent") {
				agent = mAgentFactory->provideRoboFishAgent(agentType);
			}
			mAgents.push_back(agent);
			registerObject(agent, agentType);
			agent->setPluginPath(agentPlugin);
			agent->setConfigPath(agentConfig);
			agent->init();
		}
	}

	for (auto a : mAgents) {
		std::vector<Agent*> otherAgents = getOtherAgents(a->getHandle());
		for (auto oA : otherAgents) {
			a->addOtherAgent(oA);
		}
	}
}

/*---------------------------------------*/
void AnimalSwarm::initializeAgents() {

	//create random number sequence based on scenario limits
	std::uniform_real_distribution<> distrW(0, mScenario->getWidth());
	std::uniform_real_distribution<> distrH(0, mScenario->getHeight());

	for (auto a : mAgents) {
		//make scenario known to all agents
		a->setScenario(mScenario);
		//initialize position values for agents
		a->setPos(Position(distrW(swarmRng), distrH(swarmRng)));
		a->setTargetPos(Position(distrW(swarmRng), distrH(swarmRng)));
	}
}
