#pragma once

#include "librti/Agent.h"
#include "rti/Exception.h"

#include "util/Position/Position.h"
#include "federates/animalSwarm/wrapper/SimulatorWorld.h"
#include "federates/animalSwarm/wrapper/SimulatorAgent.h"


class IBehaviour;

class FishAgent: public Agent {
	public:
		FishAgent(std::string _type);

		void init();
		void update();


		Position getPos() {return pos;}
		Position getTargetPos() {return targetPos;}
		Position getOrientation() {return orientation;}

		void setPos(Position p) {pos = p;};
		void setTargetPos(Position p) {targetPos = p;}
		void setOrientation(Position o) {orientation = o;}
		void setPluginPath(std::string path) {pluginPath = path;}
		void setConfigPath(std::string path) {configPath = path;}
		void setScenario(Scenario* _scenario) {scenario = _scenario;}

	private:
		Position pos;
		Position targetPos;
		Position orientation;

		std::string behaviour;
		std::string pluginPath;
		std::string configPath;
		IBehaviour* interactiveBehaviour;  //the loaded Qt behaviour plugin
		
		SimulatorAgent agent; //representation of this agent for the RF_interface
		std::shared_ptr<IBody> robot; //shared_ptr to this object as RF_interfaces object
		std::shared_ptr<std::vector<std::shared_ptr<IBody>>> robots;
		std::shared_ptr<std::vector<std::shared_ptr<IBody>>> fish;
		SimulatorWorld* world; //representation of the current scenario for the RF_interface
};
