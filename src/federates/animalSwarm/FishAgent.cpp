#include "FishAgent.h"

#include <boost/filesystem.hpp>
#include <iostream>
#include <limits.h>
#include <random>
#include <string>

//#include "SrcBehaviorLoader/PluginLoader.h"


namespace fs = boost::filesystem;
using namespace std;

FishAgent::FishAgent(std::string _type):Agent(_type),
																				orientation(1.0, 1.0) {

	//create an IBody to represent our agent
	agent = SimulatorAgent();
	agent.setAgent(this);
	robot = make_shared<SimulatorAgent>(std::move(agent));
	//create wrapper object of this agent
	robots = make_shared<vector<shared_ptr<IBody>>>();
	robots->push_back(robot);

	//prepare data of other agents
	fish =   make_shared<vector<shared_ptr<IBody>>>();

	//create a wrapper world model from our scenario data
	world = new SimulatorWorld(scenario);
}

/*---------------------------------------*/
void FishAgent::init() {

	//get the current working directory and add the plugin path
	std::string curPath = fs::current_path().string();
	std::string pPath = curPath + pluginPath;
	std::string cPath = curPath + configPath;

//	PluginLoader* loader = new PluginLoader(NULL);
/*	if	(not loader.loadPluginFromFilename(QString::fromStdString(pluginPath))) {
		cout << "Could not load behavior pluginat " << pluginPath << " for agent " << this->getUid() << "\n";
	} else {
		cout << "Success while loading shit.\n";	
	}
*/

	//use pluginloader to get behaviourFactory object
	//use factory to create behaviour instance
	//call makeHeadless, activate, etc

/*	QtPluginLoader *ploader = new QtPluginLoader(NULL);
		if (ploader->loadPluginFromFilename(pPath)) {
			//Load the plugin from dll and load the name from metadata
			IBehaviour *plug = ploader->getPluginInstance();
			QStringListModel *m = ploader->getPluginMetaData();
			QStringList s = m->stringList();
			std::string str = s.join(" ").toStdString();
			behaviour = str;
			interactiveBehaviour = plug;
			std::cout << "Loaded plugin pointer: " << plug << "\n";
			interactiveBehaviour->initHeadless(cPath);
			//get available timesteps (couzin behaviour returns an empty vector on purpose!)
			std::vector<uint32_t> supportedTimesteps = interactiveBehaviour->supportedTimesteps();

			//place the agent in the world by activating it
			interactiveBehaviour->activate(robot, world);
			if (not interactiveBehaviour->active()) {
				throw PluginActivationError();
			}
		} else {
			throw UnknownPluginError();
		}
	*/
}


/*---------------------------------------*/
/*
 * Main update loop.
 */
void FishAgent::update() {
/*
	for (auto oA : mOtherAgents) {
		SimulatorAgent otherFish = SimulatorAgent();
		otherFish.setAgent(oA);
		shared_ptr<IBody> simFish = static_pointer_cast<IBody>(make_shared<SimulatorAgent>(move(otherFish)));
		fish->push_back(simFish);
	}
*/
	uint32_t speed = 1;	

	cout << "calling agent uid: " << robot->uid() << "\n";
/*
	//call qt-plugin and execute behaviour
	RobotActionList actions = interactiveBehaviour->nextSpeeds(robots, fish, speed);

	pos = Position(50.0, 50.0);
	orientation = Position(50.0, 50.0);

	//parse all robot actions and translate them into position values
	for (auto rA : actions) {
		float speedFwd = rA->getSpeedFwd();
		float speedTrn = rA->getSpeedTrn();
		cout << "agent speed fwd: " << speedFwd << "; speed trn: " << speedTrn << "\n";
	}
*/
}

