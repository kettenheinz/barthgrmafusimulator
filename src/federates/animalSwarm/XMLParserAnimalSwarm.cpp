#include "XMLParserAnimalSwarm.h"

#include <iostream>
//#include <tinyxml2.h>
#include "util/XMLParser/tinyxml2.h"


using namespace std;
using namespace tinyxml2;

XMLParserAnimalSwarm::XMLParserAnimalSwarm(){};


/*-------------------------------------------------------*/		
vector<map<string,string>> XMLParserAnimalSwarm::parseFederateAgents(string path) {
	vector<map<string,string>> config;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
//			(eResult == 0) ? cout<<"Federate config loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLNode * nSwarm = nRoot->FirstChild()->NextSibling();
	XMLNode * nAgent = nSwarm->FirstChild();
	while (nAgent != nullptr) {
		//parse individual agent
		map<string,string> agentData;
		XMLElement * eKey = nAgent->FirstChildElement("objectClass");
		string objValue = eKey->GetText();
		agentData.insert(pair<string, string>("objectClass", objValue));
		eKey = eKey->NextSiblingElement("amount");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("amount", objValue));
		eKey = eKey->NextSiblingElement("plugin");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("plugin", objValue));
		eKey = eKey->NextSiblingElement("config");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("config", objValue));

		config.push_back(agentData);

		nAgent = nAgent->NextSibling();
	}
	return config;
}

/*-------------------------------------------------------*/		
int XMLParserAnimalSwarm::parseAgentSeed(string path) {
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
	(eResult == 0) ? cout<<"Scenario config file loaded! \n" : cout << "ERROR: " << eResult << endl;
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLElement * nSeed = nRoot->FirstChildElement("seed");
	int seed = 0;
	eResult = nSeed->QueryIntText(&seed);
	return seed;
}

/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParserAnimalSwarm::parseScenarioObjects(const string path) {
	vector<map<string,string>> scenarioObjects;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
	(eResult == 0) ? cout<<"Scenario config file loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nDocRoot = xmlDoc.FirstChild();
	//get objects root node
	XMLNode * nObjectRoot = nDocRoot->FirstChild()->NextSibling();
	//get scneario object
	XMLNode * nObject = nObjectRoot->FirstChild();
	//loop through objects
	while (nObject != nullptr) {
		map<string,string> objectData;
		XMLElement * eKey = nObject->FirstChildElement("posX");
		string objValue = eKey->GetText();
		objectData.insert(pair<string,string>("posX", objValue));
		eKey = eKey->NextSiblingElement("posY");
		objValue = eKey->GetText();
		objectData.insert(pair<string,string>("posY", objValue));
		eKey = eKey->NextSiblingElement("width");
		objValue = eKey->GetText();
		objectData.insert(pair<string,string>("width", objValue));
		eKey = eKey->NextSiblingElement("height");
		objValue = eKey->GetText();
		objectData.insert(pair<string,string>("height", objValue));
		eKey = eKey->NextSiblingElement("solid");
		objValue = eKey->GetText();
		objectData.insert(pair<string,string>("solid", objValue));

		scenarioObjects.push_back(objectData);

		nObject = nObject->NextSibling();

	}
	return scenarioObjects;
}

