#pragma once

#include <string>
#include <map>
#include <vector>


/*
 * \class XMLParser
 * A simple XML-Parser which can parse all config XML-Files of this project.
 * \author Gregor Barth
 */

class XMLParserAnimalSwarm {
public:
	XMLParserAnimalSwarm();

	/**
	 * Parse all agents to be created of this federate.
	 */
	std::vector<std::map<std::string,std::string>> parseFederateAgents(std::string path);

	/**
	 * Parse the int seed for srand().
	 */
	int parseAgentSeed(std::string path);

	/**
	 * Parse all objects included in a scenario.
	 * This representation of scenario data is not generic, therefor it is handled in here and
	 * not in the general HLA-XML-Parser.
	 */
	std::vector<std::map<std::string, std::string>> parseScenarioObjects(std::string path);

private:
};
