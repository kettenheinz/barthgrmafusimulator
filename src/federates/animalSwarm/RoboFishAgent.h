#pragma once

#include <iostream>
#include "librti/Agent.h"
#include "util/Position/Position.h"


class RoboFishAgent: public Agent {
	public:
		RoboFishAgent(std::string _type):Agent(_type),pos(Position(0.0,0.0)), targetPos(Position(0.0,0.0)), orientation(0.0, 0.0){}

		//initialize data
		void init(){}

		/*
		 * Main update loop.
		 */
		void update(){
			//call qt-plugin and execute behavior
		}

		Position getPos() {return pos;}
		Position getTargetPos() {return targetPos;}
		Position getOrientation() {return orientation;}

		void setPos(Position p) {pos = p;};
		void setTargetPos(Position p) {targetPos = p;}
		void setOrientation(Position o) {orientation = o;}
		void setPluginPath(std::string path) {pluginPath = path;}
		void setConfigPath(std::string path) {configPath = path;}
		void setScenario(Scenario* _scenario) {scenario = _scenario;}


	private:
		Position pos;
		Position targetPos;
		Position orientation; //must be other than 0.0 | 0.0 !
		std::string pluginPath;
		std::string configPath;
};
