#pragma once

#include <string>

#include "federates/animalSwarm/FishAgent.h"
#include "federates/animalSwarm/RoboFishAgent.h"

/**
 * \class AgentFactory
 * Factory for animal swarm agent class instances.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class AnimalSwarmAgentFactory {
	public:
		AnimalSwarmAgentFactory(){}
		~AnimalSwarmAgentFactory(){}

	FishAgent* provideFishAgent(std::string agentType) {
		FishAgent* agent = new FishAgent(agentType);
		return agent;
	}

	RoboFishAgent* provideRoboFishAgent(std::string agentType) {
		RoboFishAgent* agent = new RoboFishAgent(agentType);
		return agent;
	}

};
