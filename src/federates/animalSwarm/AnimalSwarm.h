#pragma once

#include <vector>
#include <random>

#include "librti/RTIAmbassador.h"
#include "librti/Federate.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"
#include "util/Scenario/Scenario.h"

#include "federates/animalSwarm/AnimalSwarmAgentFactory.h"
#include <random>

/**
 * \class AnimalSwarm
 * Federate for simulating a swarm of several fish.
 * \author Gregor Barth
 *
 */


class AnimalSwarm: public Federate {
public:

	AnimalSwarm(std::string const& name, std::string const configPath, std::string somPath);

	void init();
	void update();

	/**
	 * Load a scenario from an xml-file which specifies its parameters and contained scenario objects.
	 */
	void loadScenario(std::string path);

	/**
	 * 6.11 HLA service
	 */
	void reflectAttributeValues(ObjectInstanceHandle obj, std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH, std::vector<ParameterHandleValuePair> parameters);

	/**
	 * Get all agents except one
	 */
	std::vector<Agent*> getOtherAgents(ObjectInstanceHandle agentH);
private:
	AnimalSwarmAgentFactory* mAgentFactory;
	Scenario* mScenario;

	std::mt19937 swarmRng; //rng generator

	/**
	 * Create all agent objects, but without setting scenario related values
	 */
	void createAgents();

	/**
	 * Set scenario related values in all agents
	 * Call this after the scenario has been loaded
	 */
	void initializeAgents();
};

