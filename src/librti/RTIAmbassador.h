#pragma once
#include <string>
#include <vector>

#include "rti/Exception.h"

#include "rti/handles/FederateHandle.h"
#include "rti/handles/ObjectClassHandle.h"
#include "rti/handles/AttributeHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/handles/ParameterHandle.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"

#include "network/Connector.h"
#include "util/Encoder/Encoder.h"

/**
 * \class RTI Ambassador
 * The rti ambassador is the connection to the rti for the federate.
 * All service calls to the managing rti must go through this wrapper class.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class RTIExecution;
class FederationExecution;
class Federate;


class RTIAmbassador {
public:
	RTIAmbassador();

	/**
	 * method stub for later use with distributed simulation in a network
	 */
	void connect();
	void setConnector (Connector* c) {connector = c;}

	/**
	 * Make ourselves known to the fake network.
	 */
	void addFederateToConnector(FederateHandle h, FederateAmbassador* fedAmb);

	/*--------------- Federate Management ---------------*/

	/**
	* 4.5 HLA service
	*/
	void createFederationExecution(std::string const & federationExecutionName,
																 std::string const & fomModule);

	/**
	 * 4.6 HLA service
	 * Try to destroy the federation execution if it is empty.
	 */
	void destroyFederationExecution(std::string const & federationExecutionName);

	/**
	 * 4.9 HLA service
	 * Try to join a federation execution with the specified name if it exists.
	 */
	FederateHandle joinFederationExecution(std::string const & federationExecutionName,
																				 Federate fed);

	//4.10 HLA service
	void resignFederationExecution(std::string const & federateName);


	/*--------------- Declaration Management ---------------*/

	/**
	 * 5.2 HLA service
	 * Declare to the whole simulation that we want to provide data about the specified object class
	 * and its related attributes.
	 */
	void publishObjectClassAttributes(std::string objectClass, FederateHandle fed, std::vector<std::string> objectClassAttributes);

		/**
	 * 5.4 HLA service
	 * Declare to the whole simulation that we want to provide data about the specified interaction class
	 * and its related parameters.
	 */
	void publishInteractionClass(std::string interactionClass, FederateHandle fed, std::vector<std::string> parameters);


	/**
	 * 5.6 HLA service
	 * Declare to the whole simulation that we want to receive data updates about the provided object class
	 * and its related attributes.
	 */
	void subscribeObjectClassAttributes(std::string objectClass, FederateHandle fed, std::vector<std::string> objectClassAttributes);

	/**
	 * 5.8 HLA service
	 * Declare to the whole simulation that we want to receive interactions of the specified interaction class
	 * and its related parameters.
	 */
	void subscribeInteractionClass(std::string interactionClass, FederateHandle fed, std::vector<std::string> parameters);


	/*--------------- Object Management -----------------*/

	/**
	 * 6.8 HLA service
	 * Make a specific object we own known to the simulation.
	 */
	ObjectInstanceHandle registerObjectInstance(std::string objClassName, FederateHandle fed);

	/**
	 * 6.10 HLA service
	 * Provide a data update for one specific object instance we own to the simulation.
	 */
	void updateAttributeValues(FederateHandle fedH,
														 ObjectInstanceHandle obj,
														 std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.12 HLA service
	 * Send an interaction to all subscribing federates
	 */
	void sendInteraction(FederateHandle fedH,
											 InteractionClassHandle interH,
											 std::vector<ParameterHandleValuePair> parameters);

	/*--------------- Ownership Management ---------------*/
	/*--------------- Time Management ---------------*/

	/**
	 * 8.8 HLA service
	 * Request a new timestamp to advance our local simulation to.
	 * NOTE: There is no guarantee that this request is granted at all or when!
	 */
	void timeAdvanceRequest(FederateHandle fed);

	/*--------------- Support Services ---------------*/

	//get global handles from the rti
	FederateHandle const getFederateHandle(std::string federateName);
	ObjectClassHandle const getObjectClassHandle(std::string objClassName);
	AttributeHandle const getAttributeHandle(std::string attributeName);
	InteractionClassHandle const getInteractionClassHandle(std::string interactionName);
	ParameterHandle const getParameterHandle(std::string parameterName);
	ObjectInstanceHandle const getObjectInstanceHandle(std::string objectInstanceName);
	InstanceAttributeHandle const getInstanceAttributeHandle(std::string instAttributeName);

	std::string const getInteractionClassName(InteractionClassHandle interH);
	std::string const getParameterName(ParameterHandle paramH);
	std::string const getInstanceAttributeName(InstanceAttributeHandle instAttrH);
	std::vector<InstanceAttributeHandle> const getObjectInstanceAttributes(ObjectInstanceHandle objH);
	std::vector<ParameterHandle> const getInteractionClassParameters(InteractionClassHandle interH);

private:
	Connector* connector;

};
