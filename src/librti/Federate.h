#pragma once
#include <string>
#include <iostream>
#include <vector>

#include "network/Connector.h"
#include "rti/handles/FederateHandle.h"
#include "rti/handles/ObjectClassHandle.h"
#include "rti/handles/AttributeHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/handles/ParameterHandle.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InstanceAttributeHandle.h"
#include "librti/FederateAmbassador.h"

#include "util/XMLParser/XMLParser.h"
#include "util/Encoder/Encoder.h"

/**
 * \class Federate
 * A federate is one simulation element of a HLA-federation. It manages its data and communicates with the
 * managing RTI via its two ambassadors.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class RTIExecution;
class RTIAmbassador;
class Agent;

class Federate {
public:

	Federate(std::string const& name, std::string const configPath, std::string const somPath);

	/*------ Methods for sub clases -------*/
	virtual void init () {}
	virtual void update() {}

	/*--------------------------- Federate Management --------------------------------*/
	/**
	 * Try to join an existing federation execution. If none exists, create one.
	 */
	FederateHandle createAndJoinFederationExecution(std::string const & federationExecutionName,
																									std::string const & fomModule);

	/**
	* Resign from a federation. If we are the last member, delete the federation.
	*/
	void resignFromFederationExecution(std::string const & federationExecutionName);

	/*-------------------------- Declaration Management ----------------------------*/
	/**
	 * Parse the SOM, create and declare (publish and subscribe) all object classes with their instance attributes
	 */
	void declareObjects();

	/**
	 * Parse the SOM, publish and subsribe all interactions classes
	 */
	void declareInteractions();

	//TODO check if already published or subscribed
	void addPublishedObjectClass(ObjectClassHandle h) {mPublishedObjectClasses.push_back(h);}
	void addSubscribedObjectClass(ObjectClassHandle h) {mSubscribededObjectClasses.push_back(h);}
	void addPublishedAttribute(AttributeHandle h) {mPublishedAttributes.push_back(h);}
	void addSubscribedAttribute(AttributeHandle h) {mSubscribededAttributes.push_back(h);}
	void addPublishedInteractionClass(InteractionClassHandle h) {mPublishedInteractionClasses.push_back(h);}
	void addSubscribedInteractionClass(InteractionClassHandle h) {mSubscribededInteractionClasses.push_back(h);}
	void addPublishedParameter(ParameterHandle h) {mPublishedParameters.push_back(h);}
	void addSubscribedParameter(ParameterHandle h) {mSubscribededParameters.push_back(h);}

	/*--------------------------- Object Management --------------------------------*/
	/**
	 * 6.8 HLA service
	 * Try to register an agent we possess and connect it to its mHandle.
	 */
	void registerObject(Agent* agent, std::string objClass);

	void addRegisteredObjectInstance(ObjectInstanceHandle h) {mRegisteredObjects.push_back(h);}
	void addDiscoveredObjectInstance(ObjectInstanceHandle h) {mDiscoveredObjects.push_back(h);}
	void addRegisteredInstanceAttribute(InstanceAttributeHandle h) {mRegisteredAttributes.push_back(h);}
	void addDiscoveredInstanceAttribute(InstanceAttributeHandle h) {mDiscoveredAttributes.push_back(h);}

	/**
	 * 6.11 HLA service
	 */
	virtual void reflectAttributeValues(ObjectInstanceHandle obj,
																			std::vector<AttributeHandleValuePair> attributes) {}

	/**
	 * 6.13 HLA service
	 */
	virtual void receiveInteraction(InteractionClassHandle interH,
																	std::vector<ParameterHandleValuePair> parameters) {}



	/*--------------------------- Getters and Setters ------------------------------*/
	FederateHandle getHandle() const {return mHandle;}
	std::string getName() const {return mFederateName;}
	std::vector<ObjectClassHandle> getPublishedObjectClasses() const {return mPublishedObjectClasses;}
	std::vector<ObjectClassHandle> getSubscribedObjectClasses() const {return mSubscribededObjectClasses;}
	std::vector<AttributeHandle> getPublishedAttributes() const {return mPublishedAttributes;}
	std::vector<AttributeHandle> getSubscribedAttributes() const {return mSubscribededAttributes;}
	std::vector<InteractionClassHandle> getPublishedInteractionClasses() const {return mPublishedInteractionClasses;}
	std::vector<InteractionClassHandle> getSubscribedInteractionClasses() const {return mSubscribededInteractionClasses;}
	std::vector<ParameterHandle> getPublishedParameters() const {return mPublishedParameters;}
	std::vector<ParameterHandle> getSubscribedParameters() const {return mSubscribededParameters;}
	std::vector<ObjectInstanceHandle> getRegisteredObjects() const {return mRegisteredObjects;}
	std::vector<ObjectInstanceHandle> getDiscoveredObjects() const {return mDiscoveredObjects;}
	std::vector<InstanceAttributeHandle> getRegisteredAttributes() const {return mRegisteredAttributes;}
	std::vector<InstanceAttributeHandle> getDiscoveredAttributes() const {return mDiscoveredAttributes;}
	Agent* getAgent(ObjectInstanceHandle h);
	bool publishesObjectClass(ObjectClassHandle objClass);
	bool publishesObjectClassWithName(std::string objClassName);

	void setHandle (FederateHandle h) {mHandle = h;}
	void setConnector (Connector* c);

	bool subscribesObjectClass(ObjectClassHandle objClass);
	bool subscribesInteractionClass(InteractionClassHandle interH);


protected:
	const std::string mFederateName;
	FederateHandle mHandle;
	const std::string CONFIGPATH;
	const std::string SOMPATH;

	std::vector<Agent*> mAgents;

	FederateAmbassador* mFedAmb;
	RTIAmbassador* mRtia;
	Encoder* mEncoder;

	std::vector<ObjectClassHandle> mPublishedObjectClasses;
	std::vector<ObjectClassHandle> mSubscribededObjectClasses;
	std::vector<AttributeHandle> mPublishedAttributes;
	std::vector<AttributeHandle> mSubscribededAttributes;
	std::vector<InteractionClassHandle> mPublishedInteractionClasses;
	std::vector<InteractionClassHandle> mSubscribededInteractionClasses;
	std::vector<ParameterHandle> mPublishedParameters;
	std::vector<ParameterHandle> mSubscribededParameters;
	std::vector<ObjectInstanceHandle> mRegisteredObjects;
	std::vector<ObjectInstanceHandle> mDiscoveredObjects;
	std::vector<InstanceAttributeHandle> mRegisteredAttributes;
	std::vector<InstanceAttributeHandle> mDiscoveredAttributes;

};

