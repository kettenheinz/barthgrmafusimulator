#include "Federate.h"

#include "rti/RTIExecution.h"
#include "librti/RTIAmbassador.h"
#include "librti/Agent.h"

#include <map>
#include <iostream>
#include <vector>

using namespace std;

Federate::Federate(std::string const& name,
									 std::string const configPath,
									 std::string const somPath)
	: mFederateName(name)
	,	CONFIGPATH(configPath)
	,	SOMPATH(somPath)
{
	mFedAmb = new FederateAmbassador(this);
	mRtia = new RTIAmbassador();
	mEncoder = new Encoder();
	cout<< "Federate "+ name +" created\n";
}

/*---------------------------------------*/
void Federate::setConnector (Connector* c) {
	mRtia->setConnector(c);
}

/*---------------------------------------*/
FederateHandle Federate::createAndJoinFederationExecution(string const & federationExecutionName,
																												  string const & fomModule) {
mRtia->createFederationExecution(federationExecutionName, fomModule);
FederateHandle h = mRtia->joinFederationExecution(federationExecutionName, *this);
//make ourself known to the network/connector
mRtia->addFederateToConnector(h, mFedAmb);
return h;
}

/*---------------------------------------*/
void Federate::resignFromFederationExecution(std::string const & federationExecutionName) {
mRtia->resignFederationExecution(mFederateName);
mRtia->destroyFederationExecution(federationExecutionName);
}


/*---------------------------------------*/
void Federate::declareObjects() {
	XMLParser parser;
	vector<map<string, string>> objectClasses = parser.parseObjectClasses(SOMPATH);
	for (map<string,string> m : objectClasses) {
		string objName = m.at("name");
		string objShare = m.at("sharing");
		//declare all published object classes and attributes
		vector<map<string,string>> attributes = parser.parseObjectClassAttributes(SOMPATH, objName);
		if (objShare == "Publish" || objShare == "PublishSubscribe") {
			vector<string> objectClassAttributesPublish;
			for (map<string,string> m : attributes) {
				string attrName = m.at("name");
				string attrShare = m.at("sharing");
				if (attrShare == "Publish" || attrShare == "PublishSubscribe") {
					objectClassAttributesPublish.push_back(attrName);
				}
			}
			mRtia->publishObjectClassAttributes(objName, mHandle, objectClassAttributesPublish);
		}

		//declare all subscribed object classes and attributes
		if (objShare == "Subscribe" || objShare == "PublishSubscribe") {
			vector<string> objectClassAttributesSubscribe;
			for (map<string,string> m : attributes) {
				string attrName = m.at("name");
				string attrShare = m.at("sharing");
				if (attrShare == "Subscribe" || attrShare == "PublishSubscribe") {
					objectClassAttributesSubscribe.push_back(attrName);
				}
			}
			mRtia->subscribeObjectClassAttributes(objName, mHandle, objectClassAttributesSubscribe);
		}
	}
}

/*---------------------------------------*/
void Federate::declareInteractions() {
	XMLParser parser;
	vector<map<string, string>> interactionClasses = parser.parseInteractionClasses(SOMPATH);
	for (map<string, string> m : interactionClasses) {
		string interName = m.at("name");
		string interShare = m.at("sharing");
		//declare all published interaction classes and parameters
		vector<map<string,string>> parameters = parser.parseInteractionClassParameters(SOMPATH, interName);
		if (interShare == "Publish" || interShare == "PublishSubscribe") {
			vector<string> interactionClassParametersPublish;
			for (map<string,string> m : parameters) {
				string paramName = m.at("name");
				interactionClassParametersPublish.push_back(paramName);
			}
			mRtia->publishInteractionClass(interName, mHandle, interactionClassParametersPublish);
		}

		//declare all subscribed interaction classes and parameters
		if (interShare == "Subscribe" || interShare == "PublishSubscribe") {
			vector<string> interactionClassParametersSubscribe;
			for (map<string,string> m : parameters) {
				string paramName = m.at("name");
				//we always want all parameters shared
				interactionClassParametersSubscribe.push_back(paramName);
			}
			mRtia->subscribeInteractionClass(interName, mHandle, interactionClassParametersSubscribe);
		}
	}
}

/*---------------------------------------*/
void Federate::registerObject(Agent* agent, string objClass) {
	//register the object instance and trigger all object class subscribers to discover this
	//instance, including ourselves
	ObjectInstanceHandle h = mRtia->registerObjectInstance(objClass, mHandle);
	agent->setHandle(h); //connect the mHandle to the agent
}


/*---------------------------------------*/
Agent* Federate::getAgent(ObjectInstanceHandle h) {
	for (auto a : mAgents ) {
		if (a->getHandle() == h) {
			return a;
		}
	}
	throw UnknownObjectError();
}

/*---------------------------------------*/
bool Federate::publishesObjectClass(ObjectClassHandle objClass) {
	return find(mPublishedObjectClasses.begin(), mPublishedObjectClasses.end(), objClass) != mPublishedObjectClasses.end();
}
/*---------------------------------------*/
bool Federate::publishesObjectClassWithName(string objClass) {
	ObjectClassHandle mHandle = mRtia->getObjectClassHandle(objClass);
	vector<ObjectClassHandle>::const_iterator it;
	for (it = mPublishedObjectClasses.begin(); it != mPublishedObjectClasses.end(); ++it ) {
		if (*it == mHandle) {
			return true;
		}
	}
	return false;
}

/*---------------------------------------*/
bool Federate::subscribesObjectClass(ObjectClassHandle objClass) {
	return find(mSubscribededObjectClasses.begin(), mSubscribededObjectClasses.end(), objClass) != mSubscribededObjectClasses.end();
}

/*---------------------------------------*/
bool Federate::subscribesInteractionClass(InteractionClassHandle interH) {
	return find(mSubscribededInteractionClasses.begin(), mSubscribededInteractionClasses.end(), interH) != mSubscribededInteractionClasses.end();
}

