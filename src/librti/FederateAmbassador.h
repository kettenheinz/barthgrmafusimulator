#pragma once
#include <string>
#include <vector>

#include "rti/handles/ObjectClassHandle.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InstanceAttributeHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/AttributeHandleValuePair.h"
#include "rti/ParameterHandleValuePair.h"


/**
 * \class FederateAmbassador
 * The mFederate ambassador is the connection to a mFederate for the RTI.
 * It provides all necessary service calls and all communication from the RTI
 * to a mFederate must go through this wrapper class.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class Federate;

class FederateAmbassador {
public:
	FederateAmbassador(Federate* _mFed);
	~FederateAmbassador();

	Federate* getFederate() const {return mFed;}

	/*--------------- Declaration Management ---------------*/

	/*--------------- Object Management ---------------*/

	/**
	 * 6.9 HLA service
	 * Register handles of specific object instances other mFederates have
	 * registered in the simulation.
	 */
	void discoverObjectInstance(ObjectInstanceHandle theObject,
															ObjectClassHandle theObjectClass,
															std::string const & theObjectInstanceName,
															std::vector<InstanceAttributeHandle> attributes);

	/**
	 * 6.11 HLA service
	 * Handle incomming data of objects we subscribed to.
	 */
	void reflectAttributeValues(ObjectInstanceHandle obj,
															std::vector<AttributeHandleValuePair> attributes);

	/**
	 * 6.13 HLA service
	 */
	void receiveInteraction(InteractionClassHandle interH,
													std::vector<ParameterHandleValuePair> parameters);

	/**
	 * 6.20 HLA service
	 */
	void provideAttributeValueUpdate();

	/*--------------- Ownership Management ---------------*/

	/*--------------- Time Management ---------------*/


	// 8.13 HLA service
	void timeAdvanceGrant();


private:
	Federate* mFed;
};

