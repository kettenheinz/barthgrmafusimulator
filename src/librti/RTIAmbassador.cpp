#include "RTIAmbassador.h"

#include "rti/RTIExecution.h"
#include "librti/Federate.h"

RTIAmbassador::RTIAmbassador() {}

/*---------------------------------------*/
void RTIAmbassador::connect() {}

/*---------------------------------------*/
void RTIAmbassador::addFederateToConnector(FederateHandle h, FederateAmbassador* fedAmb) {
	connector->addFederateAmbassador(h, fedAmb);
}

/*--------------- Federate Management ---------------*/
void RTIAmbassador::createFederationExecution(std::string const & federationExecutionName,
																							std::string const & fomModule) {
	try {
		FederationExecution* fedExec = connector->getRTIExection()->createFederationExecution(federationExecutionName);
		connector->setFederationExecution(fedExec);
	} catch(FederationExecutionAlreadyExists e) {
		printException(e);
		FederationExecution* fedExec = connector->getRTIExection()->getFederationByName(federationExecutionName);
		connector->setFederationExecution(fedExec);
	}
}

/*---------------------------------------*/
void RTIAmbassador::destroyFederationExecution(std::string const & federationExecutionName) {
	try {
		connector->getRTIExection()->destroyFederationExecution(federationExecutionName);
		connector->setFederationExecution(0);
	} catch(FederationExecutionNotEmpty e) {
		printException(e);
	}
}

/*---------------------------------------*/
FederateHandle RTIAmbassador::joinFederationExecution(std::string const & federationExecutionName,
																											Federate fed) {
	FederateHandle h = connector->getFederationExecution()->join(fed);
	return h;
}

/*---------------------------------------*/
void RTIAmbassador::resignFederationExecution(std::string const & federateName) {
	try {
		connector->getFederationExecution()->resign(federateName);
	} catch(FederateNotExecutionMember e) {
		printException(e); //just dont resign
	}
}

/*--------------- Declaration Management ---------------*/
void RTIAmbassador::publishObjectClassAttributes(std::string objectClass, FederateHandle fed, std::vector<std::string> objectClassAttributes) {
	connector->getFederationExecution()->publishObjectClassAttributes(objectClass, fed, objectClassAttributes);
}

/*---------------------------------------*/
void RTIAmbassador::publishInteractionClass(std::string interactionClass, FederateHandle fed, std::vector<std::string> parameters) {
	connector->getFederationExecution()->publishInteractionClass(interactionClass, fed, parameters);
}

/*---------------------------------------*/
void RTIAmbassador::subscribeObjectClassAttributes(std::string objectClass, FederateHandle fed, std::vector<std::string> objectClassAttributes) {
	connector->getFederationExecution()->subscribeObjectClassAttributes(objectClass, fed, objectClassAttributes);
}

/*---------------------------------------*/
void RTIAmbassador::subscribeInteractionClass(std::string interactionClass, FederateHandle fed, std::vector<std::string> parameters) {
	connector->getFederationExecution()->subscribeInteractionClass(interactionClass, fed, parameters);
}

/*--------------- Object Management -----------------*/
ObjectInstanceHandle RTIAmbassador::registerObjectInstance(std::string objClassName, FederateHandle fed) {
		return connector->getFederationExecution()->registerObjectInstance(objClassName, fed);		
}

/*---------------------------------------*/
void RTIAmbassador::updateAttributeValues(FederateHandle fedH,
																					ObjectInstanceHandle obj,
																					std::vector<AttributeHandleValuePair> attributes) {
	connector->getFederationExecution()->updateAttributeValues(fedH, obj, attributes);
}

/*---------------------------------------*/
void RTIAmbassador::sendInteraction(FederateHandle fedH,
																		InteractionClassHandle interH,
																		std::vector<ParameterHandleValuePair> parameters) {
	connector->getFederationExecution()->sendInteraction(fedH, interH, parameters);
}


/*--------------- Time Management ---------------*/
void RTIAmbassador::timeAdvanceRequest(FederateHandle fed) {
	connector->getFederationExecution()->advanceTime(fed);
}

/*--------------- Support Services ---------------*/
FederateHandle const RTIAmbassador::getFederateHandle(std::string federateName) {
	return connector->getFederationExecution()->getFederateHandle(federateName);
}
ObjectClassHandle const RTIAmbassador::getObjectClassHandle(std::string objClassName) {
	return connector->getFederationExecution()->getObjectClassHandle(objClassName);
}
AttributeHandle const RTIAmbassador::getAttributeHandle(std::string attributeName) {
	return connector->getFederationExecution()->getAttributeHandle(attributeName);
}
InteractionClassHandle const RTIAmbassador::getInteractionClassHandle(std::string interactionName) {
	return connector->getFederationExecution()->getInteractionClassHandle(interactionName);
}
ParameterHandle const RTIAmbassador::getParameterHandle(std::string parameterName) {
	return connector->getFederationExecution()->getParameterHandle(parameterName);
}
ObjectInstanceHandle const RTIAmbassador::getObjectInstanceHandle(std::string objectInstanceName) {
	return connector->getFederationExecution()->getObjectInstanceHandle(objectInstanceName);
}
InstanceAttributeHandle const RTIAmbassador::getInstanceAttributeHandle(std::string instAttributeName) {
	return connector->getFederationExecution()->getInstanceAttributeHandle(instAttributeName);
}


std::string const RTIAmbassador::getInteractionClassName(InteractionClassHandle interH) {
	return connector->getFederationExecution()->getInteractionClassName(interH);
}
std::string const RTIAmbassador::getParameterName(ParameterHandle paramH) {
	return connector->getFederationExecution()->getParameterName(paramH);
}
std::string const RTIAmbassador::getInstanceAttributeName(InstanceAttributeHandle instAttrH) {
	return connector->getFederationExecution()->getInstanceAttributeName(instAttrH);
}

std::vector<InstanceAttributeHandle> const RTIAmbassador::getObjectInstanceAttributes(ObjectInstanceHandle objH) {
	return connector->getFederationExecution()->getObjectInstanceAttributes(objH);
}

std::vector<ParameterHandle> const RTIAmbassador::getInteractionClassParameters(InteractionClassHandle interH) {
	return connector->getFederationExecution()->getInteractionClassParameters(interH);
}

