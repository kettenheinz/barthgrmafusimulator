#include "FederateAmbassador.h"
#include "librti/Federate.h"

#include <iostream>

FederateAmbassador::FederateAmbassador(Federate* _mFed):mFed(_mFed) {}

/*------------------ Object Management ----------------------*/

	void FederateAmbassador::discoverObjectInstance(ObjectInstanceHandle theObject,
															ObjectClassHandle theObjectClass,
															std::string const & theObjectInstanceName,
															std::vector<InstanceAttributeHandle> attributes) {
		//TODO check HLA what else to do here with all the params
		//TODO check if we already discovered the object
		
		mFed->addDiscoveredObjectInstance(theObject);
		std::cout << getFederate()->getName() << " discovered object " << theObjectInstanceName << "\n";
		for (auto attr : attributes) {
			mFed->addDiscoveredInstanceAttribute(attr);
		}
	}

/*---------------------------------------*/
	void FederateAmbassador::reflectAttributeValues(ObjectInstanceHandle obj,
																									std::vector<AttributeHandleValuePair> attributes) {
		getFederate()->reflectAttributeValues(obj, attributes);
	}

/*---------------------------------------*/
	void FederateAmbassador::receiveInteraction(InteractionClassHandle interH,
																							std::vector<ParameterHandleValuePair> parameters) {
		getFederate()->receiveInteraction(interH, parameters);
	}

/*---------------------------------------*/
	void FederateAmbassador::provideAttributeValueUpdate() {

	}

/*------------------ Time Management ----------------------*/
	void FederateAmbassador::timeAdvanceGrant() {
		getFederate()->update();
	}
