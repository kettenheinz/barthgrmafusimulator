#pragma once

#include <string>
#include <vector>

#include "rti/handles/ObjectInstanceHandle.h"
#include "util/Position/Position.h"
#include "util/Scenario/Scenario.h"


/**
 * \class Agent
 * This is an interface defining the basic functionality for an agent.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class Agent {
	public:
		Agent(std::string type):agentType(type){}
		~Agent(){}

		virtual void init()=0;
		virtual void update()=0;

		virtual Position getPos()=0;
		virtual Position getTargetPos()=0;
		virtual Position getOrientation()=0;
		
		virtual void setPos(Position p)=0;
		virtual void setTargetPos(Position p)=0;
		virtual void setOrientation(Position o)=0;
		virtual void setPluginPath(std::string path)=0;
		virtual void setConfigPath(std::string path)=0;
		virtual void setScenario(Scenario* _scenario)=0;

		//getter and setter
		ObjectInstanceHandle getHandle() {return handle;}
		std::string getAgentType(){return agentType;}
		
		int getUid() {
			return handle.getID();
		}

		void setHandle(ObjectInstanceHandle _handle) {handle = _handle;}
		void addOtherAgent(Agent* a) {mOtherAgents.push_back(a);}

	protected:
		ObjectInstanceHandle handle;
		std::string agentType;
		Scenario* scenario;
		std::vector<Agent*> mOtherAgents;
};
