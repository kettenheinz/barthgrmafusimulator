#pragma once

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>

#include <iostream>
#include <sstream>
#include <string>
#include <array>
#include <tuple>

using namespace boost::archive;


class Position {
	private:
		std::array<double, 2> mPos;
		friend class boost::serialization::access;

		template <typename Archive>
		void serialize(Archive &ar, const unsigned int version) {
			ar & mPos[0];
			ar & mPos[1];
		}

	public:
		Position(double x, double y);
		Position(std::tuple<double, double> _pos);
		Position();

		auto operator+(Position const& pos) const -> Position;
		auto operator-(Position const& pos) const -> Position;
		auto operator+=(Position const& pos) -> Position;
		auto operator-=(Position const& pos) -> Position;
		auto operator-() const -> Position;
		auto operator*(double s) -> Position;
		auto operator/(double s) -> Position;

		auto at(int idx) const -> double const&;

		//utility functions
		auto normalize() const -> double;
		auto dot(Position pos) const -> double;
		auto distanceTo(Position pos) const -> double;

		auto toString() const -> std::string;

	};
