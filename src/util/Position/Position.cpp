#include "Position.h"

#include <cmath>

	/*--- Constructors ---*/
	Position::Position(double x, double y) {
		mPos[0] = x;
		mPos[1] = y;
	}

	Position::Position(std::tuple<double, double> _pos) {
		mPos[0] = std::get<0>(_pos);
		mPos[1] = std::get<1>(_pos);
	}

	Position::Position():Position(0,0) {}


	/*--- Operators ---*/
	auto Position::operator+(Position const& pos) const -> Position {
		return Position(mPos[0] + pos.mPos[0], mPos[1] + pos.mPos[1]);
	}

	auto Position::operator-(Position const& pos) const -> Position {
		return Position(mPos[0] - pos.mPos[0], mPos[1] - pos.mPos[1]);
	}

	auto Position::operator+=(Position const& pos) -> Position {
		mPos[0] += pos.mPos[0];
		mPos[1] += pos.mPos[1];
		return *this;
	}

	auto Position::operator-=(Position const& pos) -> Position {
		mPos[0] -= pos.mPos[0];
		mPos[1] -= pos.mPos[1];
		return *this;
	}

	auto Position::operator-() const -> Position {
		return Position(-mPos[0], -mPos[1]);
	}

	auto Position::operator*(double s) -> Position {
		return Position(mPos[0]*s, mPos[1]*s);
	}

	auto Position::operator/(double s) -> Position {
		return Position(mPos[0] / s, mPos[1] / s);
	}

	auto Position::at(int idx) const -> double const& {
		if (idx < 0 || idx > 2) throw std::runtime_error("invalid index: " + std::to_string(idx));
		return mPos[idx];
	}

	auto Position::normalize() const -> double {
		return std::sqrt(mPos[0] * mPos[0] + mPos[1] * mPos[1]);
	}

	auto Position::dot(Position pos) const -> double {
		return (mPos[0] * pos.mPos[0] + mPos[1] * pos.mPos[1]);
	}

	auto Position::distanceTo(Position pos) const -> double {
		double diffX = mPos[0] - pos.mPos[0];
		double diffY = mPos[1] - pos.mPos[1];
		return std::sqrt((diffX * diffX) + (diffY * diffY) );
	}


	auto Position::toString() const -> std::string {
		std::string str = std::to_string((double)mPos[0]) + "|" + std::to_string((double)mPos[1]);
		return str;
	}
