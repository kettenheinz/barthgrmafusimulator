#include "XMLParser.h"

#include <iostream>
//#include <tinyxml2.h>
#include "util/XMLParser/tinyxml2.h"

using namespace std;
using namespace tinyxml2;

XMLParser::XMLParser(){};

/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParser::parseFederateConfig(string path) {
	vector<map<string, string>> config;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(path.c_str());
//			(eResult == 0) ? cout<<"Federate config loaded! \n" : cout << "ERROR: " << eResult << endl;

	//parse document
	XMLNode * nRoot = xmlDoc.FirstChild();
	XMLNode * nSwarm = nRoot->FirstChild();
	XMLNode * nAgent = nSwarm->FirstChild();
	while (nAgent != nullptr) {
		//parse individual agent
		map<string,string> agentData;
		XMLElement * eKey = nAgent->FirstChildElement("objectClass");
		string objValue = eKey->GetText();
		agentData.insert(pair<string, string>("objectClass", objValue));
		eKey = eKey->NextSiblingElement("amount");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("amount", objValue));
		eKey = eKey->NextSiblingElement("plugin");
		objValue = eKey->GetText();
		agentData.insert(pair<string, string>("plugin", objValue));
		config.push_back(agentData);

		nAgent = nAgent->NextSibling();
	}
	return config;
}

/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParser::parseObjectClasses(const string SOMPATH) {
	vector<map<string, string>> vecObjectClasses;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(SOMPATH.c_str());
	//(eResult == 0) ? cout<<"SOM loaded! \n" : cout << "ERROR: " << eResult << endl;

	XMLNode * nDocRoot = xmlDoc.FirstChild();
	//get the HLAObjectRoot node
	XMLNode * nObjectClassRoot = nDocRoot->FirstChild()->NextSibling()->FirstChild();
	//get object classes
	XMLNode * nObjectClass = nObjectClassRoot->FirstChildElement("name")->NextSiblingElement();

	while (nObjectClass != nullptr) {
		map<string,string> objectClassData;
		XMLElement * eKey = nObjectClass->FirstChildElement("name");
		string objValue = eKey->GetText();
		objectClassData.insert(pair<string,string>("name",objValue));	
		eKey = eKey->NextSiblingElement("sharing");
		objValue = eKey->GetText();
		objectClassData.insert(pair<string,string>("sharing",objValue));
		vecObjectClasses.push_back(objectClassData);

		nObjectClass = nObjectClass->NextSibling();
	}
	return vecObjectClasses;
}

/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParser::parseObjectClassAttributes(const string SOMPATH,
																																 const string  objectClassName) {
	vector<map<string, string>> attributes;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(SOMPATH.c_str());
	//(eResult == 0) ? cout<<"SOM loaded! \n" : cout << "ERROR: " << eResult << endl;

	XMLNode * nDocRoot = xmlDoc.FirstChild();
	//get the HLAObjectRoot node
	XMLNode * nObjectClassRoot = nDocRoot->FirstChild()->NextSibling()->FirstChild();
	//get object classes
	XMLNode * nObjectClass = nObjectClassRoot->FirstChildElement("name")->NextSiblingElement();

	//loop through object classes
	while (nObjectClass != nullptr) {
		XMLElement * eKey = nObjectClass->FirstChildElement("name");
		//get attribtues if object class found
		if (eKey->GetText() == objectClassName  ) {
			//get to attribute node
			XMLNode* nAttribute = eKey->NextSibling()->NextSibling()->NextSibling();
			//while loop over attributes
			while (nAttribute != nullptr) {
				map<string, string> attributeData;
				eKey = nAttribute->FirstChildElement("name");
				string attrValue = eKey->GetText();
				attributeData.insert(pair<string,string>("name",attrValue));
				eKey = eKey->NextSiblingElement("dataType");
				attrValue = eKey->GetText();
				attributeData.insert(pair<string,string>("dataType",attrValue));
				eKey = eKey->NextSiblingElement("sharing");
				attrValue = eKey->GetText();
				attributeData.insert(pair<string,string>("sharing",attrValue));
				attributes.push_back(attributeData);

				nAttribute = nAttribute->NextSibling();
			}
		}
		nObjectClass = nObjectClass->NextSibling();
	}
	return attributes;
}

/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParser::parseInteractionClasses(const string SOMPATH) {
	vector<map<string, string>> vecInteractionClasses;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(SOMPATH.c_str());
	(eResult == 0) ? cout<<"SOM loaded! \n" : cout << "ERROR: " << eResult << endl;

	XMLNode * nDocRoot = xmlDoc.FirstChild();
	//get the HLAInteractionRoot node
	XMLNode * nInteractionClassRoot = nDocRoot->FirstChild()->NextSibling()->NextSibling()->FirstChild();
	//get interaction classes
	XMLNode * nInteractionClass = nInteractionClassRoot->FirstChildElement("name")->NextSiblingElement();

	while (nInteractionClass != nullptr) {
		map<string,string> interactionClassData;
		XMLElement * eKey = nInteractionClass->FirstChildElement("name");
		string objValue = eKey->GetText();
		interactionClassData.insert(pair<string,string>("name",objValue));	
		eKey = eKey->NextSiblingElement("sharing");
		objValue = eKey->GetText();
		interactionClassData.insert(pair<string,string>("sharing",objValue));
		vecInteractionClasses.push_back(interactionClassData);

		nInteractionClass = nInteractionClass->NextSibling();
	}
	return vecInteractionClasses;

}


/*-------------------------------------------------------*/		
vector<map<string, string>> XMLParser::parseInteractionClassParameters(const string SOMPATH,
																																			const	string interactionClassName) {
	vector<map<string, string>> parameters;
	XMLDocument xmlDoc;
	XMLError eResult = xmlDoc.LoadFile(SOMPATH.c_str());
	//(eResult == 0) ? cout<<"SOM loaded! \n" : cout << "ERROR: " << eResult << endl;


	XMLNode * nDocRoot = xmlDoc.FirstChild();
	//get the HLAInteractionRoot node
	XMLNode * nInteractionClassRoot = nDocRoot->FirstChild()->NextSibling()->NextSibling()->FirstChild();
	//get interaction classes
	XMLNode * nInteractionClass = nInteractionClassRoot->FirstChildElement("name")->NextSiblingElement();

	//loop through interaction classes
	while (nInteractionClass != nullptr) {
		XMLElement * eKey = nInteractionClass->FirstChildElement("name");
		//get parameters if interaction class found
		if (eKey->GetText() == interactionClassName  ) {
			//get to parameters node
			XMLNode* nParameter = eKey->NextSibling()->NextSibling()->NextSibling();
			//while loop over parameters
			while (nParameter != nullptr) {
				map<string, string> parameterData;
				eKey = nParameter->FirstChildElement("name");
				string paramValue = eKey->GetText();
				parameterData.insert(pair<string,string>("name",paramValue));
				eKey = eKey->NextSiblingElement("dataType");
				paramValue = eKey->GetText();
				parameterData.insert(pair<string,string>("dataType",paramValue));
				parameters.push_back(parameterData);

				nParameter = nParameter->NextSibling();
			}
		}
		nInteractionClass = nInteractionClass->NextSibling();
	}
	return parameters;

}

