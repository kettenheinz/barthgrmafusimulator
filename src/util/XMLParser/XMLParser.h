#pragma once

#include <iostream>
#include <string>
#include <map>
#include <vector>

/*
 * \class XMLParser
 * A simple XML-Parser which can parse all config XML-Files of this project.
 * \author Gregor Barth
 * \date 12.12.2017
 */

class XMLParser {
public:
XMLParser();

	/**
	 * Parses all config values for a federate.
	 */
	std::vector<std::map<std::string,std::string>> parseFederateConfig(std::string path);

	/**
	 * Parses all object classes of this federate and their settings.
	 */
	std::vector<std::map<std::string,std::string>> parseObjectClasses(const std::string SOMPATH);

	/**
	 * Parses all attributes of an object class and their settings.
	 */
	std::vector<std::map<std::string,std::string>> parseObjectClassAttributes(const std::string SOMPATH,
																																						const std::string objectClassName);

	/**
	 * Parses all interaction classes of this federate and their settings.
	 */
	std::vector<std::map<std::string,std::string>> parseInteractionClasses(const std::string SOMPATH);

	/**
	 * Parses all parameters of an interaction class and their settings.
	 */
	std::vector<std::map<std::string,std::string>> parseInteractionClassParameters(const std::string SOMPATH,
																																						const std::string interactionClassName);

private:
};
