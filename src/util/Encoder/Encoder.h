#pragma once

#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>
#include <sstream>
#include <string>
#include <utility>
#include <array>
#include <memory>
#include <type_traits>
#include <iostream>
#include <iomanip>

#include "util/Position/Position.h"

/**
 * This class encodes and decodes variables to various formats for network communication.
 * \author Gregor Barth
 */

using namespace boost::archive;

using byte = unsigned char ;

class Encoder {
private:
	friend class boost::serialization::access;
public:
	Encoder() {}
	~Encoder(){}


	/**
	 * Serizalise to binary format.
	 */
	template<typename T>
		std::string serialize(T object) {
			std::stringstream ss;
			binary_oarchive oa{ss};
			oa << object;
			return ss.str();
		}


	/**
	 * Deserizalise from binary format.
	 */
	template<typename T>
		T deserialize(std::string s) {
			std::stringstream ss;
			ss << s;
			binary_iarchive ia{ss};
			T obj;
			ia >> obj;
			return obj;
		}



	/**
	 * Convert any object into a byte array.
	 * Warning: Different operating systems use different endians!
	 *
	 * usage example:
	 * double d = 123.456789 ;
	 * const auto bytes = to_bytes(d) ;
	 * std::cout << std::hex << std::setfill('0') ;
	 * for( byte b : bytes ) std::cout << std::setw(2) << int(b) << ' ' ;
	 * std::cout << '\n' ;
	 * d = 0 ;
	 * from_bytes( bytes, d ) ;
	 * std::cout << std::fixed << d << '\n' ;
	 *
	 */
	template< typename T >
	std::array< byte, sizeof(T) >  to_bytes( const T& object )
	{
			std::array< byte, sizeof(T) > bytes ;

			const byte* begin = reinterpret_cast< const byte* >( std::addressof(object) ) ;
			const byte* end = begin + sizeof(T) ;
			std::copy( begin, end, std::begin(bytes) ) ;

			return bytes ;
	}

	/**
	 * Convert a byte array back to its original object form. See above for usage.
	 */
	template< typename T >
	T& from_bytes( const std::array< byte, sizeof(T) >& bytes, T& object )
	{
			// http://en.cppreference.com/w/cpp/types/is_trivially_copyable
			static_assert( std::is_trivially_copyable<T>::value, "not a TriviallyCopyable type" ) ;

			byte* begin_object = reinterpret_cast< byte* >( std::addressof(object) ) ;
			std::copy( std::begin(bytes), std::end(bytes), begin_object ) ;

			return object ;
	}

};
