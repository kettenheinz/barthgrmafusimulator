#pragma once

#include <vector>
#include <string>

#include "ScenarioObject.h"

/**
 * \class Scenario
 * \author Gregor Barth
 * \date 14.12.2017
 */

class Scenario {
public:
	Scenario() {}
	~Scenario() {}

	void addScenarioObject (ScenarioObject* obj) {mScenarioObjects.push_back(obj);	}

	std::vector<ScenarioObject*> getScenarioObjects() {return mScenarioObjects;}
	//TODO calculate from objects
	double getWidth()  {return 1000.0;}
	double getHeight() {return 1000.0;}

	auto toString() const -> std::string {
		std::string str = "Scenario: \n";
		for (auto obj : mScenarioObjects) {
			str = str + obj->toString();
		}
		return str;
	}

private:
	std::vector<ScenarioObject*> mScenarioObjects;
};
