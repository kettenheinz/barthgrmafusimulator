#pragma once

#include <string>
#include "util/Position/Position.h"

/**
 * \class Scenario Object
 * \author Gregor Barth
 * \date 14.12.2017
 */

class ScenarioObject {
public:
	ScenarioObject(Position _pos, double _width, double _height):pos(_pos),
																															 width(_width),
																															 height(_height)
	{}

	~ScenarioObject() {}

	Position getPosition() { return pos; }
	double getWidth() {return width; }
	double getHeight() {return height; }

	auto toString() const -> std::string {
		std::string str = "Object: " + pos.toString() +
											" Width: " + std::to_string((int)width) +
											" Height " + std::to_string((int)height) +
											"\n";
		return str;
	}

private:	
	Position pos;
	double width;
	double height;
};
