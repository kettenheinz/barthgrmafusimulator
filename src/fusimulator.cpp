#include <chrono>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <stdexcept>
#include <string>
#ifdef __linux__ 
//unistd does not exist on windows and does not seem to be required.
#include <unistd.h>
#endif

#include <boost/program_options.hpp>
//#include <boost/program_options/options_description.hpp>
//#include <boost/program_options/parsers.hpp>
//#include <boost/program_options/variables_map.hpp>
#include <boost/tokenizer.hpp>
#include <boost/token_functions.hpp>
#include "util/BoostFix.h" //fix boost bug under windows

#include "rti/RTIExecution.h"
#include "rti/FederationExecution.h"
#include "rti/HandleFactory.h"
#include "rti/Exception.h"

#include "network//Connector.h"

#include "federates/animalSwarm/AnimalSwarm.h"
#include "federates/logger/Logger.h"
#include "federates/logPlayer/LogPlayer.h"
#include "federates/master/Master.h"
#include "federates/statisticsGenerator/StatisticsGenerator.h"
#include "federates/terminalController/TerminalController.h"
#include "federates/visualizer/Visualizer.h"


/**
* \class Fusim
* \author Gregor Barth
*
* Main class for the fusimulator.
* run with ./bin/fusimulator <number of passes>
*/

int main(int argc, char** argv) {

	int runCount = 10000;
	bool headless = false;
	bool replay = false;


	//start measuring execution time
	std::chrono::steady_clock::time_point timeBegin = std::chrono::steady_clock::now();

	//parse arguments
	//Taken from http://www.boost.org/doc/libs/1_64_0/libs/program_options/example/option_groups.cpp
	using namespace boost;
	using namespace boost::program_options;

	try {
		options_description general("General options");
		general.add_options()
			("help", "Produce this help message")
			("headless", "Run the simulation without visualizer")
			("replay", "Run the simulation with data from a log")
			("runCount", value<int>(), "How many iterations shall the simulation run")
		;
		// Declare an options description instance which will include all the options
		options_description all("Allowed options");
		all.add(general);
		// Declare an options description instance which will be shown to the user
		options_description visible("Allowed options");
		visible.add(general);

		variables_map vm;
		store(parse_command_line(argc, argv, all), vm);

		if (vm.count("help")) {
			std::cout << visible;
			exit(0);
		}
		if (vm.count("headless")) {
			headless = true;
		}
		if (vm.count("replay")) {
			replay = true;
		}
		if (vm.count("runCount")) {
			runCount = vm["runCount"].as<int>();
		}

		//simulate a network layer
		Connector* connector = new Connector();
		RTIExecution* rtiExec = new RTIExecution();
		connector->setRTIExecution(rtiExec);

		Master fedMaster("Master",
										 "src/federates/master/configMaster.xml",
										 "src/federates/master/SOM.xml" );
		fedMaster.setConnector(connector);
		FederateHandle h2 = fedMaster.createAndJoinFederationExecution("animalSwarm", "Fom");
		fedMaster.setHandle(h2);

		//set connector separately and not in federate constructor to ensure loose coupling
		rtiExec->getFederationByName("animalSwarm")->setConnector(connector);
		fedMaster.setRunCount(runCount);
		fedMaster.init();

		// Use data from a log file if the replay option is chosen.
		// A regular simulation generates its own data from the AnimalSwam federate.
		AnimalSwarm fedSwarm("AnimalSwarm",
												 "src/federates/animalSwarm/configSwarm.xml",
												 "src/federates/animalSwarm/SOM.xml");
		//assign the name of the fed we want to replay
		LogPlayer fedLogPlayer("AnimalSwarm",
													 "src/federates/logPlayer/configLogPlayer.xml",
													 "src/federates/logPlayer/SOM.xml");
		if (replay) {
			fedLogPlayer.setConnector(connector);
			FederateHandle h2 = fedLogPlayer.createAndJoinFederationExecution("animalSwarm", "Fom");
			fedLogPlayer.setHandle(h2);
			fedLogPlayer.init();
		} else {
			fedSwarm.setConnector(connector);
			FederateHandle h = fedSwarm.createAndJoinFederationExecution("animalSwarm", "Fom");
			fedSwarm.setHandle(h);
			fedSwarm.init();
		}

		// A headless simulation is started via terminal input.
		// Normal simulation runs are controlled via the webGUI of the visualizer.
		TerminalController fedTerm("TerminalController", "", "src/federates/terminalController/SOM.xml");
		Visualizer fedVisu("Visualizer","","src/federates/visualizer/SOM.xml");
		if (headless) {
			fedTerm.setConnector(connector);
			FederateHandle h5 = fedTerm.createAndJoinFederationExecution("animalSwarm", "Fom");
			fedTerm.setHandle(h5);
			fedTerm.init();
		} else {
			fedVisu.setConnector(connector);
			FederateHandle h3 = fedVisu.createAndJoinFederationExecution("animalSwarm", "Fom");
			fedVisu.setHandle(h3);
			fedVisu.init();
		}

		Logger fedLog("Logger","","src/federates/logger/SOM.xml");
		fedLog.setConnector(connector);
		FederateHandle h4 = fedLog.createAndJoinFederationExecution("animalSwarm","Fom");
		fedLog.setHandle(h4);
		fedLog.init();

		StatisticsGenerator fedStat("StatisticsGenerator","","src/federates/statisticsGenerator/SOM.xml");
		fedStat.setConnector(connector);
		FederateHandle h6 = fedStat.createAndJoinFederationExecution("animalSwarm","Fom");
		fedStat.setHandle(h6);
		fedStat.init();

		//call this after all federates have initialized
		fedMaster.loadScenario();

		std::cout << "\n----------- Finished initialization of federates! ------------\n";

		//somehow get the master running in here
		while (true) {
			fedMaster.update();
		}

		std::cout << "\n----------- Finished simulation, shutting down! ------------\n";
		fedMaster.resignFromFederationExecution("animalSwarm");
/*		if (replay) {
			fedLogPlayer.resignFromFederationExecution("animalSwarm");
		} else {
			fedSwarm.resignFromFederationExecution("animalSwarm");
		}
*/
/*		if (headless) {
			fedTerm.resignFromFederationExecution("animalSwarm");
		} else {
			fedVisu.resignFromFederationExecution("animalSwarm");
		}
*/
		fedLog.resignFromFederationExecution("animalSwarm");
		fedStat.resignFromFederationExecution("animalSwarm");



		} catch (std::exception const& e) {
			printException(e);
			return EXIT_FAILURE;
		} catch (const char* msg) {
			std::cerr << msg << "\n";
			return EXIT_FAILURE;
		}

	//measure execution time
	std::chrono::steady_clock::time_point timeEnd= std::chrono::steady_clock::now();
	std::cout << "Execution time (sec) was: " << std::chrono::duration_cast<std::chrono::seconds>(timeEnd - timeBegin).count() <<std::endl;
	return EXIT_SUCCESS;
}
