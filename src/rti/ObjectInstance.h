#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/FederateHandle.h"

/**
 * \class Object Instance
 * A HLA object instance is a generic representation of a specific object which is communicated
 * in the simulation.
 * It generalizes the object into a format for easy communiction based on the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017 
 */

class ObjectInstance {
public:
	ObjectInstance(){}
	ObjectInstance(ObjectInstanceHandle h, std::string name, FederateHandle fed)
		: mHandle(h)
		,	mName(name)
		,	mOwner(fed)
	{}
	
	~ObjectInstance(){}

	ObjectInstanceHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}
	FederateHandle const getOwner() {return mOwner;}
	std::vector<InstanceAttributeHandle> const getAttributes() {return mAttributes;}

	void setHandle(ObjectInstanceHandle handle) {mHandle = handle;}
	void setName(std::string name) {mName = name;}
	void setOwner(FederateHandle owner) {mOwner = owner;}
	void addAttributeHandle(InstanceAttributeHandle attribute) {mAttributes.push_back(attribute);}

	bool hasAttribute(InstanceAttributeHandle attrH) {
		if (std::find(mAttributes.begin(), mAttributes.end(), attrH) != mAttributes.end() ) {
			return true;
		}
		return false;
	}


private:
	ObjectInstanceHandle mHandle;
	std::string mName;
	FederateHandle mOwner;
	std::vector<InstanceAttributeHandle> mAttributes;
};
