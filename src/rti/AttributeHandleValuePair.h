#pragma once
#include <array>

#include <string>

#include "rti/handles/InstanceAttributeHandle.h"

  /**
   * \class AttributeHandleValuePair
   * \author Gregor Barth
   * \date 08.12.2017
   */

class AttributeHandleValuePair {
public:
	AttributeHandleValuePair(InstanceAttributeHandle handle, std::string value)
		: mHandle(handle)
		,	mValue (value)
	{	}

	~AttributeHandleValuePair(){}

	InstanceAttributeHandle const getHandle() {return mHandle;}
	std::string getValue() {return mValue;}
	std::string const getName() {return mName;}

	void setValue(std::string value) {mValue = value;}
	void setName(std::string name) {mName = name;}


private:
	InstanceAttributeHandle mHandle;
	std::string mValue;
	std::string mName;
};
