#pragma once
#include <array>

#include <string>

#include "rti/handles/ParameterHandle.h"

  /**
   * \class ParameterHandleValuePair
   * \author Gregor Barth
   * \date 08.12.2017
   */

class ParameterHandleValuePair {
public:
	ParameterHandleValuePair(ParameterHandle handle, std::string value)
		: mHandle(handle)
		, mValue (value)
	{	}

	~ParameterHandleValuePair(){}

	ParameterHandle const getHandle() {return mHandle;}
	std::string getValue() {return mValue;}
	std::string const getName() {return mName;}

	void setValue(std::string value) {mValue = value;}
	void setName(std::string name) {mName = name;}


private:
	ParameterHandle mHandle;
	std::string mValue;
	std::string mName;
};
