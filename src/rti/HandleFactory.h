#pragma once

#include "rti/handles/Handle.h"
#include "rti/handles/FederateHandle.h"
#include "rti/handles/ObjectClassHandle.h"
#include "rti/handles/AttributeHandle.h"
#include "rti/handles/InteractionClassHandle.h"
#include "rti/handles/ParameterHandle.h"
#include "rti/handles/ObjectInstanceHandle.h"
#include "rti/handles/InstanceAttributeHandle.h"

/**
 * \class HandleFactory
 * A factory for various types of handles.
 * Each handle receives an id wich is unique and persistent within a federation.
 * \author Gregor Barth
 * \date 08.12.2017 
 */

class HandleFactory {

public:

	HandleFactory()
		: mFederatesID(0)
		,	mObjectClassesID(0)
		,	mAttributesID(0)
		,	mInteractionClassesID(0)
		,	mParametersID(0)
		,	mObjectInstancesID(0)
		,	mInstanceAttributesID(0)
	{}

	/*--------------------------------*/
	FederateHandle provideFederateHandle(){
		FederateHandle fedHandle;
		fedHandle.setID(getNextFederateID());
		return fedHandle;
	}

	/*--------------------------------*/
	ObjectClassHandle provideObjectClassHandle(){
		ObjectClassHandle objHandle;
		objHandle.setID(getNextObjectClassID());
		return objHandle;
	}

	/*--------------------------------*/
	AttributeHandle provideAttributeHandle(){
		AttributeHandle attHandle;
		attHandle.setID(getNextAttributeID());
		return attHandle;
	}

	/*--------------------------------*/
	InteractionClassHandle provideInteractionClassHandle(){
		InteractionClassHandle interHandle;
		interHandle.setID(getNextInteractionClassID());
		return interHandle;
	}

	/*--------------------------------*/
	ParameterHandle provideParameterHandle(){
		ParameterHandle paramHandle;
		paramHandle.setID(getNextParameterID());
		return paramHandle;
	}

	/*--------------------------------*/
	ObjectInstanceHandle provideObjectInstanceHandle(){
		ObjectInstanceHandle objInstHandle;
		objInstHandle.setID(getNextObjectInstanceID());
		return objInstHandle;
	}

	/*--------------------------------*/
	InstanceAttributeHandle provideInstanceAttributeHandle(){
		InstanceAttributeHandle instAttrHandle;
		instAttrHandle.setID(getNextInstanceAttributeID());
		return instAttrHandle;
	}

private:
	int getNextFederateID() {
		mFederatesID += 1;
		return mFederatesID;
	}

	int getNextObjectClassID() {
		mObjectClassesID += 1;
		return mObjectClassesID;
	}

	int getNextAttributeID() {
		mAttributesID += 1;
		return mAttributesID;
	}

		int getNextInteractionClassID() {
		mInteractionClassesID += 1;
		return mInteractionClassesID;
	}

	int getNextParameterID() {
		mParametersID += 1;
		return mParametersID;
	}

	int getNextObjectInstanceID() {
		mObjectInstancesID += 1;
		return mObjectInstancesID;
	}

	int getNextInstanceAttributeID() {
		mInstanceAttributesID += 1;
		return mInstanceAttributesID;
	}

	int mFederatesID;
	int mObjectClassesID;
	int mAttributesID;
	int mInteractionClassesID;
	int mParametersID;
	int mObjectInstancesID;
	int mInstanceAttributesID;

};
