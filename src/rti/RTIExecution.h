#pragma once

#include <list>
#include <string>
#include <algorithm>
#include <iostream>

#include "rti/FederationExecution.h"
#include "rti/Exception.h"

/**
 * \class RTIExecution
 * The RTI Execution manages the federation execution(s) and provides the corresponding service calls.
 * \author Gregor Barth
 * \date 08.12.2017
 */


class RTIExecution {

public:
	RTIExecution(){
		std::cout<<"RTI Execution created \n";
	}

	/*-----------------------------------------*/
	FederationExecution* createFederationExecution(std::string const & federationExecutionName) {
		//dont create the same execution twice
		if (existsFederationName(federationExecutionName)) {
			throw FederationExecutionAlreadyExists(federationExecutionName);
		} else {
			FederationExecution* fedExec = new FederationExecution(federationExecutionName);	
			mFederations.push_front(fedExec);
			return fedExec;
		}
	}

	/*-----------------------------------------*/
	void destroyFederationExecution(std::string federationExecutionName) {
		FederationExecution* fedExec = getFederationByName(federationExecutionName);
		if (! fedExec->isHandleFederateIndexEmpty()) {
			throw FederationExecutionNotEmpty(federationExecutionName);
		} else {
			mFederations.remove(fedExec);		
			std::cout<< "destroyed federation execution \n";
		}
	}

	/*-----------------------------------------*/
	FederationExecution* getFederationByName(std::string federationExecutionName) {
		for (std::list<FederationExecution*>::const_iterator it = mFederations.begin(); it != mFederations.end(); it++ )
			if((*it)->getName() == federationExecutionName ) {
				return (*it);
			}
		throw UnknownFederationError(federationExecutionName);
	}

	/*-----------------------------------------*/
	bool existsFederationName(std::string federationExecutionName) {
		for (std::list<FederationExecution*>::const_iterator it = mFederations.begin(); it != mFederations.end(); it++ )
			if((*it)->getName() == federationExecutionName ) {
				return true;
			}
		return false;
	}

private:
	std::list<FederationExecution*> mFederations;
};
