#pragma once

#include <vector>
#include <string>
#include <algorithm>

#include "rti/handles/InteractionClassHandle.h"
#include "rti/handles/ParameterHandle.h"

/**
 * \class Interaction Class
 * A HLA object class is a generic representation of a group of interaction objects with the same parameters
 * which are communicated throughout the simulation.
 * It generalizes them into a format for easy communiction based on the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017 
 */

class InteractionClass{
public:
	InteractionClass(std::string name) : mName(name){}
	~InteractionClass(){}

	InteractionClassHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}
	std::vector<ParameterHandle> const getParameters() {return mParameters;}

	void setHandle(InteractionClassHandle handle) {mHandle = handle;}
	void addParameterHandle(ParameterHandle parameter) {mParameters.push_back(parameter);}

	bool const hasParameter(ParameterHandle paramH) {
		if (std::find(mParameters.begin(), mParameters.end(), paramH) != mParameters.end() ) {
			return true;
			}
		return false;
	}

private:
  InteractionClassHandle mHandle;
	std::string mName;
	std::vector <ParameterHandle> mParameters;
};
