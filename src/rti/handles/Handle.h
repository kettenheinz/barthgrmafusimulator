#pragma once
#include <string>
/**
 * \class Handle
 * Class for a generic handle.
 * All HLA handles must be derived from this class!
 * \author Gregor Barth
 * \date 08.12.2017
 */

class Handle {
public:
	Handle(){}

	virtual int encode() = 0;

	void setID(int i){ id = i;}
	int getID() const {return id;}

	bool operator==(Handle const &rhs) const {
		if (id == rhs.id) {
			return true;
		} else { return false;}
	}

	bool operator!=(Handle const &rhs) const {
		if (id != rhs.id) {
			return true;
		} else { return false;}
	}


	//needed to insert handles as keys in maps
	bool operator<(Handle const &rhs) const {
		if (id < rhs.id) {
			return true;
		} else { return false;}
	}


protected:
	int id;

};

