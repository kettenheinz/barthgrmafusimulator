#pragma once

#include <string>
#include <iostream>
#include <vector>
#include <map>

#include "librti/Federate.h"
#include "rti/ObjectClass.h"
#include "rti/Attribute.h"
#include "rti/Parameter.h"
#include "rti/InteractionClass.h"
#include "rti/ObjectInstance.h"
#include "rti/InstanceAttribute.h"
#include "rti/AttributeHandleValuePair.h"

#include "rti/HandleFactory.h"
#include "rti/Exception.h"

#include "network/Connector.h"


/**
  * \class FederationExecution
  * The federation execution connects and manages all federates as well as the communication between them.
	* Federates join a federation execution and can then declare object classes with attributes which they 
	* want to communicate with other joined federates.
	* They also declare interactions with parameters they send to other federates in here.
  * \author Gregor Barth
  * \date 08.12.2017
  */

typedef std::map<FederateHandle, Federate> HandleFederateMap;
typedef std::map<ObjectClassHandle, ObjectClass*> HandleObjectClassMap;
typedef std::map<AttributeHandle, Attribute*> HandleAttributeMap;
typedef std::map<InteractionClassHandle, InteractionClass*> HandleInteractionClassMap;
typedef std::map<ParameterHandle, Parameter*> HandleParameterMap;
typedef std::map<ObjectInstanceHandle, ObjectInstance*> HandleObjectInstanceMap;
typedef std::map<InstanceAttributeHandle, InstanceAttribute*> HandleInstanceAttributeMap;

class FederationExecution {
public:
	FederationExecution(std::string mName) : mName(mName) {
		std::cout << "created federation execution " + mName + " \n";	
		mHandleFactory = new HandleFactory();
	}

	/*--------------------------- Federate Management --------------------------------*/
	/**
	 * Let a federate join a federation execution.
	 * 4.9 HLA service
	 */
	FederateHandle join(Federate fed) {
		FederateHandle h = mHandleFactory->provideFederateHandle();
		if (hasJoinedFederate(fed.getName())) {
			throw FederateAlreadyExecutionMember(fed.getName());
		} else {
		mHandleFederateIndex.insert(HandleFederateMap::value_type(h, fed));
		std::cout << fed.getName() + " joined federation execution "+ mName + " \n";
		}
		return h;
	}

	/*---------------------------------------*/
	/**
	 * 4.10 HLA-service
	 */
	void resign(std::string const & fedName) {
		FederateHandle fedHandle = getFederateHandle(fedName);
		if (hasJoinedFederate(fedName)) {
			mHandleFederateIndex.erase(fedHandle);
			std::cout<<"federate resigned from federation exection \n";
		} else {
			throw FederateNotExecutionMember(fedName);
		}
	}

	/*-------------------------- Declaration Management ----------------------------*/

	/**
	 * 5.2 HLA service
	 * Handle federate request to publish object classes. The data about which class is
	 * published is stored in the federate itself. The RTI only keeps record which object
	 * classes are transmitted in the federation.
	 */
	void publishObjectClassAttributes(std::string objectClassName,
																		FederateHandle fedH,
																		std::vector<std::string> objectClassAttributes ) {
		ObjectClassHandle h;

		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			//if class is already existing, just add it to the list of published classes of the federate
			if (existsObjectClass(objectClassName)) {
				h = getObjectClassHandle(objectClassName);
			} else {
				ObjectClass* objClass = new ObjectClass(objectClassName);
				h = mHandleFactory->provideObjectClassHandle();
				objClass->setHandle(h);
				mHandleObjectClassIndex.insert(std::pair<ObjectClassHandle, ObjectClass*>(h, objClass));

				//create attributes and add their handles to the object class
				for (std::string attrName : objectClassAttributes) {
					AttributeHandle attrH;
					//if attribute is already existing, just add it to the list of published attributes of the federate
					if (existsAttribute(attrName)) {
						attrH = getAttributeHandle(attrName);
					} else {
						//create new attribute
						Attribute* attribute = new Attribute(attrName);
						attrH = mHandleFactory->provideAttributeHandle();
						attribute->setHandle(attrH);
						mHandleAttributeIndex.insert(std::pair<AttributeHandle, Attribute*>(attrH, attribute));
					}
						objClass->addAttributeHandle(attrH);
						mConnector->getFederateAmbassador(fedH)->getFederate()->addPublishedAttribute(attrH);
				}
			}
			mConnector->getFederateAmbassador(fedH)->getFederate()->addPublishedObjectClass(h);
			std::cout << "published object class " << objectClassName << " to rti" << "\n";
		}
	}

	/**
	 * 5.4 HLA service
	 * Handle federate request to publish interaction classes. The data about which class is
	 * published is stored in the federate itself. The RTI only keeps record which interaction
	 * classes are transmitted in the federation.
	 */
	void publishInteractionClass(std::string interactionClassName,
															 FederateHandle fedH,
															 std::vector<std::string> parameters) {
		InteractionClassHandle interClassH;

		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			//if class is already existing, just add it to the list of published classes of the federate
			if (existsInteractionClass(interactionClassName)) {
				interClassH = getInteractionClassHandle(interactionClassName);
			} else {
				interClassH = createInteractionClass(interactionClassName);
				InteractionClass* interClass = getInteractionClass(interClassH);

				//create parameters and add their handles to the interaction class
				for (std::string paramName : parameters) {
					ParameterHandle paramH;
					//if parameter is already existing, just add it to the list of published parameters of the federate
					if (existsParameter(paramName)) {
						paramH = getParameterHandle(paramName);
					} else {
						//create new Parameter
						paramH = createParameter(paramName);
					}
					interClass->addParameterHandle(paramH);
					mConnector->getFederateAmbassador(fedH)->getFederate()->addPublishedParameter(paramH);
				}
			}
			mConnector->getFederateAmbassador(fedH)->getFederate()->addPublishedInteractionClass(interClassH);
			std::cout << "published interaction class " << interactionClassName << " to rti" << "\n";
		}
	}

	/**
	 * 5.6 HLA service
	 * Handle federate request to subscribe to object classes. The data about which class
	 * has been subscribed to is stored in the federate itself.
	 */
	void subscribeObjectClassAttributes(std::string objClassName, FederateHandle fedH, std::vector<std::string> objectClassAttributes) {
		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			ObjectClassHandle objClassH = getObjectClassHandle(objClassName);
			if (! existsObjectClass(objClassName)) {
				//if no object class exists yet, we just create one. Some other federate might
				//publish it and if we never receive it the program wont crash either.
				objClassH = createObjectClass(objClassName);
				ObjectClass* objClass = getObjectClass(objClassH);
				Federate* fed = mConnector->getFederateAmbassador(fedH)->getFederate();
				fed->addSubscribedObjectClass(objClassH);
				for (std::string attrName : objectClassAttributes) {
					AttributeHandle attrH;
					attrH = createAttribute(attrName);
					objClass->addAttributeHandle(attrH);
					fed->addSubscribedAttribute(attrH);
				}
			} else {
				objClassH = getObjectClassHandle(objClassName);
				ObjectClass* objClass = getObjectClass(objClassH);
				Federate* fed = mConnector->getFederateAmbassador(fedH)->getFederate();
				fed->addSubscribedObjectClass(objClassH);

				for (std::string attrName : objectClassAttributes) {
					AttributeHandle attrH = getAttributeHandle(attrName);
					if (objClass->hasAttribute(attrH)) {
						fed->addSubscribedAttribute(attrH);
					} else {
						throw UnknownAttributeNameError(attrName);
					}
				}
				//let federate discover all existing object instances of this obj class here
				for (auto instanceH : objClass->getInstances()) {
					ObjectInstance* objInst = getObjectInstance(instanceH);
					mConnector->getFederateAmbassador(fedH)->discoverObjectInstance(instanceH, objClassH, objInst->getName(), objInst->getAttributes());
				}
			}
			std::cout << "subscribed to object class " << objClassName << "\n";
		}
	}

	/**
	 * Create a new object class
	 */
	ObjectClassHandle createObjectClass(std::string const& objClassName) {
		ObjectClass * objClass = new ObjectClass(objClassName);
		ObjectClassHandle h = mHandleFactory->provideObjectClassHandle();
		objClass->setHandle(h);
		mHandleObjectClassIndex.insert(std::pair<ObjectClassHandle, ObjectClass*>(h, objClass));
		return h;
	}

	/**
	 * Create a new attribute for an object class
	 */
	AttributeHandle createAttribute(std::string const& attrName) {
		Attribute* attribute = new Attribute(attrName);
		AttributeHandle h = mHandleFactory->provideAttributeHandle();
		attribute->setHandle(h);
		mHandleAttributeIndex.insert(std::pair<AttributeHandle, Attribute*>(h, attribute));
		return h;
	}


	/**
	 * 5.8 HLA service
	 * Handle federate request to subscribe to interaction classes. The data about which class
	 * has been subscribed to is stored in the federate itself.
	 */
	void subscribeInteractionClass(std::string interactionClassName, FederateHandle fedH, std::vector<std::string> parameters) {
		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			InteractionClassHandle interClassH;
			if (! existsInteractionClass(interactionClassName)) {
				//if no interaction exists yet, we just create one. Some other federate might
				//publish it and if we never receive it the program wont crash either.
				interClassH = createInteractionClass(interactionClassName);
				InteractionClass* interClass = getInteractionClass(interClassH);
				Federate* fed = mConnector->getFederateAmbassador(fedH)->getFederate();
				fed->addSubscribedInteractionClass(interClassH);
				for (std::string paramName : parameters) {
					ParameterHandle paramH;
					paramH = createParameter(paramName);
					interClass->addParameterHandle(paramH);
					fed->addSubscribedParameter(paramH);
				}
			} else {
				interClassH = getInteractionClassHandle(interactionClassName);
				InteractionClass* interClass = getInteractionClass(interClassH);
				Federate* fed = mConnector->getFederateAmbassador(fedH)->getFederate();
				fed->addSubscribedInteractionClass(interClassH);

				for (std::string paramName : parameters) {
					ParameterHandle paramH = getParameterHandle(paramName);
					if (interClass->hasParameter(paramH)) {
						fed->addSubscribedParameter(paramH);
					} else {
						throw UnknownParameterNameError(paramName);
					}
				}
			}
			std::cout << "subscribed to interaction class " << interactionClassName << "\n";
		}
	}

	/**
	 * Create a new interaction class
	 */
	InteractionClassHandle createInteractionClass(std::string const& interactionClassName) {
			InteractionClass* interClass = new InteractionClass(interactionClassName);
			InteractionClassHandle h = mHandleFactory->provideInteractionClassHandle();
			interClass->setHandle(h);
			mHandleInteractionClassIndex.insert(std::pair<InteractionClassHandle, InteractionClass*>(h, interClass));
			return h;
	}

	/**
	 * Create a new parameter for an interaction class
	 */
	ParameterHandle createParameter(std::string const& paramName) {
		Parameter* parameter = new Parameter(paramName);
		ParameterHandle paramH = mHandleFactory->provideParameterHandle();
		parameter->setHandle(paramH);
		mHandleParameterIndex.insert(std::pair<ParameterHandle, Parameter*>(paramH, parameter));
		return paramH;
	}



/*--------------------------- Object Management --------------------------------*/
	/**
	 * 6.9 HLA service
	 */
	ObjectInstanceHandle registerObjectInstance(std::string objClassName, FederateHandle fedH) {
		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			//check if fed publishes the object class
			Federate* fed = mConnector->getFederateAmbassador(fedH)->getFederate();
			ObjectClassHandle objClassH = getObjectClassHandle(objClassName);
			ObjectInstanceHandle h;
			if (! fed->publishesObjectClass(objClassH)) {
				throw FederateNotPublishing(objClassName, getFederate(fedH).getName());
			} else {
				h = mHandleFactory->provideObjectInstanceHandle();
				std::string objInstName = fed->getName() + "." + objClassName + "." + std::to_string(h.getID());
				ObjectInstance* objInst = new ObjectInstance(h, objInstName, fedH);
				mHandleObjectInstanceIndex.insert(std::pair<ObjectInstanceHandle, ObjectInstance*>(h, objInst));
				ObjectClass* objClass = getObjectClass(objClassH);
				objClass->addInstanceHandle(h);
				fed->addRegisteredObjectInstance(h);

				//add instanceAttributeHandles for objectInstance
				std::vector<InstanceAttributeHandle> instAttributes;
				for (auto attrH : objClass->getAttributes()) {
					InstanceAttributeHandle instAttrH = mHandleFactory->provideInstanceAttributeHandle();
					std::string instAttrName = objInstName + "." + getAttribute(attrH)->getName();
					InstanceAttribute* instAttr = new InstanceAttribute(instAttrH, instAttrName, fedH);
					mHandleInstanceAttributeIndex.insert(std::pair<InstanceAttributeHandle, InstanceAttribute*>(instAttrH, instAttr));
					//add to obj instance
					objInst->addAttributeHandle(instAttrH);
					fed->addRegisteredInstanceAttribute(instAttrH);
					instAttributes.push_back(instAttrH);
				}

				//get all federates that subscribe to the object class (maybe to just one attribute?) and
				//let them discover the new object
				std::vector<FederateHandle> subscribers = getSubscribingFederates(objClassH);
				for (auto s : subscribers ) {
					mConnector->getFederateAmbassador(s)->discoverObjectInstance(h, objClassH, objInstName, instAttributes);
				}
			}		
			return h;
		}
	}

	/*---------------------------------------*/
	/**
	 * 6.10 HLA service
	 */
	void updateAttributeValues(FederateHandle fedH,
														 ObjectInstanceHandle obj,
														 std::vector<AttributeHandleValuePair> attributes) {
		//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			//check if sender is owner of attributes
			std::vector<InstanceAttributeHandle> attrHandles;		
			for (auto p : attributes) {
				attrHandles.push_back(p.getHandle());
			}
			if (! ownsAttributes(fedH, attrHandles)) {
				throw AttributeNotOwned(getFederate(fedH).getName());
			} else {
				//send new values to all subscribers
				ObjectClassHandle objClassH = getObjectClassOfInstance(obj);
				std::vector<FederateHandle> subscribers = getSubscribingFederates(objClassH);
				for (auto s : subscribers ) {
					mConnector->getFederateAmbassador(s)->reflectAttributeValues(obj, attributes);
				}
			}
		}
	}

	/*---------------------------------------*/
	/**
	 * Check if a certain federate owns EVERY single instanceAttribute of a given set
	 */
	bool const ownsAttributes(FederateHandle fed, std::vector<InstanceAttributeHandle> attrHandles) {
		for (auto h : attrHandles) {
			if (getInstanceAttribute(h)->getOwner() == fed) {	return true;}
		}
		return false;
	}

	/*---------------------------------------*/
	/**
	 * 6.10 HLA service
	 * Send an interaction to all subscribing federates.
	 */
	void sendInteraction(FederateHandle fedH,
											 InteractionClassHandle interH,
											 std::vector<ParameterHandleValuePair> parameters) {
			//federate must be execution member
		if (! hasJoinedFederate(getFederate(fedH).getName())) {
				throw FederateNotExecutionMember(getFederate(fedH).getName());
		} else {
			//send new values to all subscribers
			std::vector<FederateHandle> subscribers = getSubscribingFederates(interH);
			for (auto s : subscribers ) {
				mConnector->getFederateAmbassador(s)->receiveInteraction(interH, parameters);
			}
		}
	}

/*--------------------------- Ownership Management --------------------------------*/
/*--------------------------- Time Management --------------------------------*/

	//in true hla we would have more complex logic here.
	//for the time being we expect all federates to have the same framerate and only one
	//federate is regulating. Thus we only pass on the advance step to all other
	//federates once a request comes in. This way we keep all federates synchronized.
	void advanceTime(FederateHandle fedH) {
		for (auto fed : mHandleFederateIndex) {
			if (fedH != fed.first) {//FIXME sort out the caller by handle comparison, not like this
				mConnector->getFederateAmbassador(fed.first)->timeAdvanceGrant();
			}
		}	
	}


/*--------------------------- Data Distribution Management --------------------------------*/


/*--------------------------- Support Services --------------------------------*/

//----------- Federates -------------//
	Federate getFederate(FederateHandle h) const {
    HandleFederateMap::const_iterator it = mHandleFederateIndex.find(h);
    if (it != mHandleFederateIndex.end()) {
			return it->second;
    }
		throw UnknownFederateError();
	}

/*---------------------------------------*/
	FederateHandle getFederateHandle(std::string mName) const {
		HandleFederateMap::const_iterator it;
		for (it = mHandleFederateIndex.begin(); it != mHandleFederateIndex.end(); ++it ) {
			if (it->second.getName() == mName) { return it->first;}
		}
		throw UnknownFederateNameError(mName);
	}

/*---------------------------------------*/
	bool hasJoinedFederate(std::string fedName) {
		for (std::map<FederateHandle, Federate>::const_iterator it = mHandleFederateIndex.begin(); it != mHandleFederateIndex.end(); it++ )
			if(it->second.getName() == fedName) {
				return true;
			}
		return false;

	}


//----------- Object Classes -------------//
	ObjectClass* getObjectClass(ObjectClassHandle h) const {
    HandleObjectClassMap::const_iterator it = mHandleObjectClassIndex.find(h);
    if (it != mHandleObjectClassIndex.end()) {
			return it->second;
    }
		throw UnknownObjectClassError();
	}

/*---------------------------------------*/
	ObjectClassHandle getObjectClassHandle(std::string mName) const {
		HandleObjectClassMap::const_iterator it;
		for (it = mHandleObjectClassIndex.begin(); it != mHandleObjectClassIndex.end(); ++it ) {
			if (it->second->getName() == mName) { return it->first;}
		}
		throw UnknownObjectClassNameError(mName);
	}

/*---------------------------------------*/
	bool existsObjectClass(std::string objectClassName) {
		for (std::map<ObjectClassHandle, ObjectClass*>::const_iterator it = mHandleObjectClassIndex.begin(); it != mHandleObjectClassIndex.end(); it++ )
			if(it->second->getName() == objectClassName) {
				return true;
			}
		return false;
	}

/*---------------------------------------*/
	std::vector<FederateHandle> getSubscribingFederates(ObjectClassHandle objClass) {
		std::vector<FederateHandle> subscribers;
		for (auto  it : mHandleFederateIndex) {
			Federate* fed = mConnector->getFederateAmbassador(it.first)->getFederate();
			if (fed->subscribesObjectClass(objClass)) {
				subscribers.push_back(it.first);	
			}			
		}	
		return subscribers;
	}

/*---------------------------------------*/
	ObjectClassHandle getObjectClassOfInstance(ObjectInstanceHandle instH) {
		ObjectClassHandle h;
		for (auto pair : mHandleObjectClassIndex) {
			if (pair.second->hasInstance(instH)) {
				h = pair.first;
			}
		}
		return h;
	}

//----------- Attributes -------------//
	
	Attribute* getAttribute(AttributeHandle h) const {
    HandleAttributeMap::const_iterator it = mHandleAttributeIndex.find(h);
    if (it != mHandleAttributeIndex.end()) {return it->second;}
		throw UnknownAttributeError();
	}

/*---------------------------------------*/
	AttributeHandle getAttributeHandle(std::string mName) const {
		HandleAttributeMap::const_iterator it;
		for (it = mHandleAttributeIndex.begin(); it != mHandleAttributeIndex.end(); ++it ) {
			if (it->second->getName() == mName) { return it->first;}
		}
		throw UnknownAttributeNameError(mName);
	}

/*---------------------------------------*/
	std::string getAttributeName(AttributeHandle h) {
		return getAttribute(h)->getName();
	}
/*---------------------------------------*/
	bool existsAttribute(std::string attributeName) {
		for (std::map<AttributeHandle, Attribute*>::const_iterator it = mHandleAttributeIndex.begin(); it != mHandleAttributeIndex.end(); it++ ) {
			if(it->second->getName() == attributeName) {
				return true;
			}
		}
		return false;
	}

//----------- Interaction Classes -------------//
	InteractionClass* getInteractionClass(InteractionClassHandle h) const {
    HandleInteractionClassMap::const_iterator it = mHandleInteractionClassIndex.find(h);
    if (it != mHandleInteractionClassIndex.end()) {return it->second; }
		throw UnknownInteractionClassError();
	}

/*---------------------------------------*/
	InteractionClassHandle getInteractionClassHandle(std::string mName) const {
		HandleInteractionClassMap::const_iterator it;
		for (it = mHandleInteractionClassIndex.begin(); it != mHandleInteractionClassIndex.end(); ++it ) {
			if (it->second->getName() == mName) { return it->first; }
		}
		throw UnknownInteractionClassNameError(mName);
	}

/*---------------------------------------*/
	std::string getInteractionClassName(InteractionClassHandle h) {
		return getInteractionClass(h)->getName();
	}
/*---------------------------------------*/
	std::vector<ParameterHandle> getInteractionClassParameters(InteractionClassHandle interH) {
		return getInteractionClass(interH)->getParameters();
	}

/*---------------------------------------*/
	bool existsInteractionClass(std::string interactionClassName) {
		for (std::map<InteractionClassHandle, InteractionClass*>::const_iterator it = mHandleInteractionClassIndex.begin(); it != mHandleInteractionClassIndex.end(); it++ )
			if(it->second->getName() == interactionClassName) {
				return true;
			}
		return false;
	}
/*---------------------------------------*/
	std::vector<FederateHandle> getSubscribingFederates(InteractionClassHandle interH) {
		std::vector<FederateHandle> subscribers;
		for (auto  it : mHandleFederateIndex) {
			Federate* fed = mConnector->getFederateAmbassador(it.first)->getFederate();
			if (fed->subscribesInteractionClass(interH)) {
				subscribers.push_back(it.first);	
			}			
		}	
		return subscribers;
	}

//----------- Parameters -------------//
	Parameter* getParameter(ParameterHandle h) const {
    HandleParameterMap::const_iterator it = mHandleParameterIndex.find(h);
    if (it != mHandleParameterIndex.end()) {	return it->second; }
		throw UnknownParameterError();
	}
/*---------------------------------------*/
	ParameterHandle getParameterHandle(std::string mName) const {
		HandleParameterMap::const_iterator it;
		for (it = mHandleParameterIndex.begin(); it != mHandleParameterIndex.end(); ++it ) {
			if (it->second->getName() == mName) {return it->first;}
		}
		throw UnknownParameterNameError(mName);
	}
/*---------------------------------------*/
	std::string getParameterName(ParameterHandle h) {
		return getParameter(h)->getName();
	}
/*---------------------------------------*/
	bool existsParameter(std::string paramName) {
		for (std::map<ParameterHandle, Parameter*>::const_iterator it = mHandleParameterIndex.begin(); it != mHandleParameterIndex.end(); it++ )
			if(it->second->getName() == paramName) {
				return true;
			}
		return false;
	}

//----------- Object Instance -------------//
	ObjectInstance* getObjectInstance(ObjectInstanceHandle h) const {
    HandleObjectInstanceMap::const_iterator it = mHandleObjectInstanceIndex.find(h);
    if (it != mHandleObjectInstanceIndex.end()) {return it->second; }
		throw UnknownObjectInstanceError();
	}
/*---------------------------------------*/
	ObjectInstanceHandle getObjectInstanceHandle(std::string mName) const {
		HandleObjectInstanceMap::const_iterator it;
		for (it = mHandleObjectInstanceIndex.begin(); it != mHandleObjectInstanceIndex.end(); ++it ) {
			if (it->second->getName() == mName) { return it->first;}
		}
		throw UnknownObjectInstanceNameError(mName);
	}
/*---------------------------------------*/
	std::vector<InstanceAttributeHandle> getObjectInstanceAttributes(ObjectInstanceHandle objH) {
		return getObjectInstance(objH)->getAttributes();
	}

//----------- Instance Attribute -------------//
	InstanceAttribute* getInstanceAttribute(InstanceAttributeHandle h) const {
    HandleInstanceAttributeMap::const_iterator it = mHandleInstanceAttributeIndex.find(h);
    if (it != mHandleInstanceAttributeIndex.end()) {return it->second;}
		throw UnknownInstanceAttributeError();
	}
/*---------------------------------------*/
	InstanceAttributeHandle getInstanceAttributeHandle(std::string mName) const {
		HandleInstanceAttributeMap::const_iterator it;
		for (it = mHandleInstanceAttributeIndex.begin(); it != mHandleInstanceAttributeIndex.end(); ++it ) {
			if (it->second->getName() == mName) {return it->first;}
		}
		throw UnknownInstanceAttributeNameError(mName);
	}
/*---------------------------------------*/
	std::string getInstanceAttributeName(InstanceAttributeHandle h) {
		InstanceAttribute* attr = getInstanceAttribute(h);
		return attr->getName();
	}




  std::string getName() { return mName; }
	void setConnector(Connector* c) {mConnector = c;}
	bool isHandleFederateIndexEmpty() { return mHandleFederateIndex.empty();}

private:
  std::string mName;
	Connector* mConnector;
	HandleFactory* mHandleFactory;

	HandleFederateMap mHandleFederateIndex;
	HandleObjectClassMap mHandleObjectClassIndex;
	HandleAttributeMap mHandleAttributeIndex;
	HandleInteractionClassMap mHandleInteractionClassIndex;
	HandleParameterMap mHandleParameterIndex;
	HandleObjectInstanceMap mHandleObjectInstanceIndex;
	HandleInstanceAttributeMap mHandleInstanceAttributeIndex;

};
