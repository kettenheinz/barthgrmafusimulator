#pragma once
#include <string>

#include "rti/handles/AttributeHandle.h"

/**
 * \class Attribute
 * A HLA attribute is a generic representation of a single attribute of an object class which
 * is communicated throughout the simulation.
 * It generalizes a specific attribute of an agent into a format for easy communiction based on
 * the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class Attribute {
public:
	Attribute(std::string name) : mName(name){}
	~Attribute(){}

	AttributeHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}

	void setHandle(AttributeHandle handle) {mHandle = handle;}
	void setName(std::string name) {mName = name;}

private:
	AttributeHandle mHandle;
	std::string mName;
};
