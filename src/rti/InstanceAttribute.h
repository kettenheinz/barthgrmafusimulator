#pragma once
#include <string>

#include "rti/handles/InstanceAttributeHandle.h"
#include "rti/handles/FederateHandle.h"

/**
 * \class Instance Attribute
 * A HLA instance attribute is a generic representation of a specific attribute of a object instance
 * which is communicated in the simulation.
 * It generalizes the attribute into a format for easy communiction based on the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class InstanceAttribute {
public:
	InstanceAttribute(InstanceAttributeHandle h, std::string name, FederateHandle fed)
		: mHandle(h)
		,	mName(name)
		, mOwner(fed)
	{}


	~InstanceAttribute(){}

	InstanceAttributeHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}
	FederateHandle const getOwner() {return mOwner;}
	void setHandle(InstanceAttributeHandle handle) {mHandle= handle;}
	void setName(std::string name) {mName = name;}
	void setOwner(FederateHandle owner) {mOwner = owner;}


private:
	InstanceAttributeHandle mHandle;
	std::string mName;
	FederateHandle mOwner;
};
