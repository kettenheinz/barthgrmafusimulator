#pragma once
#include <vector>
#include <string>
#include <algorithm>

#include "rti/handles/ObjectClassHandle.h"
#include "rti/handles/AttributeHandle.h"
#include "rti/handles/ObjectInstanceHandle.h"

/**
 * \class Object Class
 * A HLA object class is a generic representation of a group of objects with the same attributes
 * which are communicated throughout the simulation.
 * It generalizes them into a format for easy communiction based on the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017
 */

class ObjectClass {
public:
	ObjectClass(std::string name) : mName(name) {}
	~ObjectClass(){}

	ObjectClassHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}
	std::vector<AttributeHandle> const getAttributes() {return mAttributes;}
	std::vector<ObjectInstanceHandle> const getInstances() {return mInstances;}

	void setHandle(ObjectClassHandle handle) {mHandle = handle;}
	void addAttributeHandle(AttributeHandle attribute) {mAttributes.push_back(attribute);}
	void addInstanceHandle(ObjectInstanceHandle instanceH) {mInstances.push_back(instanceH);}

	bool const hasAttribute(AttributeHandle attrH) {
		if (std::find(mAttributes.begin(), mAttributes.end(), attrH) != mAttributes.end() )  {
			return true;
		}
			return false;
	}

	bool const hasInstance(ObjectInstanceHandle instH) {
		if (std::find(mInstances.begin(), mInstances.end(), instH) != mInstances.end() ) {
			return true;
		}
		return false;
	}

private:
	ObjectClassHandle mHandle;
	std::string mName;
	std::vector<AttributeHandle> mAttributes;
	std::vector<ObjectInstanceHandle> mInstances;
};
