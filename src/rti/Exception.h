#pragma once
#include <stdexcept>
#include <string>
#include <iostream>

/**
 * \class Exception Class
 * All custom exceptions and HLA exceptions.
 * \author Gregor Barth
 * \date 08.12.2017
 */

static void printException(std::exception const& e, int level = 0) {
	std::cerr << std::string(level, ' ') << "Exception! " << e.what() << '\n';
//	try {
//		std::rethrow_if_nested(e);
//	} catch(const std::exception& e) {
//		printException(e, level+1);
//	} catch(...) {}

}


/*---------------- Federation Execution exceptions -------------------*/
class UnknownFederationError: public std::runtime_error {
	public:
		UnknownFederationError(std::string name): std::runtime_error("No federation with the name '" + name + "' was found."){}
};

class FederationExecutionAlreadyExists: public std::runtime_error {
	public:
		 FederationExecutionAlreadyExists(std::string name): std::runtime_error("Federation with the name '" + name + "' already exists. Skipping..."){}
};

class FederationExecutionNotEmpty: public std::runtime_error {
	public:
		 FederationExecutionNotEmpty(std::string name): std::runtime_error("Federation '" + name + "' still has joined members. Won't delete it."){}
};

class FederateNameAlreadyInUse: public std::runtime_error {
	public:
		 FederateNameAlreadyInUse(std::string name): std::runtime_error("Federation '" + name + "' is already in use!"){}
};

class FederateAlreadyExecutionMember: public std::runtime_error {
	public:
		 FederateAlreadyExecutionMember(std::string name): std::runtime_error("Federation '" + name + "' is already a member of this federation execution!"){}
};

class FederateNotExecutionMember: public std::runtime_error {
	public:
		 FederateNotExecutionMember(std::string name): std::runtime_error("Federation '" + name + "' is a not a member of this federation execution! Skipping..."){}
};

/*---------------- Declaration exceptions -------------------*/

class ObjectClassAlreadyDeclared: public std::runtime_error {
	public:
		 ObjectClassAlreadyDeclared(std::string name): std::runtime_error("Object class with the name '" + name + "' has already been declared. Skipping..."){}
};



/*---------------- Object management exceptions -------------------*/
class FederateNotPublishing: public std::runtime_error {
	public:
		FederateNotPublishing(std::string objClassName, std::string fedName): std::runtime_error("The object class" + objClassName + " is not published by the " + fedName + " federate."){}
};

class AttributeNotOwned: public std::runtime_error {
	public:
		AttributeNotOwned(std::string fedName): std::runtime_error("The federate " + fedName + " does not own all attributes it wants to update."){}
};

/*---------------- Time management exceptions -------------------*/

/*---------------- Support services exceptions -------------------*/
//----------- Federates -------------//
class UnknownFederateError: public std::runtime_error {
	public:
		UnknownFederateError(): std::runtime_error("No federate with the given handle was found."){}
};
class UnknownFederateNameError: public std::runtime_error {
	public:
		UnknownFederateNameError(std::string name): std::runtime_error("No federate with the name '" + name + "' was found."){}
};

//----------- Object Classes -------------//
class UnknownObjectClassError: public std::runtime_error {
	public:
		UnknownObjectClassError(): std::runtime_error("No object class with the given handle was found."){}
};
class UnknownObjectClassNameError: public std::runtime_error {
	public:
		UnknownObjectClassNameError(std::string name): std::runtime_error("No object class with the name '" + name + "' was found."){}
};

//----------- Attributes -------------//
class UnknownAttributeError: public std::runtime_error {
	public:
		UnknownAttributeError(): std::runtime_error("No attribute with the given handle was found."){}
};
class UnknownAttributeNameError: public std::runtime_error {
	public:
		UnknownAttributeNameError(std::string name): std::runtime_error("No attribute with the name '" + name + "' was found."){}
};

//----------- Interaction Classes -------------//
class UnknownInteractionClassError: public std::runtime_error {
	public:
		UnknownInteractionClassError(): std::runtime_error("No interaction class with the given handle was found."){}
};
class UnknownInteractionClassNameError: public std::runtime_error {
	public:
		UnknownInteractionClassNameError(std::string name): std::runtime_error("No interaction class with the name '" + name + "' was found."){}
};

//----------- Parameters -------------//
class UnknownParameterError: public std::runtime_error {
	public:
		UnknownParameterError(): std::runtime_error("No parameter with the given handle was found."){}
};
class UnknownParameterNameError: public std::runtime_error {
	public:
		UnknownParameterNameError(std::string name): std::runtime_error("No parameter with the name '" + name + "' was found."){}
};

//----------- Object Instance -------------//
class UnknownObjectInstanceError: public std::runtime_error {
	public:
		UnknownObjectInstanceError(): std::runtime_error("No object instance with the given handle was found."){}
};
class UnknownObjectInstanceNameError: public std::runtime_error {
	public:
		UnknownObjectInstanceNameError(std::string name): std::runtime_error("No object instance with the name '" + name + "' was found."){}
};

//----------- Instance Attribute -------------//
class UnknownInstanceAttributeError: public std::runtime_error {
	public:
		UnknownInstanceAttributeError(): std::runtime_error("No instance attribute with the given handle was found."){}
};
class UnknownInstanceAttributeNameError: public std::runtime_error {
	public:
		UnknownInstanceAttributeNameError(std::string name): std::runtime_error("No instance attribtue with the name '" + name + "' was found."){}
};


/*---------------- Agent related exceptions -------------------*/
class UnknownObjectError: public std::runtime_error {
	public:
		UnknownObjectError(): std::runtime_error("Unknown object!"){}
};

class UnknownPluginError: public std::runtime_error {
	public:
		UnknownPluginError(): std::runtime_error("Unknown behaviour plugin!"){}
};

class PluginActivationError: public std::runtime_error {
	public:
		PluginActivationError(): std::runtime_error("Cannot activate behaviour plugin!"){}
};

