#pragma once

#include <string>

#include "rti/handles/ParameterHandle.h"

/**
 * \class Parameter
 * A HLA paramter is a generic representation of a single attribute of an object class which
 * is communicated throughout the simulation.
 * It generalizes a specific parameter of an agent into a format for easy communiction based on
 * the HLA standard.
 * \author Gregor Barth
 * \date 08.12.2017 
 */

class Parameter {
public:
	Parameter(std::string name) : mName(name){}
	~Parameter(){}

	ParameterHandle const getHandle() {return mHandle;}
	std::string const getName() {return mName;}

	void setHandle(ParameterHandle handle) {mHandle = handle;}
	void setName(std::string name) {mName = name;}

private:

	ParameterHandle mHandle;
	std::string mName;
};
