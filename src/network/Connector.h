#pragma once

#include <map>

#include "rti/handles/FederateHandle.h"

/**
 * \class Connector
 * This class is a placeholder for a network layer. It connects the rti with the federates when the whole simulation
 * is run on one local system.
 * Replace this file with true network message handling to make the whole framework distributed.
 * \author Gregor Barth
 */

class RTIExecution;
class FederationExecution;
class FederateAmbassador;

class Connector {
	public:
		Connector() {}
		~Connector(){}

		RTIExecution* getRTIExection() const { return mRtiExe;}
		FederationExecution* getFederationExecution() const { return mFedExe;}
		FederateAmbassador* getFederateAmbassador(FederateHandle h) const {
			return mFederateAmbassadors.find(h)->second;
		}

		void setRTIExecution(RTIExecution* rti) {mRtiExe = rti;}
		void setFederationExecution(FederationExecution* fed) {mFedExe = fed;}
		void addFederateAmbassador(FederateHandle h, FederateAmbassador* f) {
			mFederateAmbassadors.insert(std::map<FederateHandle,FederateAmbassador*>::value_type(h,f));
		}

	private:
		RTIExecution* mRtiExe;
		FederationExecution* mFedExe;
		std::map<FederateHandle, FederateAmbassador*> mFederateAmbassadors;
};
